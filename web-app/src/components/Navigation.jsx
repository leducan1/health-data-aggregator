import React, {useEffect} from 'react';
import {NavLink, useParams} from "react-router-dom";
import classes from './Navigation.module.css';
import Icon from "./Icon";
import {withRouter} from "react-router";
import {getDateInLS} from "../helpers/Helpers";

const Navigation = () => {
    useEffect(()=>{
        console.log("render navigation");
    })

    return (
        <nav className={classes.Navigation}>
            <ul className={classes.NavigationList}>
                <li className={classes.NavigationListItem}>
                    <NavLink to={"/summary"} activeClassName={""}>
                        <Icon className={classes.NavigationListItem__Icon} hex={"f9f9f9"} type={"dashboard"}/>
                        Daily Summary
                    </NavLink>
                </li>
                <li className={classes.NavigationListItem}>
                    <NavLink to={"/activity"} activeClassName={""}>
                        <Icon className={classes.NavigationListItem__Icon} hex={"f9f9f9"} type={"dumbbell"}/>
                        Activities
                    </NavLink>
                </li>
                <li className={classes.NavigationListItem}>
                    <NavLink to={"/sleep"} activeClassName={""}>
                        <Icon className={classes.NavigationListItem__Icon} hex={"f9f9f9"} type={"sleep"}/>
                        Sleep
                    </NavLink>
                </li>
                <li className={classes.NavigationListItem}>
                    <NavLink to={"/weight"} activeClassName={""}>
                        <Icon className={classes.NavigationListItem__Icon} hex={"f9f9f9"} type={"scale"}/>
                        Weight
                    </NavLink>
                </li>
                <li className={classes.NavigationListItem}>
                    <NavLink to={"/water"} activeClassName={""}>
                        <Icon className={classes.NavigationListItem__Icon} hex={"f9f9f9"} type={"water_drop"}/>
                        Water Intake
                    </NavLink>
                </li>
                <li className={classes.NavigationListItem}>
                    <NavLink to={"/heart-rate"} activeClassName={""}>
                        <Icon className={classes.NavigationListItem__Icon} hex={"f9f9f9"} type={"heart_rate"}/>
                        Heart Rate
                    </NavLink>
                </li>
                <li className={classes.NavigationListItem}>
                    <NavLink to={"/blood-pressure"} activeClassName={""}>
                        <Icon className={classes.NavigationListItem__Icon} hex={"f9f9f9"} type={"blood_pressure"}/>
                        Blood Pressure
                    </NavLink>
                </li>
            </ul>
        </nav>
    );
}

export default Navigation;
