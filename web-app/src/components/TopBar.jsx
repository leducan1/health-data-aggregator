import React, {useEffect, useState} from 'react';
import classes from './TopBar.module.css';
import {enGB} from 'date-fns/locale'
import {DatePicker} from 'react-nice-dates'
import 'react-nice-dates/build/style.css'
import {getDateInLS, setDateInLS, validateDateInLS} from "../helpers/Helpers";
import SelectOriginModal from "./modal/SelectOriginModal";

const TopBar = () => {
    const origins = ["Fitbit", "Samsung", "This", "Google"];

    const [date, setDate] = useState(new Date(getDateInLS()));
    const [showOriginModal, setShowOriginModal] = useState(false);

    const toggleOriginModal = () => {
        setShowOriginModal(!showOriginModal);
    }

    useEffect(() => {
        console.log(date)
        validateDateInLS();
        console.log("use effect TOPBAR");
    })

    return (
        <div className={classes.TopBar}>
            <button>add</button>
            <DatePicker date={date} locale={enGB} onDateChange={(date) => {
                setDate(date);
                setDateInLS(date);
            }}>
                {({inputProps, focused}) => (
                    <input
                        className={'input' + (focused ? ' -focused' : '')}
                        {...inputProps}
                        readOnly={false}
                    />
                )}
            </DatePicker>
            <button onClick={toggleOriginModal}>toggle origin</button>
            {showOriginModal && <SelectOriginModal/>}
        </div>
    );
}

export default TopBar;
