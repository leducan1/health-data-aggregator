import {BACKEND} from "../constants/Constants";
import axios from "axios";

export const fetchWaterLogsByDate = async (origin, date) => {
    const response = await axios.get(BACKEND + `water/${origin}/${date}`);
    return response;
}
