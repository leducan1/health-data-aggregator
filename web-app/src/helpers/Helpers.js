import moment from "moment";

export function setJWT($token) {
    localStorage.setItem("token", $token);
}

export function getJWT() {
    localStorage.getItem("token");
}

export function getTodayStringDate() {
    return moment(new Date()).format("yyyy-MM-DD");
}

export function validateDateInLS() {
    try {
        moment(new Date(localStorage.getItem("date"))).format("yyyy-MM-DD");
    } catch (e) {
        localStorage.setItem("date", moment(new Date()).format("yyyy-MM-DD"))
    }
}

export function getDateInLS() {
    validateDateInLS();
    return localStorage.getItem("date");
}

export function setDateInLS(date) {
    localStorage.setItem("date", moment(date).format("yyyy-MM-DD"))
}
