import {MAX_NUMBER_OF_COLORS} from "../constants/Constants";

/*
* Checks validity of a hex value with a regular expression
* Returns true if valid, false if not
* */
export const isValidHex = (value) => {
    const regExp = /^([0-9A-F]{3}){1,2}$/i;
    return regExp.test(value);
}

/*
* Receives an array of hex values
* Returns false if array.length > NUMBER_OF_COLORS or any hex value is not a valid color
* */
export const isColorsValid = (arrayOfColors) => {
    if (arrayOfColors.length > MAX_NUMBER_OF_COLORS) return false;
    for (const color of arrayOfColors) if (!isValidHex(color)) return false;
    return true;
}

export const isEmailFormatValid = (email) => {
    const regExp = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return regExp.test(String(email).toLowerCase());
}