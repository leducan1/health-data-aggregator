import React, {Component, useContext, useEffect, useState} from 'react';
import {Link, useHistory, useParams, withRouter} from "react-router-dom";
import PropTypes from 'prop-types';
import classes from './Activity.module.css';
import axios from 'axios';
import {BACKEND} from "../constants/Constants";
import Icon from "../components/Icon";
import * as Validator from "../helpers/Validator";
import {AppContext} from "../context/AppContext";
import moment from "moment";
import TopBar from "../components/TopBar";
import Navigation from "../components/Navigation";

const Activity = () => {
    const {origin, setOrigin, fetchData, checkOriginStatus} = useContext(AppContext);
    const {log_date} = useParams();
    const [date, setDate] = useState(new Date(log_date));
    const history = useHistory();


    useEffect(() => {
        console.log(log_date)
        checkOriginStatus();
        if (date != undefined) {
            fetch().then(result => {
                console.log(result.data);
            })
        }
    });

    const fetch = async () => {
        const res = await fetchData(`activity/${origin}/${moment(date).format("yyyy-MM-DD")}`);
        return res;
    }

    return (
        <>
            <div className={classes.Activity}>
                <h1>Activities</h1>
            </div>
        </>
    );
}

export default Activity;
