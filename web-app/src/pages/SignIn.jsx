import React, {Component, useContext, useState} from 'react';
import {Link, useHistory, withRouter} from "react-router-dom";
import PropTypes from 'prop-types';
import classes from './SignIn.module.css';
import axios from 'axios';
import {BACKEND} from "../constants/Constants";
import Icon from "../components/Icon";
import * as Validator from "../helpers/Validator";
import {AppContext} from "../context/AppContext";

const SignIn = () => {
    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [emailErrMsg, setemailErrMsg] = useState('');
    const [pwdErrMsg, setpwdErrMsg] = useState('');
    const [submitErrMsg, setsubmitErrMsg] = useState('');
    const {setAuthToken} = useContext(AppContext);
    const history = useHistory();

    /**
     * Tries to sign in the user with input credentials
     * If the credentials are invalid then shows and error message else redirects the user to the homepage after login
     */
    const handleSubmit = async (event) => {
        event.preventDefault();
        if (email != "" && password != "" && emailErrMsg == "" && pwdErrMsg == "") {
            try {
                const res = await axios.post(BACKEND + "login", {
                    email: email,
                    password: password
                })
                if (res.status == 200) {
                    setAuthToken(res.data.message);
                    localStorage.setItem("authToken", res.data.message)
                    history.push("/summary");
                }
            } catch (e) {
                console.log(e);
            }
        }
    }

    /**
     * Handles on blue event of the email input
     * If the email is invalid then shows an error message
     */
    const handleEmailInputOnBlur = (event) => {
        const eventValue = event.target.value;
        if (eventValue != "") {
            if (!Validator.isEmailFormatValid(event.target.value)) {
                setemailErrMsg("E-mail is invalid");
            }
            else {
                setemailErrMsg("");
            }
        }
    }

    /**
     * Handles on blue event of the password input
     * If the email is invalid then shows an error message
     */
    const handlePasswordInputOnBlur = (event) => {
        const eventValue = event.target.value;
        if (eventValue != "") {
            if (eventValue.length < 8) {
                setpwdErrMsg("Password is too short");
            }
            else {
                setpwdErrMsg("");
            }
        }
    }

    return (
        <main className={classes.SignIn}>
            <div className={classes.FormContainer}>
                <form className={classes.Form}
                      onSubmit={handleSubmit}>
                    <h1>Sign In</h1>
                    <p className={classes.ErrMsgSubmit}>{submitErrMsg}</p>
                    <label htmlFor={"email"} className={classes.EmailLabel}>
                        <svg className={classes.EmailIcon}></svg>
                        <input placeholder={"E-mail"} id={"email"} name={"email"} onChange={(event) => {setEmail(event.target.value)}} onBlur={handleEmailInputOnBlur} type="text"
                               spellCheck={"false"} autoFocus/>
                    </label>
                    <span className={classes.ErrMsg}>{emailErrMsg}</span>
                    <label htmlFor={"password"} className={classes.PasswordLabel}>
                        <svg className={classes.PasswordIcon}></svg>
                        <input placeholder={"Password"} id={"password"} name={"password"} onChange={(event) => {setPassword(event.target.value)}} onBlur={handlePasswordInputOnBlur}
                               type="password" spellCheck={"false"}/>
                    </label>
                    <span className={classes.ErrMsg}>{pwdErrMsg}</span>
                    <button type={"submit"}>Sign In</button>
                </form>
            </div>
        </main>
    );
}

export default withRouter(SignIn);
