import React, {Component, useContext, useEffect, useState} from 'react';
import {Link, useParams, withRouter} from "react-router-dom";
import PropTypes from 'prop-types';
import classes from './SignIn.module.css';
import axios from 'axios';
import {BACKEND} from "../constants/Constants";
import Icon from "../components/Icon";
import * as Validator from "../helpers/Validator";
import {AppContext} from "../context/AppContext";
import TopBar from "../components/TopBar";

const Summary = () => {
    const {origin, setOrigin, fetchData, checkOriginStatus} = useContext(AppContext);
    const {log_date} = useParams();
    const [date, setDate] = useState(log_date);

    useEffect(() => {
        checkOriginStatus();
        fetch().then(result => {
            console.log(result.data)
        })
    });

    const fetch = async () => {
        const res = await fetchData(`daily-summary/${origin}/${date}`);
        return res;
    }

    return (
        <>
            <TopBar/>
            <h1>Daily Summary</h1>
        </>
    );
}

export default Summary;
