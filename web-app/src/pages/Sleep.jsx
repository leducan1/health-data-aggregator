import React, {Component, useContext, useEffect} from 'react';
import {Link, withRouter} from "react-router-dom";
import PropTypes from 'prop-types';
import classes from './SignIn.module.css';
import axios from 'axios';
import {BACKEND} from "../constants/Constants";
import Icon from "../components/Icon";
import * as Validator from "../helpers/Validator";
import {AppContext} from "../context/AppContext";

const Sleep = () => {
    const {origin, setOrigin, fetchData, checkOriginStatus} = useContext(AppContext);

    useEffect(() => {
        checkOriginStatus();
        fetch().then(result => {
            console.log(result.data)
        })
    });

    const fetch = async () => {
        const res = await fetchData(`sleep/${origin}/2021-03-18`);
        return res;
    }

    return (
        <>
            <h1>Sleep</h1>
        </>
    );
}

export default Sleep;
