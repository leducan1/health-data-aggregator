import React, {Component, useContext, useEffect} from 'react';
import {Link, useParams, withRouter} from "react-router-dom";
import PropTypes from 'prop-types';
import classes from './SignIn.module.css';
import axios from 'axios';
import {BACKEND} from "../constants/Constants";
import Icon from "../components/Icon";
import * as Validator from "../helpers/Validator";
import {AppContext} from "../context/AppContext";
import Navigation from "../components/Navigation";

const BloodPressure = () => {
    const {origin, setOrigin, fetchData, checkOriginStatus} = useContext(AppContext);
    const {url_date} = useParams();
    useEffect(() => {
        checkOriginStatus();
        fetch().then(result => {
            console.log(result.data)
        })
    });

    const fetch = async () => {
        const res = await fetchData(`blood-pressure/${origin}/2021-03-05`);
        return res;
    }

    return (
        <>
            <h1>Blood Pressure</h1>
        </>
    );
}

export default BloodPressure;
