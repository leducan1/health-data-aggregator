export const MAX_NUMBER_OF_COLORS = 10;
export const MIN_NUMBER_OF_COLORS = 1;

export const COLOR_TINTS_PERCENTAGE = 3.5;
export const COLOR_SHADES_PERCENTAGE = 3.5;

export const PASSWORD_LENGTH = 6;

export const BACKEND = "http://localhost:8080/";

export const originType = {
    FITBIT: "fitbit",
    GOOGLE: "google",
    THIS: "this",
    SAMSUNG: "samsung",
};
