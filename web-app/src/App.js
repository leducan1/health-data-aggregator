import {BrowserRouter as Router, Route, Switch, useHistory, useParams} from "react-router-dom";
import './App.css';
import React, {useEffect, useState} from "react";
import SignIn from "./pages/SignIn";
import axios from "axios";
import {AppContext} from "./context/AppContext";
import {BACKEND, originType} from "./constants/Constants";
import Water from "./pages/Water";
import Summary from "./pages/Summary";
import Activity from "./pages/Activity";
import Sleep from "./pages/Sleep";
import Weight from "./pages/Weight";
import BloodPressure from "./pages/Blood-Pressure";
import HeartRate from "./pages/HeartRate";
import Navigation from "./components/Navigation";
import TopBar from "./components/TopBar";

const App = () => {
    useEffect(() => {
        console.log("app use effect");
    })

    const fetchData = async (endpoint) => {
        const response = await axios.get(BACKEND + `${endpoint}`, {
            headers: {
                // Authorization: `Bearer ${authToken}`
            }
        });
        return response;
    }

    return (
        <div className="App">
            <Router>
                <Switch>
                    <Navigation/>
                    <TopBar/>
                    {/*<Route exact path={"/summary"} component={() => <Summary/>}/>*/}
                    {/*<Route exact path={"/activity"} component={() => <Activity/>}/>*/}
                    {/*<Route exact path={"/sleep"} component={() => <Sleep/>}/>*/}
                    {/*<Route exact path={"/water"} component={() => <Water/>}/>*/}
                    {/*<Route exact path={"/weight"} component={() => <Weight/>}/>*/}
                    {/*<Route exact path={"/blood-pressure"} component={() => <BloodPressure/>}/>*/}
                    {/*<Route exact path={"/heart-rate"} component={() => <HeartRate/>}/>*/}
                    {/*<Route exact path={"/food"} component={() => <SignIn/>}/>*/}
                </Switch>
            </Router>
        </div>
    );
}

export default App;
