package springboot.application.healthdataapp.request;

import lombok.Getter;

import java.io.Serializable;

/**
 * request body containing auth code
 */
@Getter
public class AuthCodeRequest implements Serializable {
    private String authCode;

    public AuthCodeRequest() {

    }
}
