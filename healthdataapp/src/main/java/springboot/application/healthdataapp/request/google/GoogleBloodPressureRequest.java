package springboot.application.healthdataapp.request.google;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.Map;

/**
 * rquest to get blood pressure data from google
 */
public class GoogleBloodPressureRequest {
    Map<?, ?>[] aggregateBy = {
            Map.ofEntries(
                    Map.entry("dataTypeName", "com.google.weight"),
                    Map.entry("dataSourceId", "derived:com.google.blood_pressure:com.google.android.gms:merged")
            )
    };

    long endTimeMillis;
    long startTimeMillis;

    public GoogleBloodPressureRequest(String date) {
        String newdate = date + " 00:00:00";
        LocalDateTime localDateTime = LocalDateTime.parse(newdate, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        startTimeMillis = (localDateTime.toEpochSecond(ZoneOffset.UTC) * 1000);
        endTimeMillis = startTimeMillis + (86400000); //1 day
    }

    public GoogleBloodPressureRequest(String startDate, String endDate) {
        startDate = startDate + " 00:00:00";
        endDate = endDate + " 00:00:00";
        LocalDateTime localStartDateTime = LocalDateTime.parse(startDate, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        LocalDateTime localEndDateTime = LocalDateTime.parse(endDate, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        startTimeMillis = (localStartDateTime.toEpochSecond(ZoneOffset.UTC) * 1000);
        endTimeMillis = (localEndDateTime.toEpochSecond(ZoneOffset.UTC) * 1000);
    }
}