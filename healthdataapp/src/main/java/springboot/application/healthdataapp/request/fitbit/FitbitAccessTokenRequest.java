package springboot.application.healthdataapp.request.fitbit;

public class FitbitAccessTokenRequest {
    private String grant_type = "authorization_code";
    private String redirect_uri = "http://localhost:3000/fitbit/auth/response";
    private String code;

    public FitbitAccessTokenRequest(String code) {
        this.code = code;
    }
}
