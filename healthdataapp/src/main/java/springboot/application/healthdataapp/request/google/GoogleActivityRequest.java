package springboot.application.healthdataapp.request.google;

import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.Map;

/**
 * request to get activity data from google servers
 */
public class GoogleActivityRequest {
    Map<?, ?>[] aggregateBy = {
            Map.ofEntries(
                    Map.entry("dataTypeName", "com.google.step_count.delta"),
                    Map.entry("dataSourceId", "derived:com.google.step_count.delta:com.google.android.gms:estimated_steps")
            ),
            Map.ofEntries(
                    Map.entry("dataTypeName", "com.google.calories.expended"),
                    Map.entry("dataSourceId", "derived:com.google.calories.expended:com.google.android.gms:platform_calories_expended")
            ),
            Map.ofEntries(
                    Map.entry("dataTypeName", "merge_distance_delta"),
                    Map.entry("dataSourceId", "derived:com.google.distance.delta:com.google.android.gms:merge_distance_delta")
            ),
            Map.ofEntries(
                    Map.entry("dataTypeName", "com.google.speed"),
                    Map.entry("dataSourceId", "derived:com.google.speed:com.google.android.gms:from_distance<-merge_distance_delta")
            ),
            Map.ofEntries(
                    Map.entry("dataTypeName", "com.google.speed"),
                    Map.entry("dataSourceId", "derived:com.google.speed:com.google.android.gms:merge_speed")
            ),
            Map.ofEntries(
                    Map.entry("dataTypeName", "com.google.active_minutes"),
                    Map.entry("dataSourceId", "derived:com.google.active_minutes:com.google.android.gms:merge_active_minutes")
            )
    };

    long endTimeMillis;
    long startTimeMillis;

    Map<String, String> bucketBySession = Map.ofEntries(
            Map.entry("minDurationMillis", "0")
    );

    public GoogleActivityRequest(String startDate, String endDate) {
        startDate = startDate + " 00:00:00";
        endDate = endDate + " 00:00:00";
        LocalDateTime localStartDateTime = LocalDateTime.parse(startDate, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        LocalDateTime localEndDateTime = LocalDateTime.parse(endDate, DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
        startTimeMillis = (localStartDateTime.toEpochSecond(ZoneOffset.UTC) * 1000);
        endTimeMillis = (localEndDateTime.toEpochSecond(ZoneOffset.UTC) * 1000);
    }
}