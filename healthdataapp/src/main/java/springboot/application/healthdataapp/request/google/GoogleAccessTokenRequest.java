package springboot.application.healthdataapp.request.google;

/**
 * request to google servers for receiving access token
 */
public class GoogleAccessTokenRequest {
    private String client_id = "807765000508-ia0jgufgiol7ampbmuh9pqccvi41gd4u.apps.googleusercontent.com";
    private String client_secret = "bAdS_qiQL4HUtATn4JDmqFWb";
    private String grant_type = "authorization_code";
    private String redirect_uri = "http://localhost:3000/google/auth/response";
    private String code;

    public GoogleAccessTokenRequest(String code) {
        this.code = code;
    }
}
