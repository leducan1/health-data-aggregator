package springboot.application.healthdataapp.model.heart;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import springboot.application.healthdataapp.model.type.TokenType;
import springboot.application.healthdataapp.model.user.User;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.time.LocalDateTime;

@Getter
@Setter
@Entity
public class HeartRate {
    @Id @GeneratedValue
    private Integer id;

    @NotNull
    private LocalDateTime startDateTime;

    @NotNull
    private LocalDateTime endDateTime;

    @NotNull
    private LocalDate date;

    @NotNull
    @Min(value = 0, message = "Heart rate value should be at least 0")
    private Integer value;

    @NotNull
    @Enumerated(EnumType.STRING)
    private TokenType origin;

    @ManyToOne
    @JsonIgnore
    private User user;
}