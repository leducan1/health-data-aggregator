package springboot.application.healthdataapp.model.weight;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import springboot.application.healthdataapp.model.type.TokenType;
import springboot.application.healthdataapp.model.user.User;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.time.LocalTime;

@Getter
@Setter
@Entity
public class WeightLog {
    @Id @GeneratedValue
    private Integer id;

    @NotNull(message = "Date must be set")
    private LocalDate date;

    private LocalTime time;

    @Min(value = 0, message = "BMI should be between 0 and 40")
    @Max(value = 40, message = "BMI should be between 0 and 40")
    private Double bmi;

    @NotNull(message = "Weight value must be set")
    @Min(value = 0, message = "Weight should be between 0 and 1000")
    @Max(value = 1000, message = "Weight should be between 0 and 1000")
    private Double weightInKg;

    @Min(value = 0, message = "Body fat percentage should be between 0 and 100")
    @Max(value = 100, message = "Body fat percentage should be between 0 and 100")
    private Double bodyFatPercentage;

    @Enumerated(EnumType.STRING)
    @NotNull
    private TokenType origin;

    @ManyToOne
    @NotNull
    @JsonIgnore
    private User user;
}
