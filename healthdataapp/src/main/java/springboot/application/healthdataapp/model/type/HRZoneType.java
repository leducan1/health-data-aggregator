package springboot.application.healthdataapp.model.type;

import lombok.AllArgsConstructor;
import lombok.ToString;

@ToString
@AllArgsConstructor
public enum HRZoneType {
    OUT_OF_RANGE("OUT_OF_RANGE"), CARDIO("CARDIO"), PEAK("PEAK"), FAT_BURN("FAT_BURN");

    private final String name;
}
