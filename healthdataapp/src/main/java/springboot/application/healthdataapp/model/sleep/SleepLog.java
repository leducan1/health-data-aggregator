package springboot.application.healthdataapp.model.sleep;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
//import org.springframework.data.mongodb.core.mapping.Document;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.relational.core.sql.In;
import springboot.application.healthdataapp.model.type.TokenType;
import springboot.application.healthdataapp.model.user.User;

import javax.persistence.*;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.util.Date;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.ArrayList;
import java.util.List;

@Getter
@Setter
@Entity
public class SleepLog {
    @Id @GeneratedValue
    private Integer id;

    @NotNull
    private LocalDate date;

    @NotNull
    private LocalDateTime startDateTime;

    @NotNull
    private LocalDateTime endDateTime;

    @Min(value = 0, message = "Duration should be at least 0")
    private Integer durationInSeconds;

    @Min(value = 0, message = "Seconds asleep should be at least 0")
    private Integer secondsAsleep;

    @Min(value = 0, message = "Seconds awake should be at least 0")
    private Integer secondsAwake;

    @Max(value = 100, message = "Efficiency should be between 0 and 100")
    @Min(value = 0, message = "Efficiency should be between 0 and 100")
    private Integer efficiency;

    @OneToMany(cascade = CascadeType.ALL, orphanRemoval = true, mappedBy = "sleepLog")
    private List<SleepLevel> sleepLevels = new ArrayList<>();

    @NotNull
    @Enumerated(EnumType.STRING)
    private TokenType origin;

    @NotNull
    @JsonIgnore
    @ManyToOne
    private User user;

    public void addSleepLogLevel(SleepLevel sleepLogLevel) {
        if (sleepLevels == null) sleepLevels = new ArrayList<>();
        sleepLevels.add(sleepLogLevel);
    }
}
