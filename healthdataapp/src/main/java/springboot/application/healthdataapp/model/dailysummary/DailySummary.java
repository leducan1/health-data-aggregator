package springboot.application.healthdataapp.model.dailysummary;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import springboot.application.healthdataapp.model.activity.ActivityMinutes;
import springboot.application.healthdataapp.model.type.TokenType;
import springboot.application.healthdataapp.model.user.User;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Getter
@Setter
@Entity
public class DailySummary {
    @Id @GeneratedValue
    private Integer id;

    @Enumerated(EnumType.STRING)
    private TokenType origin;

    @NotNull(message = "Date must be set")
    private LocalDate date;

    @Min(value = 0, message = "Steps must be at least than 0")
    private Integer steps;

    @Min(value = 0, message = "Calories must be at least than 0")
    private Integer calories;

    @Min(value = 0, message = "Calories from activities must be at least than 0")
    private Integer caloriesActivities;

    @Min(value = 0, message = "Distance must be at least than 0")
    private Double distance;

    @Min(value = 0, message = "Elevation must be at least than 0")
    private Double elevation;

    @Min(value = 0, message = "Floors must be at least than 0")
    private Double floors;

    @NotNull
    @JsonIgnore
    @ManyToOne
    private User user;
}
