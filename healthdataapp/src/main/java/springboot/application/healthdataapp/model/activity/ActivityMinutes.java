package springboot.application.healthdataapp.model.activity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import springboot.application.healthdataapp.model.activity.ActivityLog;
import springboot.application.healthdataapp.model.type.TokenType;

import javax.persistence.*;
import java.util.Date;

@Entity
@Getter
@Setter
public class ActivityMinutes {
    @Id @GeneratedValue
    private Integer id;

    private Integer lightly;

    private Integer fairly;

    private Integer very;

    private Integer sedentary;
}