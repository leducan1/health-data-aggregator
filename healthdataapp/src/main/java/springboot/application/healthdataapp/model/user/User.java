package springboot.application.healthdataapp.model.user;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.annotation.CreatedDate;
//import org.springframework.data.mongodb.core.mapping.Document;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import springboot.application.healthdataapp.model.type.Role;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;
import java.util.Date;

@Getter
@Setter
@Entity
@Table(name = "users")
@EntityListeners(AuditingEntityListener.class)
public class User {
    @Id @GeneratedValue
    private Integer id;

    @NotNull(message = "E-mail must be set")
    @Email(message = "Must be an e-mail")
    @Size(min = 5, max = 320, message = "E-mail must have between 5 and 320 characters")
    private String email;

    @NotNull(message = "Password must be set")
    @Size(min = 6, max = 128, message = "Password must have between 6 and 128 characters")
    private String password;

    @CreatedDate
    private Date createDate;

    @NotNull
    private Role role;
}
