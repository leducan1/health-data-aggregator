package springboot.application.healthdataapp.model.activity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import org.springframework.data.jpa.domain.support.AuditingEntityListener;
import springboot.application.healthdataapp.model.type.TokenType;
import springboot.application.healthdataapp.model.user.User;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.PositiveOrZero;
import javax.validation.constraints.Size;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;


@Getter
@Setter
@Entity
@EntityListeners(AuditingEntityListener.class)
public class ActivityLog {
    @Id @GeneratedValue
    private Integer id;

    @Size(min = 1, max = 120, message = "Name should be between 1 and 120 characters long")
    private String name;

    @Size(max = 160)
    private String description;

    @Min(value = 0, message = "Calories should be at least 0")
    private Integer caloriesOut;

    @Min(value = 0, message = "Steps should be at least 0")
    private Integer steps;

    @NotNull
    @Min(value = 0, message = "Duration should be at least 0")
    private Integer durationInSeconds;

    @NotNull
    private LocalDateTime startDateTime;

    @NotNull
    private LocalDate date;

    @Min(value = 0, message = "Distance should be at least 0")
    private Double distanceInMeters;

    @Min(value = 0, message = "Speed should be at least 0")
    private Double speed;

    @OneToMany(cascade = CascadeType.ALL, mappedBy = "activityLog")
    private List<HRZone> hrZones;

    @NotNull 
    @Enumerated(EnumType.STRING)
    private TokenType origin;

    @OneToOne(cascade = CascadeType.ALL)
    private ActivityMinutes activityMinutes;

    @JsonIgnore 
    @NotNull 
    @ManyToOne
    private User user;
}