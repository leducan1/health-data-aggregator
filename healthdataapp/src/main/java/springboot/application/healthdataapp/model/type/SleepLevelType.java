package springboot.application.healthdataapp.model.type;

import lombok.AllArgsConstructor;
import lombok.ToString;

@ToString
@AllArgsConstructor
public enum SleepLevelType {
    RESTLESS("RESTLESS"), AWAKE("AWAKE"), ASLEEP("ASLEEP"), DEEP("DEEP_SLEEP"), LIGHT("LIGHT_SLEEP"), REM("REM"), WAKE("WAKE");

    private final String name;
}
