package springboot.application.healthdataapp.model.user;

import lombok.Data;
//import org.springframework.data.mongodb.core.mapping.Document;
import lombok.Getter;
import lombok.Setter;
import springboot.application.healthdataapp.model.type.TokenType;
import springboot.application.healthdataapp.model.user.User;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@Entity
public class AuthCredentials {
    @Id @GeneratedValue
    private Integer id;

    @NotNull
    @Column(length = 2048)
    private String accessToken;

    @NotNull
    @Column(length = 2048)
    private String refreshToken;

    @Column(length = 2048)
    private String scope;

    @Enumerated(EnumType.STRING)
    private TokenType tokenType;

    @ManyToOne
    private User user;
}