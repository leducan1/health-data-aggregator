package springboot.application.healthdataapp.model.blood;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Getter;
import lombok.Setter;
import springboot.application.healthdataapp.model.type.TokenType;
import springboot.application.healthdataapp.model.user.User;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;
import java.time.LocalTime;

@Entity
@Getter
@Setter
public class BloodPressure {
    @Id @GeneratedValue
    private Integer id;

    @NotNull(message = "Systolic value must be set")
    @Min(value = 0, message = "Systolic value should be at least 0")
    private Integer systolic;

    @NotNull(message = "Diastolic value must be set")
    @Min(value = 0, message = "Diastolic value should be at least 0")
    private Integer diastolic;

    @NotNull(message = "Date must be set")
    private LocalDate date;

    @NotNull(message = "Time must be set")
    private LocalTime time;

    @ManyToOne
    @JsonIgnore
    private User user;

    @NotNull
    @Enumerated(EnumType.STRING)
    private TokenType origin;
}
