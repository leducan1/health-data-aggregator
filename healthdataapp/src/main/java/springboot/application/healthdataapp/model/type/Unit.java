package springboot.application.healthdataapp.model.type;

import lombok.AllArgsConstructor;
import lombok.ToString;

@ToString
@AllArgsConstructor
public enum Unit {
    BAR("BAR"),
    BOTTLE("BOTTLE"),
    ML("ML"),
    BOX("BOX"),
    CAN("CAN"),
    CONTAINER("CONTAINER"),
    CUBE("CUBE"),
    CUP("CUP"),
    FLUID_OZ("FLUID_OZ"),
    GRAM("GRAM"),
    JAR("JAR"),
    SERVING("SERVING"),
    OTHER("OTHER");

    private final String name;
}