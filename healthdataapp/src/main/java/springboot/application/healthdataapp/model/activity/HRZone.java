package springboot.application.healthdataapp.model.activity;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import springboot.application.healthdataapp.model.type.HRZoneType;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Getter
@Setter
@Entity
@Table(name = "HR_ZONE")
public class HRZone {
    @Id
    @GeneratedValue
    private Integer id = 0;

    private Integer caloriesOut;

    @NotNull
    private Integer max = 0;

    @NotNull
    private Integer min = 0;

    @NotNull
    private Integer minutes = 0;

    @NotNull
    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "activity_log_id")
    private ActivityLog activityLog;

    @NotNull
    @Enumerated(EnumType.STRING)
    private HRZoneType HRZoneType;
}
