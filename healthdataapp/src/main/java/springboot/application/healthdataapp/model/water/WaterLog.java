package springboot.application.healthdataapp.model.water;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import springboot.application.healthdataapp.model.type.TokenType;
import springboot.application.healthdataapp.model.type.Unit;
import springboot.application.healthdataapp.model.user.User;

import javax.persistence.*;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.time.LocalDate;

@Getter
@Setter
@Entity
public class WaterLog {
    @Id @GeneratedValue
    private Integer id;

    @NotNull(message = "Date must be set")
    private LocalDate date;

    @NotNull(message = "Amount must be set")
    @Min(value = 0, message = "Amount must be at least 0")
    private Double amount = 0.0;

    @NotNull
    @Enumerated(EnumType.STRING)
    private Unit unit;

    @NotNull
    @Enumerated(EnumType.STRING)
    private TokenType origin;

    @ManyToOne
    @NotNull
    @JsonIgnore
    private User user;

    public WaterLog() {
        unit = Unit.ML;
    }

    public void addAmount(double amount) {
        this.amount += amount;
    }
}
