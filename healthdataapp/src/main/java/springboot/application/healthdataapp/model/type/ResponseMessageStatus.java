package springboot.application.healthdataapp.model.type;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public enum ResponseMessageStatus {
    /* auth */
    ACCOUNT_ALREADY_EXISTS("ACCOUNT_ALREADY_EXISTS"),
    ACCOUNT_CREATED("ACCOUNT_CREATED"),
    BAD_CREDENTIALS("BAD_CREDENTIALS"),
    AUTHORIZED("AUTHORIZED"),
    UNAUTHORIZED("UNAUTHORIZED"),

    /* errors */
    TOKEN_NOT_FOUND("TOKEN_NOT_FOUND"),
    INVALID_REFRESH_TOKEN("INVALID_REFRESH_TOKEN"),
    ACCESS_TOKEN_NOT_GRANTED("ACCESS_TOKEN_NOT_GRANTED"),
    DATA_NOT_FOUND("DATA_NOT_FOUND"),
    INVALID_ENDPOINT_REQUEST("INVALID_REQUEST"),

    /* ok */
    DATA_CREATED("DATA_CREATED"),
    DATA_SAVED("DATA_SAVED"),
    DATA_NOT_SAVED("DATA_NOT_SAVED");

    private final String name;
}
