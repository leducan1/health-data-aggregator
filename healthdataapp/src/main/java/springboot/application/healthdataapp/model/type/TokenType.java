package springboot.application.healthdataapp.model.type;

import lombok.AllArgsConstructor;
import lombok.ToString;

@ToString
@AllArgsConstructor
public enum TokenType {
    GOOGLE("GOOGLE"), FITBIT("FITBIT"), SAMSUNG("SAMSUNG"), THIS("THIS"), ALL("ALL");

    private final String name;
}
