package springboot.application.healthdataapp.model.sleep;

import com.fasterxml.jackson.annotation.JsonIgnore;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import springboot.application.healthdataapp.model.type.SleepLevelType;

import javax.persistence.*;
import java.time.LocalDateTime;

@Getter
@Setter
@Entity
public class SleepLevel {
    @Id @GeneratedValue
    private Integer id;

    private LocalDateTime starDateTime;

    private LocalDateTime endDateTime;

    private SleepLevelType levelType;

    private Integer durationInSeconds;

    @JsonIgnore
    @ManyToOne
    @JoinColumn(name = "sleep_log_id")
    private SleepLog sleepLog;
}
