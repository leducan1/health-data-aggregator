package springboot.application.healthdataapp.config;

import org.springframework.security.crypto.bcrypt.BCrypt;

public abstract class PasswordHash {
    public static String hashPassword(String password){
        return BCrypt.hashpw(password, BCrypt.gensalt());
    }

    public static boolean verifyPassword(String userInput, String hashedPassword){
        return BCrypt.checkpw(userInput, hashedPassword);
    }
}
