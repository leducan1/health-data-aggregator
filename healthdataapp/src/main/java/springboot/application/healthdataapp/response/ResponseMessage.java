package springboot.application.healthdataapp.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.Getter;
import lombok.Setter;
import springboot.application.healthdataapp.model.type.ResponseMessageStatus;

/**
 * response body with code, message and status
 */
@AllArgsConstructor
@Getter
@Setter
public class ResponseMessage {
    public int code;
    public String message;
    public ResponseMessageStatus status;
}
