package springboot.application.healthdataapp.response;

import java.io.Serializable;

/**
 * response body with jwt token
 */
public class AuthenticationResponse implements Serializable {
    private final String jwtToken;

    public AuthenticationResponse(String jwtToken) {
        this.jwtToken = jwtToken;
    }

    public String getJwtToken() {
        return jwtToken;
    }
}
