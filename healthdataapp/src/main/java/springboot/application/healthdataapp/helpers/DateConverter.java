package springboot.application.healthdataapp.helpers;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Date;

public abstract class DateConverter {

    /**
     * converts string to local date time
     * @param date date as a string
     * @param format format of the string
     * @return local date time object
     */
    public static LocalDateTime stringToLocalDateTime(String date, String format) {
        date = date.replace("T", " ");
        date = date.replace(".000", "");

        DateTimeFormatter formatter = DateTimeFormatter.ofPattern(format);
        return LocalDateTime.parse(date, formatter);
    }

    /**
     * converts string to local date
     * @param date string yyyy-MM-dd format
     * @return local date object
     */
    public static LocalDate stringToLocalDate(String date) {
        date = date.replace("T", " ");
        date = date.replace(".000", "");

        return LocalDate.parse(date);
    }

    /**
     * converts string to local date
     * @param date date as a string
     * @param pattern format of the string
     * @return local date object
     */
    public static LocalDate stringToLocalDate(String date, String pattern) {
        date = date.replace(".000", "");
        return LocalDate.parse(date, DateTimeFormatter.ofPattern(pattern));
    }

    /**
     * converts string to local time
     * @param date date as a string
     * @param pattern format of the string
     * @return local time object
     */
    public static LocalTime stringToLocalTime(String date, String pattern) {
        date = date.replace(".000", "");
        return LocalTime.parse(date, DateTimeFormatter.ofPattern(pattern));
    }

    /**
     * convers string to local time
     * @param time string in yyyy-MM-dd format
     * @return local time object
     */
    public static LocalTime stringToLocalTime(String time) {
        return LocalTime.parse(time);
    }

    /**
     * Creates local datetime from unix timestamp
     * @param millis unix timestamp
     * @return local time in yyyy-MM-dd HH:mm:ss format
     */
    public static LocalDateTime millisToDateTime(long millis) {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
        String dateTime = df.format(new Date(millis));

        return stringToLocalDateTime(dateTime, "yyyy-MM-dd HH:mm:ss");
    }

    /**
     * Creates local time from unix timestamp
     * @param millis unix timestamp
     * @return local time in HH:mm:ss format
     */
    public static LocalTime millisToTime(long millis) {
        DateFormat df = new SimpleDateFormat("HH:mm:ss");
        String time = df.format(new Date(millis));

        return LocalTime.parse(time);
    }

    /**
     * Creates local date from unix timestamp
     * @param millis unix timestamp
     * @return local date in yyyy-MM-dd format
     */
    public static LocalDate millisToDate(long millis) {
        DateFormat df = new SimpleDateFormat("yyyy-MM-dd");
        String date = df.format(new Date(millis));

        return LocalDate.parse(date);
    }
}
