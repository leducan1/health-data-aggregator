package springboot.application.healthdataapp.helpers;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.ValidatorFactory;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public abstract class ObjValidator {
    ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
    javax.validation.Validator validator = factory.getValidator();

    /**
     * get a list of constraint violations for a generic object
     * @param object generic object
     * @param <T> object class
     * @return list of constaint violations
     */
    public static <T> List<String> getConstraintViolations(T object) {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        javax.validation.Validator validator = factory.getValidator();

        Set<ConstraintViolation<T>> violations = validator.validate(object);

        List<String> violationList = new ArrayList<>();
        if (!violations.isEmpty()) {
            violations.forEach(violation -> {
                violationList.add(violation.getMessage());
            });
        }
        return violationList;
    }
}
