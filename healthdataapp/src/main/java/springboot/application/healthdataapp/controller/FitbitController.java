package springboot.application.healthdataapp.controller;

import net.minidev.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import springboot.application.healthdataapp.request.AuthCodeRequest;
import springboot.application.healthdataapp.service.sync.FitbitSyncService;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/fitbit")
@CrossOrigin(origins = "*")
@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
public class FitbitController extends BaseController {
    @Autowired private FitbitSyncService fitbitSyncService;

    /**
     * get a link to fitbit consent screen
     * @return response with consent screen URL
     */
    @GetMapping(value = "/auth")
    public JSONObject getLinkToConsentScreen() {
        return fitbitSyncService.getConsentScreenURL();
    }

    /**
     * get an access token from an authorization code
     * @param req HTTP request
     * @param authCodeRequest object containing auth code
     * @return access token
     */
    @PostMapping(value = "/auth")
    public ResponseEntity<?> getAuthTokenWithAuthCode(HttpServletRequest req, @RequestBody AuthCodeRequest authCodeRequest) {
        return fitbitSyncService.getAccessToken(getUserFromRequest(req), authCodeRequest);
    }

    /**
     * sync data from fitbit from a specified date range
     * @param req HTTP request
     * @param startdate yyyy-MM-dd format
     * @param enddate yyyy-MM-dd format
     * @return response with status code, and message
     */
    @GetMapping(value = "/sync/{startdate}/{enddate}")
    public ResponseEntity<?> synchFromDateRange(HttpServletRequest req, @PathVariable String startdate, @PathVariable String enddate) {
        return fitbitSyncService.syncFromDateRange(getUserFromRequest(req), startdate, enddate);
    }
}
