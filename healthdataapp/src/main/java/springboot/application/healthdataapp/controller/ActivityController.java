package springboot.application.healthdataapp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import springboot.application.healthdataapp.model.activity.ActivityLog;
import springboot.application.healthdataapp.service.ActivityService;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/activity")
@CrossOrigin(origins = "*")
@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
public class ActivityController extends BaseController {
    @Autowired ActivityService activityService;

    /**
     * add activity log to this server
     * @param req HTTP request
     * @param activity activity object
     * @param date yyyy-MM-dd format
     * @return response with status and message
     */
    @PostMapping(value = "/this/{date}")
    public ResponseEntity<?> addActivity(HttpServletRequest req, @RequestBody ActivityLog activity, @PathVariable String date) {
        return activityService.addActivityLog(activity, getUserFromRequest(req), date);
    }

    /**
     * get activities from a specified date
     * @param req HTTP request
     * @param date yyyy-MM-dd format
     * @param from data origin
     * @return response status, and response body
     */
    @GetMapping(value = "/{from}/{date}")
    public ResponseEntity<?> getDailyActivities(HttpServletRequest req, @PathVariable String date, @PathVariable String from) {
        return ResponseEntity.status(HttpStatus.OK).body(
                activityService.getActivityLogs(getUserFromRequest(req), getOrigin(from), date)
        );
    }
}
