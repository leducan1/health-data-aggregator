package springboot.application.healthdataapp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import springboot.application.healthdataapp.service.DailySummaryService;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/daily-summary")
@CrossOrigin(origins = "*")
@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
public class DailySummaryController extends BaseController {
    @Autowired DailySummaryService dailySummaryService;

    /**
     * get daily summary from a specified date
     * @param req HTTP request
     * @param date yyyy-MM-dd format
     * @param from data origin
     * @return response status, and response body
     */
    @GetMapping(value = "/{from}/{date}")
    public ResponseEntity<?> getDailySummary(HttpServletRequest req, @PathVariable String from, @PathVariable String date) {
        return ResponseEntity.status(HttpStatus.OK).body(
                dailySummaryService.getDailySummaryLog(getUserFromRequest(req), getOrigin(from), date)
        );
    }
}
