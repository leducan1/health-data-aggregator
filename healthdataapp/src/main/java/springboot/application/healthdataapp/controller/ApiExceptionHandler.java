package springboot.application.healthdataapp.controller;

import com.google.gson.Gson;
import io.jsonwebtoken.ExpiredJwtException;
import io.jsonwebtoken.JwtException;
import io.jsonwebtoken.MalformedJwtException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;
import springboot.application.healthdataapp.exception.*;
import springboot.application.healthdataapp.model.type.ResponseMessageStatus;
import springboot.application.healthdataapp.response.ResponseMessage;

import javax.validation.ConstraintViolationException;
import java.util.Date;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@ControllerAdvice
@CrossOrigin(origins = "*")
public class ApiExceptionHandler extends ResponseEntityExceptionHandler {
    Gson gson = new Gson();

    @ExceptionHandler(value = {JwtException.class})
    protected ResponseEntity<Object> handleExpiredJwtException(JwtException ex, WebRequest request) {
        return new ResponseEntity<>(gson.toJson(new ResponseMessage(401, ex.getMessage(), ResponseMessageStatus.UNAUTHORIZED)), HttpStatus.UNAUTHORIZED);
    }

    @ExceptionHandler(value = {InvalidEndpointRequestException.class})
    protected ResponseEntity<Object> handleInvalidEndpointRequestException(InvalidEndpointRequestException ex, WebRequest request) {
        return new ResponseEntity<>(gson.toJson(ex.getError()), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = {LogsNotFoundException.class})
    protected ResponseEntity<Object> handleLogsNotFoundException(LogsNotFoundException ex, WebRequest request) {
        return new ResponseEntity<>(gson.toJson(ex.getError()), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(value = {AccessTokenNotFoundException.class})
    protected ResponseEntity<Object> handleAccessTokenNotFoundException(AccessTokenNotFoundException ex, WebRequest request) {
        return new ResponseEntity<>(gson.toJson(ex.getError()), HttpStatus.NOT_FOUND);
    }

    @ExceptionHandler(value = {AccessTokenNotGrantedException.class})
    protected ResponseEntity<Object> handleAccessTokenNotGrantedException(AccessTokenNotGrantedException ex, WebRequest request) {
        return new ResponseEntity<>(gson.toJson(ex.getError()), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = {InvalidRefreshTokenException.class})
    protected ResponseEntity<Object> handleInvalidRefreshTokenException(InvalidRefreshTokenException ex, WebRequest request) {
        return new ResponseEntity<>(gson.toJson(ex.getError()), HttpStatus.BAD_REQUEST);
    }

    @ExceptionHandler(value = {BadCredentialsException.class})
    protected ResponseEntity<Object> handleInvalidRefreshTokenException(BadCredentialsException ex, WebRequest request) {
        return new ResponseEntity<>(gson.toJson(ex.getError()), HttpStatus.UNAUTHORIZED);
    }

    @ExceptionHandler(value = {ConstraintViolationException.class})
    protected ResponseEntity<Object> handleInvalidRefreshTokenException(ConstraintViolationException ex, WebRequest request) {
        return new ResponseEntity<>(gson.toJson(ex.getConstraintViolations()), HttpStatus.INTERNAL_SERVER_ERROR);
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex, HttpHeaders headers, HttpStatus status, WebRequest request) {

        Map<String, Object> body = new LinkedHashMap<>();
        body.put("timestamp", new Date());
        body.put("status", status.value());

        //Get all errors
        List<String> errors = ex.getBindingResult()
                .getFieldErrors()
                .stream()
                .map(x -> x.getDefaultMessage())
                .collect(Collectors.toList());

        body.put("errors", errors);

        System.out.println(body);

        return new ResponseEntity<>(body, headers, status);

    }
}
