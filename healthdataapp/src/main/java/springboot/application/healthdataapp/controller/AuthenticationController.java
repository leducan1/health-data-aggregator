package springboot.application.healthdataapp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import springboot.application.healthdataapp.model.type.Role;
import springboot.application.healthdataapp.model.user.User;
import springboot.application.healthdataapp.service.UserService;
import springboot.application.healthdataapp.service.authentication.AuthenticationRequest;

@RestController
public class AuthenticationController extends BaseController {

    @Autowired private UserService userService;

    /**
     * register a user
     *
     * @param user object with email and password
     * @return response with status code and message
     */
    @PostMapping(value = "/register", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> createUser(@RequestBody User user) {
        return userService.createUser(user, Role.ROLE_USER);
    }

    /**
     * log in a user
     *
     * @param authenticationRequest request containing credentials
     * @return response with status code and message
     */
    @PostMapping(value = "/login", consumes = MediaType.APPLICATION_JSON_VALUE)
    public ResponseEntity<?> createAuthenticationToken(@RequestBody AuthenticationRequest authenticationRequest) {
        return userService.authenticate(authenticationRequest);
    }
}
