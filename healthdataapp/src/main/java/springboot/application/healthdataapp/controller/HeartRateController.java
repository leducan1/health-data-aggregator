package springboot.application.healthdataapp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import springboot.application.healthdataapp.model.heart.HeartRate;
import springboot.application.healthdataapp.service.HeartRateService;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/heart-rate")
@CrossOrigin(origins = "*")
@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
public class HeartRateController extends BaseController {
    @Autowired HeartRateService heartRateService;

    /**
     * get heart rate logs from a specified date
     * @param req HTTP request
     * @param date yyyy-MM-dd format
     * @param from data origin
     * @return response status, and response body
     */
    @GetMapping(value = "/{from}/{date}")
    public ResponseEntity<?> getHeartRateLogs(HttpServletRequest req, @PathVariable String from, @PathVariable String date) {
        return ResponseEntity.status(HttpStatus.OK).body(
                heartRateService.getHeartRateLogs(getUserFromRequest(req), getOrigin(from), date)
        );
    }

    /**
     * add heart rate log to this server
     * @param req HTTP request
     * @param body heart rate object
     * @param date yyyy-MM-dd format
     * @return response with status and message
     */
    @PostMapping(value = "/{from}/{date}")
    public ResponseEntity<?> postHeartRateLog(HttpServletRequest req, @RequestBody HeartRate body, @PathVariable String from, @PathVariable String date) {
        return heartRateService.addHeartRateLog(body, getUserFromRequest(req));
    }

    /**
     * get heart ratye logs from a date range
     * @param req HTTP request
     * @param from data origin
     * @param startDate yyyy-MM-dd format
     * @param endDate yyyy-MM-dd format
     * @return response with status, and response body
     */
    @GetMapping(value = "/{from}/{startDate}/{endDate}")
    public ResponseEntity<?> getHeartRateLogs(HttpServletRequest req, @PathVariable String from, @PathVariable String startDate, @PathVariable String endDate) {
        return ResponseEntity.status(HttpStatus.OK).body(
                heartRateService.getHeartRateLogsByDateRange(getUserFromRequest(req), getOrigin(from), startDate, endDate)
        );
    }
}
