package springboot.application.healthdataapp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.Errors;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;
import springboot.application.healthdataapp.model.weight.WeightLog;
import springboot.application.healthdataapp.service.WeightService;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;
import java.util.Collections;

@RestController
@RequestMapping("/weight")
@CrossOrigin(origins = "*")
@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
public class WeightController extends BaseController {
    @Autowired WeightService weightService;

    /**
     * add weight log to this server
     * @param req HTTP request
     * @param weightLog weight log
     * @param date yyyy-MM-dd format
     * @return response with status code, and message
     */
    @PostMapping(value = "/this/{date}")
    public ResponseEntity<?> addWeightLog(HttpServletRequest req, @RequestBody WeightLog weightLog, @PathVariable String date) {
        return weightService.addWeightLog(weightLog, getUserFromRequest(req), date);
    }

    /**
     * get weight logs from a specified date
     * @param req HTTP request
     * @param from data origin
     * @param date yyyy-MM-dd format
     * @return response with status code, and response body
     */
    @GetMapping(value = "/{from}/{date}")
    public ResponseEntity<?> getWeightLogsByDate(HttpServletRequest req, @PathVariable String from, @PathVariable String date) {
        return ResponseEntity.status(HttpStatus.OK).body(
                weightService.getWeightLogsByDate(date, getOrigin(from), getUserFromRequest(req))
        );
    }

    /**
     * get weight logs from a specified date range
     * @param req HTTP request
     * @param from data origin
     * @param startDate yyyy-MM-dd format
     * @param endDate yyyy-MM-dd format
     * @return response with status code, and message
     */
    @GetMapping(value = "/{from}/{startDate}/{endDate}")
    public ResponseEntity<?> getWeightLogsByDateRange(HttpServletRequest req, @PathVariable String from, @PathVariable String startDate, @PathVariable String endDate) {
        return ResponseEntity.status(HttpStatus.OK).body(
                weightService.getWeightLogsByDateRange(startDate, endDate, getOrigin(from), getUserFromRequest(req))
        );
    }
}
