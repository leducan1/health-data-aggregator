package springboot.application.healthdataapp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import springboot.application.healthdataapp.model.sleep.SleepLevel;
import springboot.application.healthdataapp.model.sleep.SleepLog;
import springboot.application.healthdataapp.service.SleepService;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/sleep")
@CrossOrigin(origins = "*")
@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
public class SleepController extends BaseController {
    @Autowired SleepService sleepService;

    /**
     * get sleep log from a specified date
     * @param req HTTP request
     * @param from data origin
     * @param date yyyy-MM-dd format
     * @return response with response status, and response body
     */
    @GetMapping(value = "/{from}/{date}")
    public ResponseEntity<?> getSleep(HttpServletRequest req, @PathVariable String from, @PathVariable String date) {
        return ResponseEntity.status(HttpStatus.OK).body(
                sleepService.getSleepLog(getUserFromRequest(req), getOrigin(from), date)
        );
    }

    /**
     * add sleep log to this server
     * @param req HTTP request
     * @param sleepLog sleep log
     * @param from data origin
     * @param date yyyy-MM-dd format
     * @return response with status code, and message
     */
    @PostMapping(value = "/{from}/{date}")
    public ResponseEntity<?> postSleep(HttpServletRequest req, @RequestBody SleepLog sleepLog, @PathVariable String from, @PathVariable String date) {
        return sleepService.addSleepLog(sleepLog, getUserFromRequest(req), date);
    }

    /**
     * get sleep logs from a date range
     * @param req HTTP request
     * @param from data origin
     * @param startDate yyyy-MM-dd format
     * @param endDate yyyy-MM-dd format
     * @return response with status code, and message
     */
    @GetMapping(value = "/{from}/{startDate}/{endDate}")
    public ResponseEntity<?> getSleepByDateRange(HttpServletRequest req, @PathVariable String from, @PathVariable String startDate, @PathVariable String endDate) {
        return ResponseEntity.status(HttpStatus.OK).body(
                sleepService.getSleepLogByDateRange(getUserFromRequest(req), getOrigin(from), startDate, endDate)
        );
    }
}
