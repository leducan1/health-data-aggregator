package springboot.application.healthdataapp.controller;

import com.fasterxml.jackson.databind.JsonNode;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import springboot.application.healthdataapp.model.user.User;
import springboot.application.healthdataapp.service.sync.SamsungSyncService;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/samsung")
@CrossOrigin(origins = "*")
@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
public class SamsungController extends BaseController {
    @Autowired
    private SamsungSyncService samsungSyncService;

    /**
     * sync data from samsung from a specified date range
     * @param req HTTP request
     * @param data Samsung data
     * @param startMillis start date time in milliseconds
     * @param endMillis end date time in milliseconds
     * @return response with status code, and message
     */
    @PostMapping(value = "/sync/{startMillis}/{endMillis}")
    public ResponseEntity<?> syncDataFromDateRange(HttpServletRequest req, @RequestBody JsonNode data, @PathVariable long startMillis, @PathVariable long endMillis) {
        User user = getUserFromRequest(req);
        return samsungSyncService.createAndPersistDataFromDateRange(data, user, startMillis, endMillis);
    }
}
