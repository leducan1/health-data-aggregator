package springboot.application.healthdataapp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import springboot.application.healthdataapp.model.blood.BloodPressure;
import springboot.application.healthdataapp.service.BloodPressureService;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/blood-pressure")
@CrossOrigin(origins = "*")
@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
public class BloodPressureController extends BaseController {
    @Autowired BloodPressureService bloodPressureService;

    /**
     * get blood pressure logs from a specified date
     * @param req HTTP request
     * @param date yyyy-MM-dd format
     * @param from data origin
     * @return response status, and response body
     */
    @GetMapping(value = "/{from}/{date}")
    public ResponseEntity<?> getBloodPressureLogs(HttpServletRequest req, @PathVariable String from, @PathVariable String date) {
        return ResponseEntity.status(HttpStatus.OK).body(
                bloodPressureService.getBloodPressureLogs(getUserFromRequest(req), getOrigin(from), date)
        );
    }

    /**
     * add blood pressure log to this server
     * @param req HTTP request
     * @param b blood pressure object
     * @param date yyyy-MM-dd format
     * @return response with status and message
     */
    @PostMapping(value = "/{from}/{date}")
    public ResponseEntity<?> postBloodPressureLog(HttpServletRequest req, @RequestBody BloodPressure b, @PathVariable String from, @PathVariable String date) {
        return bloodPressureService.addBloodPressureLog(b, getUserFromRequest(req));
    }

    /**
     * get blood pressure logs from a date range
     * @param req HTTP request
     * @param from data origin
     * @param startDate yyyy-MM-dd format
     * @param endDate yyyy-MM-dd format
     * @return response with status, and response body
     */
    @GetMapping(value = "/{from}/{startDate}/{endDate}")
    public ResponseEntity<?> getBloodPressureLogs(HttpServletRequest req, @PathVariable String from, @PathVariable String startDate, @PathVariable String endDate) {
        return ResponseEntity.status(HttpStatus.OK).body(
                bloodPressureService.getBloodPressureLogsByDateRange(getUserFromRequest(req), getOrigin(from), startDate, endDate)
        );
    }


}
