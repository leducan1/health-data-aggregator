package springboot.application.healthdataapp.controller;

import net.minidev.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import springboot.application.healthdataapp.request.AuthCodeRequest;
import springboot.application.healthdataapp.service.sync.GoogleSyncService;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/google")
@CrossOrigin(origins = "*")
@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
public class GoogleController extends BaseController{
    @Autowired
    private GoogleSyncService googleSyncService;

    /**
     * get a link to google consent screen
     * @return response with consent screen URL
     */
    @GetMapping(value = "/auth")
    public JSONObject getLinkToConsentScreen() {
        return googleSyncService.getConsentScreenURL();
    }

    /**
     * get an access token from an authorization code
     * @param request HTTP request
     * @param googleAuthCodeRequest object containing auth code
     * @return access token
     */
    @PostMapping(value = "/auth")
    public ResponseEntity<?> getAuthTokenWithAuthCode(HttpServletRequest request, @RequestBody AuthCodeRequest googleAuthCodeRequest){
        return googleSyncService.getAccessToken(getUserFromRequest(request), googleAuthCodeRequest);
    }

    /**
     * sync data from google from a specified date range
     * @param req HTTP request
     * @param startdate yyyy-MM-dd format
     * @param enddate yyyy-MM-dd format
     * @return response with status code, and message
     */
    @GetMapping(value = "/sync/{startdate}/{enddate}")
    public ResponseEntity<?> syncFromDateRange(HttpServletRequest req, @PathVariable String startdate, @PathVariable String enddate) {
        return googleSyncService.syncFromDateRange(getUserFromRequest(req), startdate, enddate);
    }
}
