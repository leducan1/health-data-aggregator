package springboot.application.healthdataapp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RestController;
import springboot.application.healthdataapp.exception.InvalidEndpointRequestException;
import springboot.application.healthdataapp.model.type.TokenType;
import springboot.application.healthdataapp.model.user.User;
import springboot.application.healthdataapp.service.UserService;
import springboot.application.healthdataapp.service.authentication.JwtUtil;

import javax.servlet.http.HttpServletRequest;

@RestController
public class BaseController {
    @Autowired
    protected JwtUtil jwtTokenUtil;

    @Autowired
    protected UserService userService;

    /**
     * extracts user from a request
     * @param request HTTP request
     * @return user or null
     */
    protected User getUserFromRequest(HttpServletRequest request) {
        String email = jwtTokenUtil.extractUsername(jwtTokenUtil.extractTokenPart(request.getHeader("Authorization")));
        return userService.findByEmail(email);
    }

    /**
     * convers origin in String for to object
     * @param from origin in String format
     * @return TokenType object or throws an exception
     */
    protected TokenType getOrigin(String from) {
        switch (from.toLowerCase()) {
            case "google":
                return TokenType.GOOGLE;
            case "fitbit":
                return TokenType.FITBIT;
            case "samsung":
                return TokenType.SAMSUNG;
            case "this":
                return TokenType.THIS;
            case "all":
                return TokenType.ALL;
            default:
                throw new InvalidEndpointRequestException("Requesting invalid endpoint");
        }
    }
}
