package springboot.application.healthdataapp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;
import springboot.application.healthdataapp.model.water.WaterLog;
import springboot.application.healthdataapp.service.WaterService;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping("/water")
@CrossOrigin(origins = "*")
@PreAuthorize("hasRole('ROLE_ADMIN') or hasRole('ROLE_USER')")
public class WaterController extends BaseController {
    @Autowired WaterService waterService;

    /**
     * add water log to this server
     * @param req HTTP request
     * @param water water log
     * @param date yyyy-MM-dd format
     * @return response, with status code and message
     */
    @PostMapping(value = "/this/{date}")
    public ResponseEntity<?> addWater(HttpServletRequest req, @RequestBody WaterLog water, @PathVariable String date) {
        return waterService.addWaterLog(water, getUserFromRequest(req), date);
    }

    /**
     * get daily water log from a specified date
     * @param req HTTP request
     * @param from data origin
     * @param date yyyy-MM-dd format
     * @return response with status code, and response body
     */
    @GetMapping(value = "/{from}/{date}")
    public ResponseEntity<?> getDailyWaterLog(HttpServletRequest req, @PathVariable String from, @PathVariable String date) {
        return ResponseEntity.status(HttpStatus.OK).body(
                waterService.getDailyWaterLog(getUserFromRequest(req), getOrigin(from), date)
        );
    }

    /**
     * get daily water log from a date range
     * @param req HTTP request
     * @param from data origin
     * @param startDate yyyy-MM-dd format
     * @param endDate yyyy-MM-dd format
     * @return response with status code, and response body
     */
    @GetMapping(value = "/{from}/{startDate}/{endDate}")
    public ResponseEntity<?> getDailyWaterLog(HttpServletRequest req, @PathVariable String from, @PathVariable String startDate, @PathVariable String endDate) {
        return ResponseEntity.status(HttpStatus.OK).body(
                waterService.getWaterLogsByDateRange(getUserFromRequest(req), getOrigin(from), startDate, endDate)
        );
    }
}
