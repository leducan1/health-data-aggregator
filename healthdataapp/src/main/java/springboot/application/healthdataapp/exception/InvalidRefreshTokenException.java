package springboot.application.healthdataapp.exception;

import springboot.application.healthdataapp.model.type.ResponseMessageStatus;

public class InvalidRefreshTokenException extends APIException {
    public InvalidRefreshTokenException(String message) {
        super(400, message, ResponseMessageStatus.INVALID_REFRESH_TOKEN);
    }
}