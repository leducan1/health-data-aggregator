package springboot.application.healthdataapp.exception;

import springboot.application.healthdataapp.model.type.ResponseMessageStatus;

public class BadCredentialsException extends APIException {
    public BadCredentialsException() {
        super(401, "Bad credentials", ResponseMessageStatus.BAD_CREDENTIALS);
    }
}