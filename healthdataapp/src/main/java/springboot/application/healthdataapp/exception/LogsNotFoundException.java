package springboot.application.healthdataapp.exception;

import springboot.application.healthdataapp.model.type.ResponseMessageStatus;

public class LogsNotFoundException extends APIException {
    public LogsNotFoundException(String message) {
        super(404, message, ResponseMessageStatus.DATA_NOT_FOUND);
    }
}