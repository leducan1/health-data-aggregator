package springboot.application.healthdataapp.exception;

import springboot.application.healthdataapp.model.type.ResponseMessageStatus;

public class AccessTokenNotGrantedException extends APIException {
    public AccessTokenNotGrantedException(String message) {
        super(400, message, ResponseMessageStatus.ACCESS_TOKEN_NOT_GRANTED);
    }
}