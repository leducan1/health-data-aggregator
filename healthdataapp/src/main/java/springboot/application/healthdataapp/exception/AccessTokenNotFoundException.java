package springboot.application.healthdataapp.exception;

import springboot.application.healthdataapp.model.type.ResponseMessageStatus;

public class AccessTokenNotFoundException extends APIException {
    public AccessTokenNotFoundException(String message, ResponseMessageStatus responseMessageStatus) {
        super(404, message, responseMessageStatus);
    }
}