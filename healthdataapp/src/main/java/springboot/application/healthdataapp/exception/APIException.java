package springboot.application.healthdataapp.exception;
import lombok.Getter;
import springboot.application.healthdataapp.response.ResponseMessage;
import springboot.application.healthdataapp.model.type.ResponseMessageStatus;

@Getter
public class APIException extends RuntimeException {
    protected ResponseMessage error;

    public APIException(int code, String message, ResponseMessageStatus responseMessageStatus) {
        error = new ResponseMessage(code, message, responseMessageStatus);
    }
}