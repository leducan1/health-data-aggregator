package springboot.application.healthdataapp.exception;

import springboot.application.healthdataapp.model.type.ResponseMessageStatus;

public class InvalidEndpointRequestException extends APIException {
    public InvalidEndpointRequestException(String message) {
        super(400, message, ResponseMessageStatus.INVALID_ENDPOINT_REQUEST);
    }
}