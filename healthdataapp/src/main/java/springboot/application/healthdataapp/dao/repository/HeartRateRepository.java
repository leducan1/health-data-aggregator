package springboot.application.healthdataapp.dao.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import springboot.application.healthdataapp.model.heart.HeartRate;
import springboot.application.healthdataapp.model.type.TokenType;
import springboot.application.healthdataapp.model.user.User;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

/**
 * method names are declarative, and describe their function
 */
@Repository
public interface HeartRateRepository extends JpaRepository<HeartRate, Integer> {
    void removeAllByStartDateTimeBetweenAndOriginAndUser(LocalDateTime startDateTime, LocalDateTime endDateTime, TokenType type, User user);

    void removeAllByStartDateTimeAndOriginAndUser(LocalDateTime startDateTime, TokenType origin, User user);

    List<HeartRate> findAllByDateAndOriginAndUserOrderByStartDateTimeAsc(LocalDate date, TokenType from, User user);

    List<HeartRate> findAllByDateAndUserOrderByStartDateTimeAsc(LocalDate date, User user);

    List<HeartRate> findAllByDateBetweenAndOriginAndUserOrderByStartDateTimeAsc(LocalDate startDate, LocalDate endDate, TokenType from, User user);

    List<HeartRate> findAllByDateBetweenAndUserOrderByStartDateTimeAsc(LocalDate startDate, LocalDate endDate, User user);
}
