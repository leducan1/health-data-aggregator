package springboot.application.healthdataapp.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import springboot.application.healthdataapp.model.type.TokenType;
import springboot.application.healthdataapp.model.user.AuthCredentials;
import springboot.application.healthdataapp.model.user.User;
import springboot.application.healthdataapp.dao.repository.AuthCredentialsRepository;

import java.util.List;

/**
 * method names are declarative, and describe their function
 */
@Repository
@Transactional
public class AuthCredentialsDao {
    @Autowired AuthCredentialsRepository authCredentialsRepository;

    public void removeAllByUserAndTokenType(User user, TokenType tokenType) {
        authCredentialsRepository.removeAllByUserAndTokenType(user, tokenType);
    }

    public AuthCredentials findByUserAndTokenType(User user, TokenType tokenType) {
        return authCredentialsRepository.findByUserAndTokenType(user, tokenType);
    }

    public void save(AuthCredentials authCredentials) {
        authCredentialsRepository.save(authCredentials);
    }

    public void saveAll(List<AuthCredentials> authCredentials) {
        authCredentialsRepository.saveAll(authCredentials);
    }
}
