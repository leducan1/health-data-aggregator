package springboot.application.healthdataapp.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import springboot.application.healthdataapp.model.type.TokenType;
import springboot.application.healthdataapp.model.user.User;
import springboot.application.healthdataapp.model.water.WaterLog;
import springboot.application.healthdataapp.dao.repository.WaterLogRepository;

import java.time.LocalDate;
import java.util.List;

/**
 * method names are declarative, and describe their function
 */
@Repository
@Transactional
public class WaterLogDao {
    @Autowired WaterLogRepository waterLogRepository;

    public List<WaterLog> findAllByDateAndOriginAndUserOrderByDateAsc(LocalDate date, TokenType from, User user) {
        return waterLogRepository.findAllByDateAndOriginAndUserOrderByDateAsc(date, from, user);
    }

    public List<WaterLog> findAllByDateAndUserOrderByDateAsc(LocalDate date, User user) {
        return waterLogRepository.findAllByDateAndUserOrderByDateAsc(date, user);
    }

    public List<WaterLog> findAllByDateBetweenAndOriginAndUserOrderByDateAsc(LocalDate startDate, LocalDate endDate, TokenType from, User user) {
        return waterLogRepository.findAllByDateBetweenAndOriginAndUserOrderByDateAsc(startDate, endDate, from, user);
    }

    public List<WaterLog> findAllByDateBetweenAndUserOrderByDateAsc(LocalDate startDate, LocalDate endDate, User user) {
        return waterLogRepository.findAllByDateBetweenAndUserOrderByDateAsc(startDate, endDate, user);
    }

    public void removeAllByDateBetweenAndUserAndOrigin(LocalDate startDate, LocalDate endDate, User user, TokenType origin) {
        waterLogRepository.removeAllByDateBetweenAndUserAndOrigin(startDate, endDate, user, origin);
    }

    public void removeByDateAndUserAndOrigin(LocalDate date, User user, TokenType origin) {
        waterLogRepository.removeByDateAndUserAndOrigin(date, user, origin);
    }

    public void save(WaterLog log) {
        waterLogRepository.save(log);
    }

    public void saveAll(List<WaterLog> log) {
        waterLogRepository.saveAll(log);
    }
}
