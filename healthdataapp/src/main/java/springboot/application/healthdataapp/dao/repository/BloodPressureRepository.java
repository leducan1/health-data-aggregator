package springboot.application.healthdataapp.dao.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import springboot.application.healthdataapp.model.blood.BloodPressure;
import springboot.application.healthdataapp.model.type.TokenType;
import springboot.application.healthdataapp.model.user.User;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

/**
 * method names are declarative, and describe their function
 */
@Repository
public interface BloodPressureRepository extends JpaRepository<BloodPressure, Integer> {
    void removeAllByDateBetweenAndOriginAndUser(LocalDate startDate, LocalDate endDate, TokenType type, User user);

    void removeAllByDateAndTimeAndOriginAndUser(LocalDate date, LocalTime time, TokenType from, User user);

    List<BloodPressure> findAllByDateAndOriginAndUserOrderByDateAsc(LocalDate date, TokenType type, User user);

    List<BloodPressure> findAllByDateAndUserOrderByDateAsc(LocalDate date, User user);

    List<BloodPressure> findAllByDateBetweenAndOriginAndUserOrderByDateAsc(LocalDate startDate, LocalDate endDate, TokenType from, User user);

    List<BloodPressure> findAllByDateBetweenAndUserOrderByDateAsc(LocalDate startDate, LocalDate endDate, User user);
}