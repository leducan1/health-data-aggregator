package springboot.application.healthdataapp.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import springboot.application.healthdataapp.model.dailysummary.DailySummary;
import springboot.application.healthdataapp.model.type.TokenType;
import springboot.application.healthdataapp.model.user.User;
import springboot.application.healthdataapp.dao.repository.DailySummaryRepository;

import java.time.LocalDate;
import java.util.List;

/**
 * method names are declarative, and describe their function
 */
@Repository
@Transactional
public class DailySummaryDao {
    @Autowired DailySummaryRepository dailySummaryRepository;

    public void removeAllByDateBetweenAndUserAndOrigin(LocalDate startDate, LocalDate endDate, User user, TokenType origin) {
        dailySummaryRepository.removeAllByDateBetweenAndUserAndOrigin(startDate, endDate,  user, origin);
    }

    public List<DailySummary> findAllByDateAndOriginAndUserOrderByDateAsc(LocalDate date, TokenType type, User user) {
        return dailySummaryRepository.findAllByDateAndOriginAndUserOrderByDateAsc(date, type, user);
    }

    public List<DailySummary> findAllByDateAndUserOrderByDateAsc(LocalDate date, User user) {
        return dailySummaryRepository.findAllByDateAndUserOrderByDateAsc(date, user);
    }

    public List<DailySummary> findAllByDateBetweenAndOriginAndUserOrderByDateAsc(LocalDate startDate, LocalDate endDate, TokenType from, User user) {
        return dailySummaryRepository.findAllByDateBetweenAndOriginAndUserOrderByDateAsc(startDate, endDate, from, user);
    }

    public List<DailySummary> findAllByDateBetweenAndUserOrderByDateAsc(LocalDate startDate, LocalDate endDate, User user) {
        return dailySummaryRepository.findAllByDateBetweenAndUserOrderByDateAsc(startDate, endDate, user);
    }

    public void save(DailySummary log) {
        dailySummaryRepository.save(log);
    }

    public void saveAll(List<DailySummary> log) {
        dailySummaryRepository.saveAll(log);
    }
}
