package springboot.application.healthdataapp.dao.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import springboot.application.healthdataapp.model.sleep.SleepLog;
import springboot.application.healthdataapp.model.type.TokenType;
import springboot.application.healthdataapp.model.user.User;

import java.time.LocalDate;
import java.util.List;

/**
 * method names are declarative, and describe their function
 */
@Repository
public interface SleepLogRepository extends JpaRepository<SleepLog, Integer> {
    void removeAllByDateAndOriginAndUser(LocalDate date, TokenType from, User user);

    void removeAllByDateBetweenAndOriginAndUserOrderByDateAsc(LocalDate startDate, LocalDate endDate, TokenType origin, User user);

    List<SleepLog> findAllByDateAndOriginAndUserOrderByDateAsc(LocalDate date, TokenType type, User user);

    List<SleepLog> findAllByDateAndUserOrderByDateAsc(LocalDate date, User user);

    List<SleepLog> findAllByDateBetweenAndOriginAndUserOrderByDateAsc(LocalDate startDate, LocalDate endDate, TokenType from, User user);

    List<SleepLog> findAllByDateBetweenAndUserOrderByDateAsc(LocalDate startDate, LocalDate endDate, User user);
}
