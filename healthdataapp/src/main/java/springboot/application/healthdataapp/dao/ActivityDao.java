package springboot.application.healthdataapp.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import springboot.application.healthdataapp.model.activity.ActivityLog;
import springboot.application.healthdataapp.model.type.TokenType;
import springboot.application.healthdataapp.model.user.User;
import springboot.application.healthdataapp.dao.repository.ActivityLogRepository;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

/**
 * method names are declarative, and describe their function
 */
@Repository
@Transactional
public class ActivityDao {
    @Autowired
    ActivityLogRepository activityLogRepository;

    public void saveAll(List<ActivityLog> activityLogs) {
        activityLogRepository.saveAll(activityLogs);
    }

    public void save(ActivityLog log) {
        activityLogRepository.save(log);
    }

    public void removeAllByStartDateTimeBetweenAndOriginAndUser(LocalDateTime startDate, LocalDateTime endDate, TokenType from, User user) {
        activityLogRepository.removeAllByStartDateTimeBetweenAndOriginAndUser(startDate, endDate, from, user);
    }

    public List<ActivityLog> findAllByDateAndOriginAndUserOrderByDateAsc(LocalDate date, TokenType type, User user) {
        return activityLogRepository.findAllByDateAndOriginAndUserOrderByStartDateTimeAsc(date, type, user);
    }

    public List<ActivityLog> findAllByDateAndUserOrderByStartDateTimeAsc(LocalDate date, User user) {
        return activityLogRepository.findAllByDateAndUserOrderByStartDateTimeAsc(date, user);
    }
}
