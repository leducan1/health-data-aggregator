package springboot.application.healthdataapp.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import springboot.application.healthdataapp.model.blood.BloodPressure;
import springboot.application.healthdataapp.model.type.TokenType;
import springboot.application.healthdataapp.model.user.User;
import springboot.application.healthdataapp.dao.repository.BloodPressureRepository;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

/**
 * method names are declarative, and describe their function
 */
@Repository
@Transactional
public class BloodPressureDao {
    @Autowired BloodPressureRepository bloodPressureRepository;

    public void removeAllByDateBetweenAndOriginAndUser(LocalDate startDate, LocalDate endDate, TokenType type, User user) {
        bloodPressureRepository.removeAllByDateBetweenAndOriginAndUser(startDate, endDate, type, user);
    }

    public void removeAllByDateAndTimeAndOriginAndUser(LocalDate date, LocalTime time, TokenType from, User user) {
        bloodPressureRepository.removeAllByDateAndTimeAndOriginAndUser(date, time, from, user);
    }

    public List<BloodPressure> findAllByDateAndOriginAndUserOrderByDateAsc(LocalDate date, TokenType type, User user) {
        return bloodPressureRepository.findAllByDateAndOriginAndUserOrderByDateAsc(date, type, user);
    }

    public List<BloodPressure> findAllByDateAndUserOrderByDateAsc(LocalDate date, User user) {
        return bloodPressureRepository.findAllByDateAndUserOrderByDateAsc(date, user);
    }

    public List<BloodPressure> findAllByDateBetweenAndOriginAndUserOrderByDateAsc(LocalDate startDate, LocalDate endDate, TokenType from, User user) {
        return bloodPressureRepository.findAllByDateBetweenAndOriginAndUserOrderByDateAsc(startDate, endDate, from, user);
    }

    public List<BloodPressure> findAllByDateBetweenAndUserOrderByDateAsc(LocalDate startDate, LocalDate endDate, User user) {
        return bloodPressureRepository.findAllByDateBetweenAndUserOrderByDateAsc(startDate, endDate, user);
    }

    public void save(BloodPressure log) {
        bloodPressureRepository.save(log);
    }

    public void saveAll(List<BloodPressure> log) {
        bloodPressureRepository.saveAll(log);
    }

    public void delete(BloodPressure log) {
        bloodPressureRepository.delete(log);
    }
}
