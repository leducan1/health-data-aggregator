package springboot.application.healthdataapp.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import springboot.application.healthdataapp.model.type.TokenType;
import springboot.application.healthdataapp.model.user.User;
import springboot.application.healthdataapp.model.weight.WeightLog;
import springboot.application.healthdataapp.dao.repository.WeightLogRepository;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

/**
 * method names are declarative, and describe their function
 */
@Repository
@Transactional
public class WeightLogDao {
    @Autowired WeightLogRepository weightLogRepository;

    public void removeAllByDateBetweenAndOriginAndUser(LocalDate startDate, LocalDate endDate, TokenType from, User user) {
        weightLogRepository.removeAllByDateBetweenAndOriginAndUser(startDate, endDate, from, user);
    }

    public void removeAllByDateAndTimeAndOriginAndUser(LocalDate date, LocalTime time, TokenType from, User user) {
        weightLogRepository.removeAllByDateAndTimeAndOriginAndUser(date, time, from, user);
    }

    public WeightLog findByDateAndTimeAndOriginAndUser(LocalDate date, LocalTime time, TokenType from, User user) {
        return weightLogRepository.findByDateAndTimeAndOriginAndUser(date, time, from, user);
    }

    public List<WeightLog> findAllByDateAndOriginAndUserOrderByTimeAsc(LocalDate date, TokenType from, User user) {
        return weightLogRepository.findAllByDateAndOriginAndUserOrderByTimeAsc(date, from, user);
    }

    public List<WeightLog> findAllByDateAndUserOrderByTimeAsc(LocalDate date, User user) {
        return weightLogRepository.findAllByDateAndUserOrderByTimeAsc(date, user);
    }

    public List<WeightLog> findAllByDateBetweenAndOriginAndUserOrderByDateAsc(LocalDate startDate, LocalDate endDate, TokenType from, User user) {
        return weightLogRepository.findAllByDateBetweenAndOriginAndUserOrderByDateAsc(startDate, endDate, from, user);
    }

    public List<WeightLog> findAllByDateBetweenAndUserOrderByDateAsc(LocalDate startDate, LocalDate endDate, User user) {
        return weightLogRepository.findAllByDateBetweenAndUserOrderByDateAsc(startDate, endDate, user);
    }

    public void removeAllByDateBetweenAndUserAndOrigin(LocalDate start, LocalDate end, User user, TokenType origin) {
        weightLogRepository.removeAllByDateBetweenAndUserAndOrigin(start, end, user, origin);
    }

    public void save(WeightLog authCredentials) {
        weightLogRepository.save(authCredentials);
    }

    public void saveAll(List<WeightLog> authCredentials) {
        weightLogRepository.saveAll(authCredentials);
    }
}
