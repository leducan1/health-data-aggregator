package springboot.application.healthdataapp.dao.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import springboot.application.healthdataapp.model.type.TokenType;
import springboot.application.healthdataapp.model.user.User;
import springboot.application.healthdataapp.model.water.WaterLog;

import java.time.LocalDate;
import java.util.List;

/**
 * method names are declarative, and describe their function
 */
@Repository
public interface WaterLogRepository extends JpaRepository<WaterLog, Integer> {
    List<WaterLog> findAllByDateAndOriginAndUserOrderByDateAsc(LocalDate date, TokenType from, User user);

    List<WaterLog> findAllByDateAndUserOrderByDateAsc(LocalDate date, User user);

    List<WaterLog> findAllByDateBetweenAndOriginAndUserOrderByDateAsc(LocalDate startDate, LocalDate endDate, TokenType from, User user);

    List<WaterLog> findAllByDateBetweenAndUserOrderByDateAsc(LocalDate startDate, LocalDate endDate, User user);

    void removeAllByDateBetweenAndUserAndOrigin(LocalDate startDate, LocalDate endDate, User user, TokenType origin);

    void removeByDateAndUserAndOrigin(LocalDate date, User user, TokenType origin);
}
