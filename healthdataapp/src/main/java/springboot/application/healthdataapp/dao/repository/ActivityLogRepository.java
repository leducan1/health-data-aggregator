package springboot.application.healthdataapp.dao.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import springboot.application.healthdataapp.model.activity.ActivityLog;
import springboot.application.healthdataapp.model.type.TokenType;
import springboot.application.healthdataapp.model.user.User;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

/**
 * method names are declarative, and describe their function
 */
@Repository
public interface ActivityLogRepository extends JpaRepository<ActivityLog, Integer> {
    void removeAllByStartDateTimeBetweenAndOriginAndUser(LocalDateTime startDate, LocalDateTime endDate, TokenType from, User user);

    List<ActivityLog> findAllByDateAndOriginAndUserOrderByStartDateTimeAsc(LocalDate date, TokenType type, User user);

    List<ActivityLog> findAllByDateAndUserOrderByStartDateTimeAsc(LocalDate date, User user);
}
