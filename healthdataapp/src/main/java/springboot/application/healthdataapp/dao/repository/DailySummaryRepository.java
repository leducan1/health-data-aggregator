package springboot.application.healthdataapp.dao.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import springboot.application.healthdataapp.model.dailysummary.DailySummary;
import springboot.application.healthdataapp.model.type.TokenType;
import springboot.application.healthdataapp.model.user.User;

import java.time.LocalDate;
import java.util.List;

/**
 * method names are declarative, and describe their function
 */
@Repository
public interface DailySummaryRepository extends JpaRepository<DailySummary, Integer> {
    void removeAllByDateBetweenAndUserAndOrigin(LocalDate startDate, LocalDate endDate, User user, TokenType origin);

    List<DailySummary> findAllByDateAndOriginAndUserOrderByDateAsc(LocalDate date, TokenType type, User user);

    List<DailySummary> findAllByDateAndUserOrderByDateAsc(LocalDate date, User user);

    List<DailySummary> findAllByDateBetweenAndOriginAndUserOrderByDateAsc(LocalDate startDate, LocalDate endDate, TokenType from, User user);

    List<DailySummary> findAllByDateBetweenAndUserOrderByDateAsc(LocalDate startDate, LocalDate endDate, User user);
}
