package springboot.application.healthdataapp.dao.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import springboot.application.healthdataapp.model.type.TokenType;
import springboot.application.healthdataapp.model.user.User;
import springboot.application.healthdataapp.model.weight.WeightLog;

import java.time.LocalDate;
import java.time.LocalTime;
import java.util.List;

/**
 * method names are declarative, and describe their function
 */
@Repository
public interface WeightLogRepository extends JpaRepository<WeightLog, Integer> {
    void removeAllByDateBetweenAndOriginAndUser(LocalDate startDate, LocalDate endDate, TokenType from, User user);

    void removeAllByDateAndTimeAndOriginAndUser(LocalDate date, LocalTime time, TokenType from, User user);

    WeightLog findByDateAndTimeAndOriginAndUser(LocalDate date, LocalTime time, TokenType from, User user);

    List<WeightLog> findAllByDateAndOriginAndUserOrderByTimeAsc(LocalDate date, TokenType from, User user);

    List<WeightLog> findAllByDateAndUserOrderByTimeAsc(LocalDate date, User user);

    List<WeightLog> findAllByDateBetweenAndOriginAndUserOrderByDateAsc(LocalDate startDate, LocalDate endDate, TokenType from, User user);

    List<WeightLog> findAllByDateBetweenAndUserOrderByDateAsc(LocalDate startDate, LocalDate endDate, User user);

    void removeAllByDateBetweenAndUserAndOrigin(LocalDate start, LocalDate end, User user, TokenType origin);
}
