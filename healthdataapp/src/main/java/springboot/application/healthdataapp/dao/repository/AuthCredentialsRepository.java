package springboot.application.healthdataapp.dao.repository;


import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import springboot.application.healthdataapp.model.user.AuthCredentials;
import springboot.application.healthdataapp.model.type.TokenType;
import springboot.application.healthdataapp.model.user.User;

/**
 * method names are declarative, and describe their function
 */
@Repository
public interface AuthCredentialsRepository extends JpaRepository<AuthCredentials, Integer> {
    void removeAllByUserAndTokenType(User user, TokenType tokenType);

    AuthCredentials findByUserAndTokenType(User user, TokenType tokenType);
}
