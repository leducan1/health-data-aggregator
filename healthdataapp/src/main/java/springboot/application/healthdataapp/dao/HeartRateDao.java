package springboot.application.healthdataapp.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import springboot.application.healthdataapp.model.heart.HeartRate;
import springboot.application.healthdataapp.model.type.TokenType;
import springboot.application.healthdataapp.model.user.User;
import springboot.application.healthdataapp.dao.repository.HeartRateRepository;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

/**
 * method names are declarative, and describe their function
 */
@Repository
@Transactional
public class HeartRateDao {
    @Autowired HeartRateRepository heartRateRepository;

    public void removeAllByStartDateTimeBetweenAndOriginAndUser(LocalDateTime startDateTime, LocalDateTime endDateTime, TokenType type, User user) {
        heartRateRepository.removeAllByStartDateTimeBetweenAndOriginAndUser(startDateTime, endDateTime, type, user);
    }

    public void removeAllByStartDateTimeAndOriginAndUser(LocalDateTime startDateTime, TokenType origin, User user) {
        heartRateRepository.removeAllByStartDateTimeAndOriginAndUser(startDateTime, origin, user);
    }

    public List<HeartRate> findAllByDateAndOriginAndUserOrderByStartDateTimeAsc(LocalDate date, TokenType type, User user) {
        return heartRateRepository.findAllByDateAndOriginAndUserOrderByStartDateTimeAsc(date, type, user);
    }

    public List<HeartRate> findAllByDateAndUserOrderByStartDateTimeAsc(LocalDate date, User user) {
        return heartRateRepository.findAllByDateAndUserOrderByStartDateTimeAsc(date, user);
    }

    public List<HeartRate> findAllByDateBetweenAndOriginAndUserOrderByStartDateTimeAsc(LocalDate startDate, LocalDate endDate, TokenType from, User user) {
        return heartRateRepository.findAllByDateBetweenAndOriginAndUserOrderByStartDateTimeAsc(startDate, endDate, from, user);
    }

    public List<HeartRate> findAllByDateBetweenAndUserOrderByStartDateTimeAsc(LocalDate startDate, LocalDate endDate, User user) {
        return heartRateRepository.findAllByDateBetweenAndUserOrderByStartDateTimeAsc(startDate, endDate, user);
    }

    public void save(HeartRate log) {
        heartRateRepository.save(log);
    }

    public void saveAll(List<HeartRate> log) {
        heartRateRepository.saveAll(log);
    }
}
