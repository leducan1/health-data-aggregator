package springboot.application.healthdataapp.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import springboot.application.healthdataapp.model.sleep.SleepLog;
import springboot.application.healthdataapp.model.type.TokenType;
import springboot.application.healthdataapp.model.user.User;
import springboot.application.healthdataapp.dao.repository.SleepLogRepository;

import java.time.LocalDate;
import java.util.List;

/**
 * method names are declarative, and describe their function
 */
@Repository
@Transactional
public class SleepLogDao {
    @Autowired SleepLogRepository sleepLogRepository;

    public void removeAllByDateAndOriginAndUser(LocalDate date, TokenType from, User user) {
        sleepLogRepository.removeAllByDateAndOriginAndUser(date, from, user);
    }

    public void removeAllByDateBetweenAndOriginAndUserOrderByDateAsc(LocalDate startDate, LocalDate endDate, TokenType origin, User user) {
        sleepLogRepository.removeAllByDateBetweenAndOriginAndUserOrderByDateAsc(startDate, endDate, origin, user);
    }

    public List<SleepLog> findAllByDateAndOriginAndUserOrderByDateAsc(LocalDate date, TokenType type, User user) {
        return sleepLogRepository.findAllByDateAndOriginAndUserOrderByDateAsc(date, type, user);
    }

    public List<SleepLog> findAllByDateAndUserOrderByDateAsc(LocalDate date, User user) {
        return sleepLogRepository.findAllByDateAndUserOrderByDateAsc(date, user);
    }

    public List<SleepLog> findAllByDateBetweenAndOriginAndUserOrderByDateAsc(LocalDate startDate, LocalDate endDate, TokenType from, User user) {
        return sleepLogRepository.findAllByDateBetweenAndOriginAndUserOrderByDateAsc(startDate, endDate, from, user);
    }

    public List<SleepLog> findAllByDateBetweenAndUserOrderByDateAsc(LocalDate startDate, LocalDate endDate, User user) {
        return sleepLogRepository.findAllByDateBetweenAndUserOrderByDateAsc(startDate, endDate, user);
    }

    public void save(SleepLog log) {
        sleepLogRepository.save(log);
    }

    public void saveAll(List<SleepLog> log) {
        sleepLogRepository.saveAll(log);
    }
}
