package springboot.application.healthdataapp.dao;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;
import springboot.application.healthdataapp.model.user.User;
import springboot.application.healthdataapp.dao.repository.UserRepository;

import java.util.List;

/**
 * method names are declarative, and describe their function
 */
@Repository
@Transactional
public class UserDao {
    @Autowired UserRepository userRepository;

    public List<User> findAll() {
        return userRepository.findAll();
    }

    public User findByEmail(String email) {
        return userRepository.findByEmail(email);
    }

    public void save(User authCredentials) {
        userRepository.save(authCredentials);
    }

    public void saveAll(List<User> authCredentials) {
        userRepository.saveAll(authCredentials);
    }
}
