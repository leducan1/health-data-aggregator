package springboot.application.healthdataapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.jpa.repository.config.EnableJpaAuditing;
import org.springframework.scheduling.annotation.EnableScheduling;
//import org.springframework.data.mongodb.config.EnableMongoAuditing;

@SpringBootApplication
//@EnableMongoAuditing
@EnableJpaAuditing
@EnableScheduling
public class HealthdataappApplication {

	public static void main(String[] args) {
		SpringApplication.run(HealthdataappApplication.class, args);
	}

}
