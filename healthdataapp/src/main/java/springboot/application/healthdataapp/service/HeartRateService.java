package springboot.application.healthdataapp.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import springboot.application.healthdataapp.dao.HeartRateDao;
import springboot.application.healthdataapp.helpers.DateConverter;
import springboot.application.healthdataapp.helpers.ObjValidator;
import springboot.application.healthdataapp.model.heart.HeartRate;
import springboot.application.healthdataapp.model.type.ResponseMessageStatus;
import springboot.application.healthdataapp.model.type.TokenType;
import springboot.application.healthdataapp.model.user.User;
import springboot.application.healthdataapp.response.ResponseMessage;

import java.util.List;

@Service
public class HeartRateService {
    @Autowired HeartRateDao heartRateDao;

    /**
     * find all heart rate logs ordered by start date time asc
     * @param user user
     * @param from data origin
     * @param date yyyy-MM-dd format
     * @return list of heart rate logs
     */
    public List<HeartRate> getHeartRateLogs(User user, TokenType from, String date) {
        if (from == TokenType.ALL)
            return heartRateDao.findAllByDateAndUserOrderByStartDateTimeAsc(DateConverter.stringToLocalDate(date), user);
        return heartRateDao.findAllByDateAndOriginAndUserOrderByStartDateTimeAsc(DateConverter.stringToLocalDate(date), from, user);
    }

    public List<HeartRate> getHeartRateLogsByDateRange(User user, TokenType from, String startDate, String endDate) {
        if (from == TokenType.ALL)
            return heartRateDao.findAllByDateBetweenAndUserOrderByStartDateTimeAsc(DateConverter.stringToLocalDate(startDate), DateConverter.stringToLocalDate(endDate), user);
        return heartRateDao.findAllByDateBetweenAndOriginAndUserOrderByStartDateTimeAsc(DateConverter.stringToLocalDate(startDate), DateConverter.stringToLocalDate(endDate), from, user);
    }

    /**
     * add heart rate log
     * @param log heart rate log
     * @param user user
     * @return response with status code and message
     */
    @Transactional
    public ResponseEntity<?> addHeartRateLog(HeartRate log, User user) {
        log.setOrigin(TokenType.THIS);
        log.setUser(user);

        List<String> violations = ObjValidator.getConstraintViolations(log);
        if (violations.size() != 0)
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(violations);

        heartRateDao.removeAllByStartDateTimeAndOriginAndUser(log.getStartDateTime(), TokenType.THIS, user);
        heartRateDao.save(log);
        return ResponseEntity.status(HttpStatus.CREATED).body(new ResponseMessage(201, "Heart rate log from " + log.getDate() + " has been created", ResponseMessageStatus.DATA_CREATED));
    }
}
