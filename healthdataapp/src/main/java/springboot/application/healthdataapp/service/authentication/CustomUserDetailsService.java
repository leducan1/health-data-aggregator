package springboot.application.healthdataapp.service.authentication;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;
import springboot.application.healthdataapp.dao.repository.UserRepository;
import springboot.application.healthdataapp.exception.BadCredentialsException;
import springboot.application.healthdataapp.model.type.Role;
import springboot.application.healthdataapp.model.user.User;

import java.util.ArrayList;
import java.util.List;

@Service
public class CustomUserDetailsService implements UserDetailsService {
    @Autowired
    private UserRepository userDao;

    @Override
    public UserDetails loadUserByUsername(String email) throws UsernameNotFoundException {
        User user = userDao.findByEmail(email);
        if (user != null) {
            List<Role> roles = new ArrayList<>();
            roles.add(user.getRole());
            return new org.springframework.security.core.userdetails.User(user.getEmail(), user.getPassword(), roles);
        }
        else throw new BadCredentialsException();
    }
}