package springboot.application.healthdataapp.service;

import com.google.gson.Gson;
import org.springframework.stereotype.Service;
import springboot.application.healthdataapp.model.user.AuthCredentials;
import springboot.application.healthdataapp.request.google.GoogleAccessTokenRequest;
import springboot.application.healthdataapp.request.google.GoogleRefreshTokenRequest;

import java.io.IOException;
import java.net.URI;
import java.net.URLEncoder;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.nio.charset.StandardCharsets;
import java.util.Map;

import static java.util.Map.entry;

@Service
public class RequestSender {
    private final HttpClient httpClient = HttpClient.newBuilder().version(HttpClient.Version.HTTP_2).build();

    /**
     * build form data from a map of strings
     * @param data map of key and values
     * @return form data
     */
    private HttpRequest.BodyPublisher buildFormDataFromMap(Map<Object, Object> data) {
        StringBuilder builder = new StringBuilder();
        data.forEach((key, value) -> {
            if (builder.length() > 0) {
                builder.append("&");
            }
            builder.append(URLEncoder.encode(key.toString(), StandardCharsets.UTF_8));
            builder.append("=");
            builder.append(URLEncoder.encode(value.toString(), StandardCharsets.UTF_8));
        });
        return HttpRequest.BodyPublishers.ofString(builder.toString());
    }

    /**
     * create a HTTP request with POST method with form data
     * @param uri uri
     * @param headers map of headers
     * @param formData map with form data
     * @return HTTP request
     */
    private HttpRequest createPostRequest(String uri, Map<String, String> headers, Map<Object, Object> formData) {
        HttpRequest.Builder requestBuilder = HttpRequest.newBuilder().uri(URI.create(uri));
        headers.forEach((key, value) -> {
            requestBuilder.header(key, value);
        });

        return requestBuilder.POST(buildFormDataFromMap(formData)).build();
    }

    /**
     * create a HTTP request with POST method with body
     * @param uri uri
     * @param headers map of headers
     * @param body request body
     * @return HTTP Request
     */
    private HttpRequest createPostRequest(String uri, Map<String, String> headers, Object body) {
        HttpRequest.Builder requestBuilder = HttpRequest.newBuilder().uri(URI.create(uri));
        headers.forEach((key, value) -> {
            requestBuilder.header(key, value);
        });

        return requestBuilder.POST(HttpRequest.BodyPublishers.ofString(new Gson().toJson(body))).build();
    }

    /**
     * create a HTTP request with GET method
     * @param uri uri
     * @param headers headers
     * @return HTTP request
     */
    private HttpRequest createGetRequest(String uri, Map<String, String> headers) {
        HttpRequest.Builder requestBuilder = HttpRequest.newBuilder().uri(URI.create(uri));

        headers.forEach((key, value) -> {
            requestBuilder.header(key, value);
        });

        return requestBuilder.GET().build();
    }

    /**
     * send request with GET methods to google servers
     * @param uri uri
     * @param authCredentials auth credentials containing token
     * @return http response
     */
    public HttpResponse<String> sendGoogleGetRequest(String uri, AuthCredentials authCredentials) {
        try {
            Map<String, String> headers = Map.ofEntries(entry("Authorization", "Bearer " + authCredentials.getAccessToken()));
            HttpRequest request = createGetRequest(uri, headers);
            return httpClient.send(request, HttpResponse.BodyHandlers.ofString());
        } catch (IOException | InterruptedException e) {
            return null;
        }
    }

    /**
     * send request with POST methods to google servers
     * @param uri uri
     * @param authCredentials auth credentials containing token
     * @param body request body
     * @return http response
     */
    public HttpResponse<String> sendGooglePostRequest(String uri, AuthCredentials authCredentials, Object body) {
        try {
            Map<String, String> headers = Map.ofEntries(entry("Authorization", "Bearer " + authCredentials.getAccessToken()));
            HttpRequest request = createPostRequest(uri, headers, body);
            return httpClient.send(request, HttpResponse.BodyHandlers.ofString());
        } catch (IOException | InterruptedException e) {
            return null;
        }
    }

    /**
     * send request with POST method to google servers for refreshing a token
     * @param refreshToken refresh token
     * @return Http response with new token
     */
    public HttpResponse<String> sendGoogleRefreshTokenRequest(String refreshToken) {
        try {
            Map<String, String> headers = Map.ofEntries(entry("Content-Type", "application/json"));
            GoogleRefreshTokenRequest refreshTokenRequest = new GoogleRefreshTokenRequest(refreshToken);
            HttpRequest request = createPostRequest("https://oauth2.googleapis.com/token", headers, refreshTokenRequest);
            return httpClient.send(request, HttpResponse.BodyHandlers.ofString());
        } catch (IOException | InterruptedException e) {
            return null;
        }
    }

    /**
     * send request with POST method to google servers for access token
     * @param authCode authorization code
     * @return http response
     */
    public HttpResponse<String> sendGoogleAccessTokenRequest(String authCode) {
        try {
            Map<String, String> headers = Map.ofEntries(entry("Content-Type", "application/json"));
            GoogleAccessTokenRequest tokenRequest = new GoogleAccessTokenRequest(authCode);
            HttpRequest postRequest = createPostRequest("https://oauth2.googleapis.com/token", headers, tokenRequest);
            return httpClient.send(postRequest, HttpResponse.BodyHandlers.ofString());
        } catch (IOException | InterruptedException e) {
            return null;
        }
    }

    /**
     * send request with POST method to fitbit servers for access token
     * @param authCode authorization code
     * @return http response
     */
    public HttpResponse<String> sendFitbitAccessTokenRequest(String authCode) {
        try {
            Map<Object, Object> data = Map.ofEntries(
                    entry("grant_type", "authorization_code"),
                    entry("code", authCode),
                    entry("redirect_uri", "http://localhost:3000/fitbit/auth/response"),
                    entry("expires_in", 3600)
            );

            Map<String, String> headers = Map.ofEntries(
                    entry("Authorization", "Basic MjJCVDY2OmRjOGNkNDI0ZjIxNGQ5ZWNmZTA4NGM5N2I4NzQyZWVl"),
                    entry("Content-Type", "application/x-www-form-urlencoded"),
                    entry("charset", "utf-8")
            );

            HttpRequest request = createPostRequest("https://api.fitbit.com/oauth2/token", headers, data);
            return httpClient.send(request, HttpResponse.BodyHandlers.ofString());
        } catch (IOException | InterruptedException e) {
            return null;
        }
    }

    /**
     * send request with POST method for refreshing a token
     * @param refreshToken refresh token
     * @return http response with new token
     */
    public HttpResponse<String> sendFitbitRefreshTokenRequest(String refreshToken) {
        try {
            Map<Object, Object> data = Map.ofEntries(
                    entry("grant_type", "refresh_token"),
                    entry("refresh_token", refreshToken),
                    entry("expires_in", 3600)
            );

            Map<String, String> headers = Map.ofEntries(
                    entry("Authorization", "Basic MjJCVDY2OmRjOGNkNDI0ZjIxNGQ5ZWNmZTA4NGM5N2I4NzQyZWVl"),
                    entry("Content-Type", "application/x-www-form-urlencoded"),
                    entry("charset", "utf-8")
            );

            HttpRequest request = createPostRequest("https://api.fitbit.com/oauth2/token", headers, data);
            return httpClient.send(request, HttpResponse.BodyHandlers.ofString());
        } catch (IOException | InterruptedException e) {
            return null;
        }
    }

    /**
     * send request with GET method to get data from fitbit servers
     * @param uri uri
     * @param authToken token
     * @return http response with data
     */
    public HttpResponse<String> sendFitbitDataRequest(String uri, String authToken) {
        try {
            Map<String, String> headers = Map.ofEntries(
                    entry("Authorization", "Bearer " + authToken)
            );

            HttpRequest request = createGetRequest(uri, headers);
            return httpClient.send(request, HttpResponse.BodyHandlers.ofString());
        } catch (IOException | InterruptedException e) {
            return null;
        }
    }
}
