package springboot.application.healthdataapp.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import springboot.application.healthdataapp.dao.WeightLogDao;
import springboot.application.healthdataapp.helpers.DateConverter;
import springboot.application.healthdataapp.helpers.ObjValidator;
import springboot.application.healthdataapp.model.user.User;
import springboot.application.healthdataapp.model.type.ResponseMessageStatus;
import springboot.application.healthdataapp.model.type.TokenType;
import springboot.application.healthdataapp.model.weight.WeightLog;
import springboot.application.healthdataapp.response.ResponseMessage;

import java.util.List;

@Service
public class WeightService {
    private static final Logger LOG = LoggerFactory.getLogger(WeightService.class);
    @Autowired private WeightLogDao weightLogDao;

    /**
     * find weight logs by date
     * @param date yyyy-MM-dd format
     * @param origin data origin
     * @param user user
     * @return list of weight logs
     */
    @Transactional
    public List<WeightLog> getWeightLogsByDate(String date, TokenType origin, User user) {
        if (origin == TokenType.ALL)
            return weightLogDao.findAllByDateAndUserOrderByTimeAsc(DateConverter.stringToLocalDate(date), user);
        return weightLogDao.findAllByDateAndOriginAndUserOrderByTimeAsc(DateConverter.stringToLocalDate(date), origin, user);
    }

    /**
     * find weight logs by date range
     * @param startDate yyyy-MM-dd format
     * @param endDate yyyy-MM-dd format
     * @param origin data origin
     * @param user user
     * @return list of weight logs
     */
    @Transactional
    public List<WeightLog> getWeightLogsByDateRange(String startDate, String endDate, TokenType origin, User user) {
        if (origin == TokenType.ALL)
            return weightLogDao.findAllByDateBetweenAndUserOrderByDateAsc(DateConverter.stringToLocalDate(startDate), DateConverter.stringToLocalDate(endDate),  user);
        return weightLogDao.findAllByDateBetweenAndOriginAndUserOrderByDateAsc(DateConverter.stringToLocalDate(startDate), DateConverter.stringToLocalDate(endDate), origin, user);
    }

    /**
     * Add new weight log to this server
     *
     * @param weightLog weight log to add to daily weight log
     * @param user      user to which the weight log belongs to
     * @param date      log date
     * @return response with status code and message
     */
    @Transactional
    public ResponseEntity<?> addWeightLog(WeightLog weightLog, User user, String date) {
        weightLog.setUser(user);
        weightLog.setOrigin(TokenType.THIS);

        List<String> violations = ObjValidator.getConstraintViolations(weightLog);
        if (violations.size() != 0) {
            LOG.info("Weight log has not been created due to constraint violation");
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(violations);
        }

        weightLogDao.removeAllByDateAndTimeAndOriginAndUser(weightLog.getDate(), weightLog.getTime(), TokenType.THIS, user);
        weightLogDao.save(weightLog);
        LOG.info("Weight log from " + date + " has been created");
        return ResponseEntity.status(HttpStatus.CREATED).body(new ResponseMessage(201, "Weight log from " + date + " has been created", ResponseMessageStatus.DATA_CREATED));
    }
}
