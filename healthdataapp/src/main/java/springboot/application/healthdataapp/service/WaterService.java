package springboot.application.healthdataapp.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import springboot.application.healthdataapp.dao.WaterLogDao;
import springboot.application.healthdataapp.helpers.DateConverter;
import springboot.application.healthdataapp.helpers.ObjValidator;
import springboot.application.healthdataapp.model.type.ResponseMessageStatus;
import springboot.application.healthdataapp.model.type.TokenType;
import springboot.application.healthdataapp.model.user.User;
import springboot.application.healthdataapp.model.water.WaterLog;
import springboot.application.healthdataapp.response.ResponseMessage;

import java.time.LocalDate;
import java.util.*;

@Service
public class WaterService {
    private static final Logger LOG = LoggerFactory.getLogger(WaterService.class);
    @Autowired private WaterLogDao waterLogDao;

    /**
     * add water log
     * @param waterLog water log
     * @param user user
     * @param date yyyy-MM-dd format
     * @return response with status code and message
     */
    @Transactional
    public ResponseEntity<?> addWaterLog(WaterLog waterLog, User user, String date) {
        waterLog.setUser(user);
        waterLog.setOrigin(TokenType.THIS);

        List<String> violations = ObjValidator.getConstraintViolations(waterLog);
        if (violations.size() != 0) {
            LOG.info("Water log has not been created due to constraint violation");
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(violations);
        }

        waterLogDao.removeByDateAndUserAndOrigin(DateConverter.stringToLocalDate(date), user, TokenType.THIS);
        waterLogDao.save(waterLog);

        LOG.info("Water log from " + date + " has been created");
        return ResponseEntity.status(HttpStatus.CREATED).body(new ResponseMessage(201, "Water log from " + date + " has been created", ResponseMessageStatus.DATA_CREATED));
    }

    /**
     * find water logs from date
     *
     * @param user user to which the log belongs to
     * @param from data origin
     * @param date date in format yyyy-MM-dd
     * @return list of water logs
     */
    @Transactional
    public List<WaterLog> getDailyWaterLog(User user, TokenType from, String date) {
        if (from == TokenType.ALL)
            return waterLogDao.findAllByDateAndUserOrderByDateAsc(LocalDate.parse(date), user);

        return waterLogDao.findAllByDateAndOriginAndUserOrderByDateAsc(LocalDate.parse(date), from, user);
    }

    /**
     * find water logs from date range
     * @param user user
     * @param from data origin
     * @param startDate yyyy-MM-dd format
     * @param endDate yyyy-MM-dd format
     * @return list of water logs
     */
    @Transactional
    public List<WaterLog> getWaterLogsByDateRange(User user, TokenType from, String startDate, String endDate) {
        if (from == TokenType.ALL)
            return waterLogDao.findAllByDateBetweenAndUserOrderByDateAsc(LocalDate.parse(startDate), LocalDate.parse(endDate), user);
        return waterLogDao.findAllByDateBetweenAndOriginAndUserOrderByDateAsc(LocalDate.parse(startDate), LocalDate.parse(endDate), from, user);
    }
}
