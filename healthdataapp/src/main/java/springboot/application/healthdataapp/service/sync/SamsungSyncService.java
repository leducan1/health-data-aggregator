package springboot.application.healthdataapp.service.sync;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;
import springboot.application.healthdataapp.dao.*;
import springboot.application.healthdataapp.helpers.DateConverter;
import springboot.application.healthdataapp.model.activity.ActivityLog;
import springboot.application.healthdataapp.model.blood.BloodPressure;
import springboot.application.healthdataapp.model.dailysummary.DailySummary;
import springboot.application.healthdataapp.model.heart.HeartRate;
import springboot.application.healthdataapp.model.sleep.SleepLevel;
import springboot.application.healthdataapp.model.sleep.SleepLog;
import springboot.application.healthdataapp.model.type.SleepLevelType;
import springboot.application.healthdataapp.model.user.User;
import springboot.application.healthdataapp.model.water.WaterLog;
import springboot.application.healthdataapp.model.weight.WeightLog;
import springboot.application.healthdataapp.service.UserService;

import java.time.Duration;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.*;
import java.util.stream.Collectors;

import static springboot.application.healthdataapp.model.type.TokenType.SAMSUNG;

@Service
public class SamsungSyncService {
    @Autowired private UserService userService;
    @Autowired private DailySummaryDao dailySummaryDao;
    @Autowired private WeightLogDao weightLogDao;
    @Autowired private SleepLogDao sleepLogDao;
    @Autowired private HeartRateDao heartRateDao;
    @Autowired private WaterLogDao waterLogDao;
    @Autowired private BloodPressureDao bloodPressureDao;
    @Autowired private ActivityDao activityDao;

    /**
     * get activity name by id from Samsung servers
     *
     * @param number id
     * @return activity name
     */
    public String getActivityName(int number) {
        String activityName = "";
        switch (number) {
            case 0:
                activityName = "custom";
                break;
            case 1001:
                activityName = "walking";
                break;
            case 1002:
                activityName = "running";
                break;
            case 2001:
                activityName = "baseball";
                break;
            case 2002:
                activityName = "softball";
                break;
            case 2003:
                activityName = "cricket";
                break;
            case 3001:
                activityName = "golf";
                break;
            case 3002:
                activityName = "billiars";
                break;
            case 3003:
                activityName = "bowling";
                break;
            case 4001:
                activityName = "hockey";
                break;
            case 4002:
                activityName = "rugby";
                break;
            case 4003:
                activityName = "basketball";
                break;
            case 4004:
                activityName = "football";
                break;
            case 4005:
                activityName = "handball";
                break;
            case 4006:
                activityName = "american football";
                break;
            case 5001:
                activityName = "volleyball";
                break;
            case 5002:
                activityName = "beach volleyball";
                break;
            case 6001:
                activityName = "squash";
                break;
            case 6002:
                activityName = "tennis";
                break;
            case 6003:
                activityName = "badminton";
                break;
            case 6004:
                activityName = "table tennis";
                break;
            case 6005:
                activityName = "racquetball";
                break;
            case 7003:
                activityName = "martial arts";
                break;
            case 7002:
                activityName = "boxing";
                break;
            case 8001:
                activityName = "ballet";
                break;
            case 8002:
                activityName = "Dancing";
                break;
            case 8003:
                activityName = "ballroom dancing";
                break;
            case 9001:
                activityName = "Pilates";
                break;
            case 9002:
                activityName = "yoga";
                break;
            case 10001:
                activityName = "stretching";
                break;
            case 10002:
                activityName = "jump rope";
                break;
            case 10003:
                activityName = "hula-hopping";
                break;
            case 10004:
                activityName = "push-ups";
                break;
            case 10005:
                activityName = "pull-ups";
                break;
            case 10006:
                activityName = "sit-ups";
                break;
            case 10007:
                activityName = "circuit training";
                break;
            case 10008:
                activityName = "mountain climbers";
                break;
            case 10009:
                activityName = "jumping jacks";
                break;
            case 10010:
                activityName = "burpee";
                break;
            case 10011:
                activityName = "bench press";
                break;
            case 10012:
                activityName = "squats";
                break;
            case 10013:
                activityName = "lunges";
                break;
            case 10014:
                activityName = "leg presses";
                break;
            case 10015:
                activityName = "leg extensions";
                break;
            case 10016:
                activityName = "leg curls";
                break;
            case 10017:
                activityName = "back extensions";
                break;
            case 10018:
                activityName = "lat pull-downs";
                break;
            case 10019:
                activityName = "deadlifts";
                break;
            case 10020:
                activityName = "shoulder presses";
                break;
            case 10021:
                activityName = "front raises";
                break;
            default:
                activityName = "custom";
                break;
        }
        return StringUtils.capitalize(activityName.toLowerCase());
    }

    /**
     * create and persist activity logs from Samsung data
     * @param data activity data
     * @param user user
     * @param startMillis date in milliseconds
     * @param endMillis date in milliseconds
     */
    @Transactional
    public void createAndPersistActivities(JsonNode data, User user, long startMillis, long endMillis) {
        activityDao.removeAllByStartDateTimeBetweenAndOriginAndUser(
                DateConverter.millisToDateTime(startMillis),
                DateConverter.millisToDateTime(endMillis),
                SAMSUNG,
                user);

        List<ActivityLog> activityLogs = new ArrayList<>();

        if (data == null || data.isEmpty() || data.size() == 0) return;

        data.get(0).get("data").forEach(activity -> {
            ActivityLog log = new ActivityLog();
            log.setOrigin(SAMSUNG);

            JsonNode distance = activity.get("distance");
            JsonNode exerciseType = activity.get("exercise_type");
            JsonNode comment = activity.get("comment");
            JsonNode calories = activity.get("calorie");
            JsonNode duration = activity.get("duration");
            JsonNode startTime = activity.get("start_time");
            JsonNode endTime = activity.get("end_time");
            JsonNode speed = activity.get("mean_speed"); // m/s
            JsonNode stepCount = activity.get("count");


            log.setUser(user);

            if (distance != null)
                log.setDistanceInMeters(distance.asDouble());
            if (comment != null)
                log.setDescription(comment.asText());
            if (calories != null)
                log.setCaloriesOut(calories.asInt());
            if (duration != null)
                log.setDurationInSeconds(duration.asInt() / 1000);
            if (startTime != null) {
                log.setStartDateTime(DateConverter.millisToDateTime(startTime.asLong()));
                log.setDate(DateConverter.millisToDate(startTime.asLong()));
            }
            if (speed != null)
                log.setSpeed(speed.asDouble());
            if (stepCount != null)
                log.setSteps(stepCount.asInt());

            String activityName = getActivityName(exerciseType.asInt());

            log.setName(activityName);

            activityLogs.add(log);
        });

        activityDao.saveAll(activityLogs);
    }

    /**
     * create and persist daily steps from Samsung data
     * @param data daily summary data
     * @param user user
     * @param startMillis date in milliseconds
     * @param endMillis date in milliseconds
     */
    @Transactional
    public void createAndPersistDailySteps(JsonNode data, User user, long startMillis, long endMillis) {

        List<DailySummary> dailySummaries = new ArrayList<>();

        if (data == null || data.isEmpty() || data.size() == 0) return;


        data.get(0).get("data").forEach(stepDaily -> {
            DailySummary log = new DailySummary();
            log.setOrigin(SAMSUNG);

            JsonNode distance = stepDaily.get("distance");
            JsonNode calories = stepDaily.get("calorie");
            JsonNode dayTime = stepDaily.get("day_time");
            JsonNode stepCount = stepDaily.get("count");


            log.setUser(user);

            if (distance != null)
                log.setDistance(distance.asDouble());
            if (calories != null)
                log.setCalories(calories.asInt());
            if (dayTime != null)
                log.setDate(DateConverter.millisToDate(dayTime.asLong()));
            if (stepCount != null)
                log.setSteps(stepCount.asInt());

            dailySummaries.add(log);
        });

        dailySummaryDao.removeAllByDateBetweenAndUserAndOrigin(
                DateConverter.millisToDate(startMillis),
                DateConverter.millisToDate(endMillis),
                user,
                SAMSUNG);
        dailySummaryDao.saveAll(dailySummaries);
    }

    /**
     * create and persist weight logs from Samsung data
     * @param data weight data
     * @param user user
     * @param startMillis date in milliseconds
     * @param endMillis date in milliseconds
     */
    @Transactional
    public void createAndPersistWeight(JsonNode data, User user, long startMillis, long endMillis) {

        List<WeightLog> weightLogs = new ArrayList<>();

        if (data == null || data.isEmpty() || data.size() == 0) return;

        data.get(0).get("data").forEach(weight -> {
            WeightLog log = new WeightLog();
            log.setOrigin(SAMSUNG);

            JsonNode weightValue = weight.get("weight");
            JsonNode startTime = weight.get("start_time");

            log.setUser(user);

            if (weightValue != null)
                log.setWeightInKg(weightValue.asDouble());
            if (startTime != null) {
                log.setDate(DateConverter.millisToDate(startTime.asLong()));
                log.setTime(DateConverter.millisToTime(startTime.asLong()));
            }

            weightLogs.add(log);
        });

        weightLogDao.removeAllByDateBetweenAndOriginAndUser(
                DateConverter.millisToDate(startMillis),
                DateConverter.millisToDate(endMillis),
                SAMSUNG,
                user);
        weightLogDao.saveAll(weightLogs);
    }

    /**
     * create and persist sleep logs from Samsung data
     * @param data sleep data
     * @param sleepStages sleep stages
     * @param user user
     * @param startMillis date in milliseconds
     * @param endMillis date in milliseconds
     */
    @Transactional
    public void createAndPersistSleep(JsonNode data, JsonNode sleepStages, User user, long startMillis, long endMillis) {

        if (data == null || data.isEmpty() || data.size() == 0) return;

        List<SleepLog> sleepLogs = new ArrayList<>();

        List<Map<String, Object>> stagesList = new ArrayList<>();
        ObjectMapper mapper = new ObjectMapper();

        if (sleepStages == null || sleepStages.isEmpty() || sleepStages.size() == 0) {

        }
        else {
            sleepStages.get(0).get("data").forEach(stage -> {
                Map<String, Object> result = mapper.convertValue(stage, new TypeReference<>() {
                });
                stagesList.add(result);
            });
        }

        data.get(0).get("data").forEach(sleep -> {
            JsonNode endTime = sleep.get("end_time");
            JsonNode startTime = sleep.get("start_time");
            JsonNode uuid = sleep.get("datauuid");

            LocalDateTime startDateTime = DateConverter.millisToDateTime(startTime.asLong());
            LocalDateTime endDateTime = DateConverter.millisToDateTime(endTime.asLong());
            Duration dur = Duration.between(startDateTime, endDateTime);

            SleepLog sleepLog = new SleepLog();
            sleepLog.setOrigin(SAMSUNG);
            sleepLog.setUser(user);
            sleepLog.setStartDateTime(startDateTime);
            sleepLog.setEndDateTime(endDateTime);
            sleepLog.setDate(DateConverter.millisToDate(startTime.asLong()));
            sleepLog.setDurationInSeconds((int) dur.getSeconds());

            List<Map<String, Object>> filteredStages = stagesList.stream().filter(stage -> stage.get("sleep_id").equals(uuid.asText())).collect(Collectors.toList());
            filteredStages.forEach(stage -> {
                SleepLevel sleepLevel = new SleepLevel();
                sleepLevel.setSleepLog(sleepLog);
                LocalDateTime stageStartTime = DateConverter.millisToDateTime((Long) stage.get("start_time"));
                LocalDateTime stageEndTime = DateConverter.millisToDateTime((Long) stage.get("end_time"));
                Duration duration = Duration.between(stageStartTime, stageEndTime);
                sleepLevel.setDurationInSeconds((int) duration.getSeconds());
                sleepLevel.setStarDateTime(stageStartTime);
                sleepLevel.setEndDateTime(stageEndTime);
                int stageId = (int) stage.get("stage");

                switch (stageId) {
                    case 40001:
                        sleepLevel.setLevelType(SleepLevelType.AWAKE);
                        break;
                    case 40002:
                        sleepLevel.setLevelType(SleepLevelType.LIGHT);
                        break;
                    case 40003:
                        sleepLevel.setLevelType(SleepLevelType.DEEP);
                        break;
                    case 40004:
                        sleepLevel.setLevelType(SleepLevelType.REM);
                        break;
                }

                sleepLog.addSleepLogLevel(sleepLevel);
            });

            sleepLogs.add(sleepLog);

            sleepLog.getSleepLevels().sort(new Comparator<SleepLevel>() {
                @Override public int compare(SleepLevel o1, SleepLevel o2) {
                    return o1.getStarDateTime().compareTo(o2.getStarDateTime());
                }
            });
        });

        sleepLogDao.removeAllByDateBetweenAndOriginAndUserOrderByDateAsc(
                DateConverter.millisToDate(startMillis),
                DateConverter.millisToDate(endMillis),
                SAMSUNG,
                user);
        sleepLogDao.saveAll(sleepLogs);
    }

    /**
     * create and persist heart rate logs from Samsung data
     * @param data heart rate data
     * @param user user
     * @param startMillis date in milliseconds
     * @param endMillis date in milliseconds
     */
    @Transactional
    public void createAndPersistHeartRate(JsonNode data, User user, long startMillis, long endMillis) {

        List<HeartRate> heartRates = new ArrayList<>();

        if (data == null || data.isEmpty() || data.size() == 0) return;

        data.get(0).get("data").forEach(sleep -> {
            HeartRate log = new HeartRate();
            log.setOrigin(SAMSUNG);
            log.setUser(user);

            JsonNode endTime = sleep.get("end_time");
            JsonNode startTime = sleep.get("start_time");
            JsonNode heartRateValue = sleep.get("heart_rate");

            LocalDateTime startDateTime = DateConverter.millisToDateTime(startTime.asLong());
            LocalDate date = DateConverter.millisToDate(startTime.asLong());
            LocalDateTime endDateTime = DateConverter.millisToDateTime(endTime.asLong());

            log.setStartDateTime(startDateTime);
            log.setEndDateTime(endDateTime);
            log.setValue(heartRateValue.asInt());
            log.setDate(date);

            heartRates.add(log);
        });

        heartRateDao.removeAllByStartDateTimeBetweenAndOriginAndUser(
                DateConverter.millisToDateTime(startMillis),
                DateConverter.millisToDateTime(endMillis),
                SAMSUNG,
                user);
        heartRateDao.saveAll(heartRates);
    }

    /**
     * create and persist water logs from Samsung data
     * @param data water data
     * @param user user
     * @param startMillis date in milliseconds
     * @param endMillis date in milliseconds
     */
    @Transactional
    public void createAndPersistWater(JsonNode data, User user, long startMillis, long endMillis) {

        List<WaterLog> waterLogs = new ArrayList<>();

        if (data == null || data.isEmpty() || data.size() == 0) return;

        data.forEach(dataSource -> {
            dataSource.get("data").forEach(water -> {
                JsonNode startTime = water.get("start_time");
                JsonNode amount = water.get("amount");
                LocalDate startDate = DateConverter.millisToDate(startTime.asLong());

                Optional<WaterLog> existingLog = waterLogs.stream().filter(log -> log.getDate().isEqual(startDate)).findAny();

                if (existingLog.isPresent()) {
                    existingLog.get().addAmount(amount.asDouble());
                }
                else {
                    WaterLog log = new WaterLog();
                    log.setOrigin(SAMSUNG);
                    log.setUser(user);
                    log.setDate(startDate);
                    log.setAmount(amount.asDouble());

                    waterLogs.add(log);
                }
            });
        });

        waterLogDao.removeAllByDateBetweenAndUserAndOrigin(
                DateConverter.millisToDate(startMillis),
                DateConverter.millisToDate(endMillis),
                user,
                SAMSUNG);
        waterLogDao.saveAll(waterLogs);
    }

    /**
     * create and persist blood pressure logs from Samsung data
     * @param data activity data
     * @param user user
     * @param startMillis date in milliseconds
     * @param endMillis date in milliseconds
     */
    @Transactional
    public void createAndPersistBloodPressure(JsonNode data, User user, long startMillis, long endMillis) {

        List<BloodPressure> bloodPressures = new ArrayList<>();

        if (data == null || data.isEmpty() || data.size() == 0) return;

        data.get(0).get("data").forEach(bp -> {
            BloodPressure log = new BloodPressure();

            JsonNode startTime = bp.get("start_time");
            JsonNode systolic = bp.get("systolic");
            JsonNode diastolic = bp.get("diastolic");

            LocalDate localDate = DateConverter.millisToDate(startTime.asLong());
            LocalTime localTime = DateConverter.millisToTime(startTime.asLong());

            log.setOrigin(SAMSUNG);
            log.setUser(user);
            log.setDate(localDate);
            log.setTime(localTime);
            log.setDiastolic(diastolic.asInt());
            log.setSystolic(systolic.asInt());

            bloodPressures.add(log);
        });

        bloodPressureDao.removeAllByDateBetweenAndOriginAndUser(
                DateConverter.millisToDate(startMillis),
                DateConverter.millisToDate(endMillis),
                SAMSUNG,
                user);
        bloodPressureDao.saveAll(bloodPressures);
    }

    /**
     * create and persist Samsung data
     * @param data activity data
     * @param user user
     * @param startMillis date in milliseconds
     * @param endMillis date in milliseconds
     */
    @Transactional
    public ResponseEntity<?> createAndPersistDataFromDateRange(JsonNode data, User user, long startMillis, long endMillis) {
        System.out.println("Sync " + LocalDateTime.now().toString());

        if (data == null)
            return ResponseEntity.status(HttpStatus.OK).body(null);

        if (data.get("activities") != null)
            createAndPersistActivities(data.get("activities"), user, startMillis, endMillis);


        if (data.get("dailySteps") != null) {
            try {
                createAndPersistDailySteps(data.get("dailySteps"), user, startMillis, endMillis);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (data.get("weight") != null) {
            try {
                createAndPersistWeight(data.get("weight"), user, startMillis, endMillis);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (data.get("sleep") != null) {
            try {
                createAndPersistSleep(data.get("sleep"), data.get("sleepStage"), user, startMillis, endMillis);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (data.get("heartRate") != null) {
            try {
                createAndPersistHeartRate(data.get("heartRate"), user, startMillis, endMillis);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (data.get("water") != null) {
            try {
                createAndPersistWater(data.get("water"), user, startMillis, endMillis);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        if (data.get("bloodPressure") != null) {
            try {
                createAndPersistBloodPressure(data.get("bloodPressure"), user, startMillis, endMillis);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        return ResponseEntity.status(HttpStatus.OK).body(null);
    }
}