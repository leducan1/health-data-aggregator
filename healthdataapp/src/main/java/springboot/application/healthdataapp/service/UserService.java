package springboot.application.healthdataapp.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;
import springboot.application.healthdataapp.service.authentication.AuthenticationRequest;
import springboot.application.healthdataapp.dao.UserDao;
import springboot.application.healthdataapp.exception.BadCredentialsException;
import springboot.application.healthdataapp.service.authentication.CustomUserDetailsService;
import springboot.application.healthdataapp.service.authentication.JwtUtil;
import springboot.application.healthdataapp.config.PasswordHash;
import springboot.application.healthdataapp.helpers.ObjValidator;
import springboot.application.healthdataapp.model.type.ResponseMessageStatus;
import springboot.application.healthdataapp.model.user.User;
import springboot.application.healthdataapp.response.ResponseMessage;
import springboot.application.healthdataapp.model.type.Role;

import java.util.List;

@Service
public class UserService {
    @Autowired
    private UserDao userDao;

    @Autowired
    private AuthenticationManager authenticationManager;

    @Autowired
    private JwtUtil jwtTokenUtil;

    @Autowired
    private CustomUserDetailsService userDetailsService;

    /**
     * Create a new user
     *
     * @param user new user entity
     * @param role role of the user entity
     * @return response entity with status code 201 if craeted successfully, 409 if account with given e-mail already exists
     */
    public ResponseEntity<?> createUser(User user, Role role) {

        List<String> violations = ObjValidator.getConstraintViolations(user);
        if (violations.size() != 0)
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(violations);

        /* checking if user with that e-mail already exists */
        if (userDao.findByEmail(user.getEmail()) != null)
            return ResponseEntity.status(HttpStatus.CONFLICT).body(new ResponseMessage(409, "Account with this e-mail already exists", ResponseMessageStatus.ACCOUNT_ALREADY_EXISTS));

        /* hash password, set role and persist */
        user.setPassword(PasswordHash.hashPassword(user.getPassword()));
        user.setRole(role);
        userDao.save(user);

        return ResponseEntity.status(HttpStatus.CREATED).body(new ResponseMessage(201, "Account has been created", ResponseMessageStatus.ACCOUNT_CREATED));
    }

    /**
     * Authenticate a user from credentials
     *
     * @param authenticationRequest request containing username (e-mail) and user's password
     * @return response entity with status code 200 if user authenticated or BadCredentialsException which returns status code 401
     */
    public ResponseEntity<?> authenticate(AuthenticationRequest authenticationRequest) {
        User user = userDao.findByEmail(authenticationRequest.getEmail());

        if (user == null)
            throw new BadCredentialsException();

        boolean verifyPassword = PasswordHash.verifyPassword(authenticationRequest.getPassword(), user.getPassword());
        if (!verifyPassword)
            throw new BadCredentialsException();

        authenticationManager.authenticate(new UsernamePasswordAuthenticationToken(authenticationRequest.getEmail(), user.getPassword()));
        final UserDetails userDetails = userDetailsService.loadUserByUsername(authenticationRequest.getEmail());
        final String jwt = jwtTokenUtil.generateToken(userDetails);

        return ResponseEntity.ok(new ResponseMessage(200, jwt, ResponseMessageStatus.AUTHORIZED));
    }

    /**
     * Get user by e-mail
     *
     * @param email e-mail by which to filter
     * @return user entity if user is found, null if not
     */
    public User findByEmail(String email) {
        return userDao.findByEmail(email);
    }
}
