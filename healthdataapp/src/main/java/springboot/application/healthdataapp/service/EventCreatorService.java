package springboot.application.healthdataapp.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;
import springboot.application.healthdataapp.dao.AuthCredentialsDao;
import springboot.application.healthdataapp.dao.UserDao;
import springboot.application.healthdataapp.model.type.TokenType;
import springboot.application.healthdataapp.model.user.AuthCredentials;
import springboot.application.healthdataapp.model.user.User;
import springboot.application.healthdataapp.service.sync.FitbitSyncService;
import springboot.application.healthdataapp.service.sync.GoogleSyncService;

import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Component
public class EventCreatorService {
    @Autowired private AuthCredentialsDao authCredentialsDao;
    @Autowired private UserDao userDao;
    @Autowired private FitbitSyncService fitbitSyncService;
    @Autowired private GoogleSyncService googleSyncService;
    private static final Logger LOG = LoggerFactory.getLogger(EventCreatorService.class);


    /**
     * scheduled method that is performed every hour with an initial delay of 5 seconds
     */
    @Scheduled(fixedRate = 1000 * 60 * 60 * 1, initialDelay = 5000)
    public void synchData() {
        List<User> users = userDao.findAll();

        users.forEach(user -> {
            AuthCredentials fitbitToken = authCredentialsDao.findByUserAndTokenType(user, TokenType.FITBIT);
            AuthCredentials googleToken = authCredentialsDao.findByUserAndTokenType(user, TokenType.GOOGLE);

            String startDate = DateTimeFormatter.ofPattern("yyyy-MM-dd").format(LocalDate.now());
            String endDate = DateTimeFormatter.ofPattern("yyyy-MM-dd").format(LocalDate.now().minusDays(7));

            if (fitbitToken != null) {
                fitbitSyncService.syncFromDateRange(user, endDate, startDate);
                LOG.info("Automatic Fitbit Data Sync For User " + user.getEmail());
            }

            if (googleToken != null) {
                googleSyncService.syncFromDateRange(user, endDate, startDate);
                LOG.info("Automatic Google Data Sync For User " + user.getEmail());
            }
        });
    }
}
