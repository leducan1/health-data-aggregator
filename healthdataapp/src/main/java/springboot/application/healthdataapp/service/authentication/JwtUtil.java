package springboot.application.healthdataapp.service.authentication;

import io.jsonwebtoken.*;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Service;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.function.Function;

@Service
public class JwtUtil {

    private final String SECRET_KEY = "<K#B;TL2~H4Z]5/m#=8)8[cd_$kI6iY4@cEVU=ilNm)2YzOPeJll4!Hy4";
    private final int expirationMs = 1000 * 60 * 60 * 24;

    /**
     * extracts username from token
     * @param token token
     * @return username
     */
    public String extractUsername(String token) {
        return extractClaim(token, Claims::getSubject);
    }

    /**
     * extracts expiration date
     * @param token token
     * @return expiration Date object
     */
    public Date extractExpiration(String token) {
        return extractClaim(token, Claims::getExpiration);
    }

    /**
     * extracts claim from a token
     * @param token token
     * @param claimsResolver claim resolver
     * @param <T> generic object
     * @return extracted claim
     */
    public <T> T extractClaim(String token, Function<Claims, T> claimsResolver) {
        final Claims claims = extractAllClaims(token);
        if (claims != null) {
            return claimsResolver.apply(claims);
        }
        return null;
    }
    private Claims extractAllClaims(String token) {
        try {
            return Jwts.parser().setSigningKey(SECRET_KEY).parseClaimsJws(token).getBody();
        } catch (ExpiredJwtException e) {
            e.printStackTrace();
            return null;
        } catch (MalformedJwtException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * checks if token is expired
     * @param token token
     * @return false if expired, else true
     */
    public Boolean isTokenExpired(String token) {
        return extractExpiration(token).before(new Date());
    }

    /**
     * generates a token
     * @param userDetails object containing user details
     * @return jwt token
     */
    public String generateToken(UserDetails userDetails) {
        Map<String, Object> claims = new HashMap<>();
        return createToken(claims, userDetails.getUsername());
    }

    /**
     * creates a token
     * @param claims claims
     * @param subject subjet
     * @return signed jwt with expiration date
     */
    private String createToken(Map<String, Object> claims, String subject) {
        return Jwts.builder().setClaims(claims).setSubject(subject).setIssuedAt(new Date(System.currentTimeMillis()))
                .setExpiration(new Date(System.currentTimeMillis() + expirationMs))
                .signWith(SignatureAlgorithm.HS256, SECRET_KEY).compact();
    }

    /**
     * checks if token is valid
     * @param token token
     * @param userDetails user datails
     * @return false if token is valid, else true
     */
    public Boolean validateToken(String token, UserDetails userDetails) {
        final String username = extractUsername(token);
        return (username.equals(userDetails.getUsername()) && !isTokenExpired(token));
    }

    /**
     *
     * @param tokenWithBearer token with 'Bearer '
     * @return token without 'Bearer ' at the beginning of the string
     */
    public String extractTokenPart(String tokenWithBearer) {
        return tokenWithBearer.substring(7);
    }
}