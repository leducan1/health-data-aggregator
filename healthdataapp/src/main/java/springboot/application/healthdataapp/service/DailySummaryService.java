package springboot.application.healthdataapp.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import springboot.application.healthdataapp.dao.DailySummaryDao;
import springboot.application.healthdataapp.helpers.DateConverter;
import springboot.application.healthdataapp.helpers.ObjValidator;
import springboot.application.healthdataapp.model.blood.BloodPressure;
import springboot.application.healthdataapp.model.dailysummary.DailySummary;
import springboot.application.healthdataapp.model.type.ResponseMessageStatus;
import springboot.application.healthdataapp.model.type.TokenType;
import springboot.application.healthdataapp.model.user.User;
import springboot.application.healthdataapp.response.ResponseMessage;

import java.util.List;

@Service
public class DailySummaryService {
    private static final Logger LOG = LoggerFactory.getLogger(BloodPressure.class);
    @Autowired private DailySummaryDao dailySummaryDao;

    /**
     * find all daily summary logs ordered by date asc
     * @param user user
     * @param from data origin
     * @param date yyyy-MM-dd format
     * @return list of daily summary logs
     */
    public List<DailySummary> getDailySummaryLog(User user, TokenType from, String date) {
        if (from == TokenType.ALL)
            return dailySummaryDao.findAllByDateAndUserOrderByDateAsc(DateConverter.stringToLocalDate(date), user);
        return dailySummaryDao.findAllByDateAndOriginAndUserOrderByDateAsc(DateConverter.stringToLocalDate(date), from, user);
    }

    /**
     * add a daily summary
     * @param log daily summary log
     * @param user user
     * @return response with status code and message
     */
    @Transactional
    public ResponseEntity<?> addDailySummary(DailySummary log, User user) {
        log.setOrigin(TokenType.THIS);
        log.setUser(user);

        List<String> violations = ObjValidator.getConstraintViolations(log);
        if (violations.size() != 0) {
            LOG.info("Daily summary log has not been created due to constraint violation");
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(violations);
        }

        dailySummaryDao.removeAllByDateBetweenAndUserAndOrigin(log.getDate(),log.getDate(), user, TokenType.THIS);
        dailySummaryDao.save(log);
        LOG.info("Daily summary from " + log.getDate().toString() + " has been created");
        return ResponseEntity.status(HttpStatus.CREATED).body(new ResponseMessage(201, "Blood pressure log from " + log.getDate() + " has been created", ResponseMessageStatus.DATA_CREATED));
    }
}
