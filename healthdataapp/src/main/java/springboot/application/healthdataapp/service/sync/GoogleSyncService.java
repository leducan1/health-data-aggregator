package springboot.application.healthdataapp.service.sync;

import com.fasterxml.jackson.databind.JsonNode;
import net.minidev.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import springboot.application.healthdataapp.exception.*;
import springboot.application.healthdataapp.helpers.DateConverter;
import springboot.application.healthdataapp.model.activity.ActivityLog;
import springboot.application.healthdataapp.model.blood.BloodPressure;
import springboot.application.healthdataapp.model.sleep.SleepLog;
import springboot.application.healthdataapp.model.user.AuthCredentials;
import springboot.application.healthdataapp.model.user.User;
import springboot.application.healthdataapp.model.weight.WeightLog;
import springboot.application.healthdataapp.request.AuthCodeRequest;
import springboot.application.healthdataapp.request.google.GoogleActivityRequest;
import springboot.application.healthdataapp.request.google.GoogleBloodPressureRequest;
import springboot.application.healthdataapp.request.google.GoogleWeightRequest;
import springboot.application.healthdataapp.response.ResponseMessage;
import springboot.application.healthdataapp.model.type.TokenType;
import springboot.application.healthdataapp.model.type.ResponseMessageStatus;

import java.net.http.HttpResponse;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

@Service
public class GoogleSyncService extends BaseSyncService {
    private final String clientId = "807765000508-ia0jgufgiol7ampbmuh9pqccvi41gd4u.apps.googleusercontent.com";
    private final String clientSecret = "bAdS_qiQL4HUtATn4JDmqFWb";
    private final String redirectUri = "http://localhost:3000/google/auth/response";
    private final String scopes = "profile%20email%20" +
            "https://www.googleapis.com/auth/fitness.blood_glucose.read%20" +
            "https://www.googleapis.com/auth/fitness.activity.read%20" +
            "https://www.googleapis.com/auth/fitness.blood_pressure.read%20" +
            "https://www.googleapis.com/auth/fitness.body.read%20" +
            "https://www.googleapis.com/auth/fitness.body_temperature.read%20" +
            "https://www.googleapis.com/auth/fitness.heart_rate.read%20" +
            "https://www.googleapis.com/auth/fitness.location.read%20" +
            "https://www.googleapis.com/auth/fitness.nutrition.read%20" +
            "https://www.googleapis.com/auth/fitness.oxygen_saturation.read%20" +
            "https://www.googleapis.com/auth/fitness.sleep.read%20";

    @Override
    public JSONObject getConsentScreenURL() {
        return new JSONObject() {{
            put("url", "https://accounts.google.com/o/oauth2/v2/auth?" + "client_id=" + clientId + "&response_type=code" + "&scope=" + scopes + "&redirect_uri=" + redirectUri + "&access_type=offline");
        }};
    }

    @Override
    public ResponseEntity<?> getAccessToken(User user, AuthCodeRequest googleAuthCodeRequest) {
        HttpResponse<String> response = requestSender.sendGoogleAccessTokenRequest(googleAuthCodeRequest.getAuthCode());

        AuthCredentials existingCredentials = authTokenRepository.findByUserAndTokenType(user, TokenType.GOOGLE);

        if (existingCredentials == null) {
            if (response.statusCode() == 200) {
                saveToken(user, convertToJsonNode(response.body()), TokenType.GOOGLE, scopes.replace("%20", " "));
                return ResponseEntity.status(HttpStatus.OK).body("Google authorization succesful");
            }
            throw new AccessTokenNotGrantedException("Google authorization failed");
        }
        refreshAccessToken(user);
        return ResponseEntity.status(HttpStatus.OK).body("Google authorization succesful");
    }

    @Override
    public AuthCredentials refreshAccessToken(User user) {
        AuthCredentials authCredentials = authTokenRepository.findByUserAndTokenType(user, TokenType.GOOGLE);

        HttpResponse<String> response = requestSender.sendGoogleRefreshTokenRequest(authCredentials.getRefreshToken());

        if (response.statusCode() == 200) {
            JsonNode json = convertToJsonNode(response.body());
            updateToken(authCredentials, json.get("access_token").textValue(), authCredentials.getRefreshToken(), json.get("scope").textValue());
            return authCredentials;
        }
        throw new InvalidRefreshTokenException("Google refresh token is invalid");
    }

    /**
     * send a request with POST method to Google servers
     *
     * @param uri      uri
     * @param postBody body of the post request
     * @param user     user
     * @return
     */
    private JsonNode fetchPostData(String uri, Object postBody, User user) {
        AuthCredentials authCredentials = authTokenRepository.findByUserAndTokenType(user, TokenType.GOOGLE);

        if (authCredentials == null)
            throw new AccessTokenNotFoundException("Google access token not found in database", ResponseMessageStatus.TOKEN_NOT_FOUND);

        HttpResponse<String> response = requestSender.sendGooglePostRequest(uri, authCredentials, postBody);

        if (response.statusCode() != 200) {
            authCredentials = refreshAccessToken(user);
            response = requestSender.sendGooglePostRequest(uri, authCredentials, postBody);
        }

        return convertToJsonNode(response.body());
    }

    /**
     * get activity name by id from Google servers
     *
     * @param activityTypeId id
     * @return activity name
     */
    private String getActivityName(int activityTypeId) {
        String activityName = "";
        switch (activityTypeId) {
            case 9:
                activityName = "Aerobics";
                break;
            case 119:
                activityName = "Archery";
                break;
            case 10:
                activityName = "Badminton";
                break;
            case 11:
                activityName = "Baseball";
                break;
            case 12:
                activityName = "Basketball";
                break;
            case 13:
                activityName = "Biathlon";
                break;
            case 1:
                activityName = "Biking";
                break;
            case 14:
                activityName = "Handbiking";
                break;
            case 15:
                activityName = "Mountain biking";
                break;
            case 16:
                activityName = "Road biking";
                break;
            case 17:
                activityName = "Spinning";
                break;
            case 18:
                activityName = "Stationary biking";
                break;
            case 19:
                activityName = "Utility biking";
                break;
            case 20:
                activityName = "Boxing";
                break;
            case 21:
                activityName = "Calisthenics";
                break;
            case 22:
                activityName = "Circuit training";
                break;
            case 23:
                activityName = "Cricket";
                break;
            case 113:
                activityName = "Crossfit";
                break;
            case 106:
                activityName = "Curling";
                break;
            case 24:
                activityName = "Dancing";
                break;
            case 102:
                activityName = "Diving";
                break;
            case 117:
                activityName = "Elevator";
                break;
            case 25:
                activityName = "Elliptical";
                break;
            case 103:
                activityName = "Ergometer";
                break;
            case 118:
                activityName = "Escalator";
                break;
            case 26:
                activityName = "Fencing";
                break;
            case 27:
                activityName = "Football (American)";
                break;
            case 28:
                activityName = "Football (Australian)";
                break;
            case 29:
                activityName = "Football (Soccer)";
                break;
            case 30:
                activityName = "Frisbee";
                break;
            case 31:
                activityName = "Gardening";
                break;
            case 32:
                activityName = "Golf";
                break;
            case 122:
                activityName = "Guided Breathing";
                break;
            case 33:
                activityName = "Gymnastics";
                break;
            case 34:
                activityName = "Handball";
                break;
            case 114:
                activityName = "HIIT";
                break;
            case 35:
                activityName = "Hiking";
                break;
            case 36:
                activityName = "Hockey";
                break;
            case 37:
                activityName = "Horseback riding";
                break;
            case 38:
                activityName = "Housework";
                break;
            case 104:
                activityName = "Ice skating";
                break;
            case 0:
                activityName = "In vehicle";
                break;
            case 115:
                activityName = "Interval Training";
                break;
            case 39:
                activityName = "Jumping rope";
                break;
            case 40:
                activityName = "Kayaking";
                break;
            case 41:
                activityName = "Kettlebell training";
                break;
            case 42:
                activityName = "Kickboxing";
                break;
            case 43:
                activityName = "Kitesurfing";
                break;
            case 44:
                activityName = "Martial arts";
                break;
            case 45:
                activityName = "Meditation";
                break;
            case 46:
                activityName = "Mixed martial arts";
                break;
            case 108:
                activityName = "Other";
                break;
            case 47:
                activityName = "P90X exercises";
                break;
            case 48:
                activityName = "Paragliding";
                break;
            case 49:
                activityName = "Pilates";
                break;
            case 50:
                activityName = "Polo";
                break;
            case 51:
                activityName = "Racquetball";
                break;
            case 52:
                activityName = "Rock climbing";
                break;
            case 53:
                activityName = "Rowing";
                break;
            case 54:
                activityName = "Rowing machine";
                break;
            case 55:
                activityName = "Rugby";
                break;
            case 8:
                activityName = "Running";
                break;
            case 56:
                activityName = "Jogging";
                break;
            case 57:
                activityName = "Running on sand";
                break;
            case 58:
                activityName = "Running (treadmill)";
                break;
            case 59:
                activityName = "Sailing";
                break;
            case 60:
                activityName = "Scuba diving";
                break;
            case 61:
                activityName = "Skateboarding";
                break;
            case 62:
                activityName = "Skating";
                break;
            case 63:
                activityName = "Cross skating";
                break;
            case 105:
                activityName = "Indoor skating";
                break;
            case 64:
                activityName = "Inline skating (rollerblading)";
                break;
            case 65:
                activityName = "Skiing";
                break;
            case 66:
                activityName = "Back-country skiing";
                break;
            case 67:
                activityName = "Cross-country skiing";
                break;
            case 68:
                activityName = "Downhill skiing";
                break;
            case 69:
                activityName = "Kite skiing";
                break;
            case 70:
                activityName = "Roller skiing";
                break;
            case 71:
                activityName = "Sledding";
                break;
            case 72:
                activityName = "Sleep";
                break;
            case 73:
                activityName = "Snowboarding";
                break;
            case 74:
                activityName = "Snowmobile";
                break;
            case 75:
                activityName = "Snowshoeing";
                break;
            case 120:
                activityName = "Softball";
                break;
            case 76:
                activityName = "Squash";
                break;
            case 77:
                activityName = "Stair climbing";
                break;
            case 78:
                activityName = "Stair-climbing machine";
                break;
            case 79:
                activityName = "Stand-up paddleboarding";
                break;
            case 3:
                activityName = "Still (not moving)";
                break;
            case 80:
                activityName = "Strength training";
                break;
            case 81:
                activityName = "Surfing";
                break;
            case 82:
                activityName = "Swimming";
                break;
            case 84:
                activityName = "Swimming (open water)";
                break;
            case 83:
                activityName = "Swimming (swimming pool)";
                break;
            case 85:
                activityName = "Table tennis (ping pong)";
                break;
            case 86:
                activityName = "Team sports";
                break;
            case 87:
                activityName = "Tennis";
                break;
            case 5:
                activityName = "Tilting (sudden device gravity change)";
                break;
            case 88:
                activityName = "Treadmill (walking or running)";
                break;
            case 4:
                activityName = "Unknown (unable to detect activity)";
                break;
            case 89:
                activityName = "Volleyball";
                break;
            case 90:
                activityName = "Volleyball (beach)";
                break;
            case 91:
                activityName = "Volleyball (indoor)";
                break;
            case 92:
                activityName = "Wakeboarding";
                break;
            case 7:
                activityName = "Walking";
                break;
            case 93:
                activityName = "Walking (fitness)";
                break;
            case 94:
                activityName = "Nording walking";
                break;
            case 95:
                activityName = "Walking (treadmill)";
                break;
            case 116:
                activityName = "Walking (stroller)";
                break;
            case 96:
                activityName = "Waterpolo";
                break;
            case 97:
                activityName = "Weightlifting";
                break;
            case 98:
                activityName = "Wheelchair";
                break;
            case 99:
                activityName = "Windsurfing";
                break;
            case 100:
                activityName = "Yoga";
                break;
            case 101:
                activityName = "Zumba";
                break;
            default:
                activityName = "Other";
                break;
        }
        return activityName;
    }

    /**
     * Creates a google activity log from data on google servers
     *
     * @param json json representation of data
     * @param user id of user whose data belong to
     * @return activity log created from data on google servers
     */
    private ActivityLog createActivityLog(JsonNode json, User user) {
        ActivityLog log = new ActivityLog();
        log.setUser(user);

        JsonNode dataset = json.get("dataset");
        JsonNode session = json.get("session");
        long endTimeMillis = Long.parseLong(json.get("endTimeMillis").textValue());
        long startTimeMillis = Long.parseLong(json.get("startTimeMillis").textValue());
        long durationInSec = (endTimeMillis - startTimeMillis) / 1000;

        dataset.forEach(set -> {
            String dataSourceId = set.get("dataSourceId").textValue();

            JsonNode point = set.get("point");
            if (point != null && point.get(0) != null) {
                JsonNode value = point.get(0).get("value").get(0);
                if (dataSourceId.contains("step_count")) {
                    if (value.get("intVal") != null)
                        log.setSteps(value.get("intVal").asInt());
                    else
                        log.setSteps(Integer.parseInt(value.get("fpVal").toString()));
                }
                else if (dataSourceId.contains("calories")) {
                    if (value.get("intVal") != null)
                        log.setCaloriesOut(value.get("intVal").asInt());
                    else
                        log.setCaloriesOut(value.get("fpVal").asInt());
                }
                else if (dataSourceId.contains("distance")) {
                    if (value.get("intVal") != null)
                        log.setDistanceInMeters(point.get(0).get("value").get(0).get("intVal").asDouble());
                    else
                        log.setDistanceInMeters(point.get(0).get("value").get(0).get("fpVal").asDouble());
                }
            }
        });

        log.setStartDateTime(DateConverter.millisToDateTime(startTimeMillis));
        log.setDate(DateConverter.millisToDate(startTimeMillis));
        log.setDurationInSeconds(Integer.parseInt(Long.toString(durationInSec)));
        log.setOrigin(TokenType.GOOGLE);
        log.setName(session.get("name").textValue());

        log.setDescription(session.get("description").textValue());

        int activityTypeId = session.get("activityType").asInt();

        String activityName = getActivityName(activityTypeId);

        if (log.getName().equals(""))
            log.setName(activityName);

        return log;
    }

    /**
     * create and persist sleep logs
     *
     * @param json      JsonNode object containing data
     * @param startDate start date in yyyy-MM-dd format
     * @param endDate   start date in yyyy-MM-dd format
     * @param user      user
     */
    @Transactional
    public void createAndPersistActivitySleepLogs(JsonNode json, String startDate, String endDate, User user) {
        List<ActivityLog> activityLogList = new ArrayList<>();
        for (JsonNode record : json.get("bucket")) {
            ActivityLog activityLog = createActivityLog(record, user);
            if (!activityLog.getName().equalsIgnoreCase("Sleep")) {
                activityLogList.add(activityLog);
            }
            else if (activityLog.getName().equalsIgnoreCase("Sleep")) {
                SleepLog sleepLog = new SleepLog();
                sleepLog.setUser(user);
                long endTimeMillis = Long.parseLong(record.get("endTimeMillis").textValue());
                long startTimeMillis = Long.parseLong(record.get("startTimeMillis").textValue());
                long durationInSec = (endTimeMillis - startTimeMillis) / 1000;

                LocalDate localDate = DateConverter.millisToDate(startTimeMillis);
                sleepLog.setDate(localDate);
                sleepLog.setStartDateTime(DateConverter.millisToDateTime(startTimeMillis));
                sleepLog.setEndDateTime(DateConverter.millisToDateTime(endTimeMillis));
                sleepLog.setDurationInSeconds((int) durationInSec);
                sleepLog.setOrigin(TokenType.GOOGLE);
                sleepLogRepository.removeAllByDateAndOriginAndUser(localDate, TokenType.GOOGLE, user);
                sleepLogRepository.save(sleepLog);
            }
        }

        activityDao.removeAllByStartDateTimeBetweenAndOriginAndUser(DateConverter.stringToLocalDateTime(startDate + " 00:00:00", "yyyy-MM-dd HH:mm:ss"), DateConverter.stringToLocalDateTime(endDate + " 00:00:00", "yyyy-MM-dd HH:mm:ss"), TokenType.GOOGLE, user);
        activityDao.saveAll(activityLogList);
    }

    /**
     * create and persist weight logs
     *
     * @param json      JsonNode object containing data
     * @param startDate date in yyyy-MM-dd format
     * @param endDate   date in yyyy-MM-dd format
     * @param user      user
     */
    @Transactional
    public void createAndPersistWeightLogs(JsonNode json, String startDate, String endDate, User user) {
        List<WeightLog> weightLogList = new ArrayList<>();
        for (JsonNode data : json.get("bucket")) {

            JsonNode points = data.get("dataset").get(0).get("point");
            for (JsonNode record : points) {
                WeightLog weightLog = new WeightLog();

                long millis = record.get("startTimeNanos").asLong() / 1000000;
                double weight = record.get("value").get(0).get("fpVal").asDouble();
                weightLog.setWeightInKg(weight);
                weightLog.setDate(DateConverter.millisToDate(millis));
                weightLog.setTime(DateConverter.millisToTime(millis));
                weightLog.setOrigin(TokenType.GOOGLE);
                weightLog.setUser(user);
                weightLogList.add(weightLog);
            }
        }
        weightLogDao.removeAllByDateBetweenAndOriginAndUser(DateConverter.stringToLocalDate(startDate), DateConverter.stringToLocalDate(endDate), TokenType.GOOGLE, user);
        weightLogDao.saveAll(weightLogList);
    }

    /**
     * create and persist blood pressure logs
     *
     * @param json      JsonNode object containing data
     * @param startDate start date in yyyy-MM-dd format
     * @param endDate   start date in yyyy-MM-dd format
     * @param user      user
     */
    @Transactional
    public void createAndPersistBloodPressureLogs(JsonNode json, String startDate, String endDate, User user) {
        JsonNode dataset = json.get("bucket").get(0).get("dataset");
        List<BloodPressure> bloodPressureList = new ArrayList<>();


        dataset.forEach(set -> {
            JsonNode point = set.get("point");

            if (point != null && point.get(0) != null) {
                point.forEach(bpPoint -> {
                    long millis = bpPoint.get("startTimeNanos").asLong() / 1000000;
                    BloodPressure bloodPressure = new BloodPressure();
                    bloodPressure.setSystolic(bpPoint.get("value").get(0).get("fpVal").asInt());
                    bloodPressure.setDiastolic(bpPoint.get("value").get(1).get("fpVal").asInt());
                    bloodPressure.setDate(DateConverter.millisToDate(millis));
                    bloodPressure.setTime(DateConverter.millisToTime(millis));
                    bloodPressure.setOrigin(TokenType.GOOGLE);
                    bloodPressure.setUser(user);
                    bloodPressureList.add(bloodPressure);
                });
            }
        });

        bloodPressureDao.removeAllByDateBetweenAndOriginAndUser(DateConverter.stringToLocalDate(startDate), DateConverter.stringToLocalDate(endDate), TokenType.GOOGLE, user);
        bloodPressureDao.saveAll(bloodPressureList);
    }

    /**
     * sync activity data from Google servers
     *
     * @param user      user
     * @param startDate date in yyyy-MM-dd format
     * @param endDate   date in yyyy-MM-dd format
     */
    @Transactional
    public void syncActivitiesAndSleep(User user, String startDate, String endDate) {
        String uri = "https://www.googleapis.com/fitness/v1/users/me/dataset:aggregate";
        JsonNode json = fetchPostData(uri, new GoogleActivityRequest(startDate, endDate), user);

        /* if no logs found in json object */
        if (json == null || json.get("bucket") == null || json.get("bucket").size() == 0)
            return;

        createAndPersistActivitySleepLogs(json, startDate, endDate, user);
    }

    /**
     * sync blood pressure data from Google servers
     *
     * @param user      user
     * @param startDate date in yyyy-MM-dd format
     * @param endDate   date in yyyy-MM-dd format
     */
    @Transactional
    public void syncBloodPressure(User user, String startDate, String endDate) {
        String uri = "https://www.googleapis.com/fitness/v1/users/me/dataset:aggregate";
        JsonNode json = fetchPostData(uri, new GoogleBloodPressureRequest(startDate, endDate), user);

        /* if no logs found in json object */
        if (json == null || json.get("bucket") == null || json.get("bucket").size() == 0)
            return;

        createAndPersistBloodPressureLogs(json, startDate, endDate, user);
    }

    /**
     * sync weight data from Google servers
     *
     * @param user      user
     * @param startDate date in yyyy-MM-dd format
     * @param endDate   date in yyyy-MM-dd format
     */
    @Transactional
    public void syncWeight(User user, String startDate, String endDate) {
        String uri = "https://www.googleapis.com/fitness/v1/users/me/dataset:aggregate";
        JsonNode json = fetchPostData(uri, new GoogleWeightRequest(startDate, endDate), user);

        /* if no logs found in json object */
        if (json == null || json.get("bucket") == null || json.get("bucket").size() == 0)
            return;

        createAndPersistWeightLogs(json, startDate, endDate, user);
    }

    /**
     * sync data from google servers
     *
     * @param startDate start date
     * @param endDate   end date
     * @param user      user
     */
    @Transactional
    public ResponseEntity<?> syncFromDateRange(User user, String startDate, String endDate) {
        AuthCredentials authCredentials = authTokenRepository.findByUserAndTokenType(user, TokenType.GOOGLE);

        if (authCredentials == null)
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ResponseMessage(400, "Access to Google data has not been granted yet!", ResponseMessageStatus.TOKEN_NOT_FOUND));

        syncWeight(user, startDate, endDate);
        syncBloodPressure(user, startDate, endDate);
        syncActivitiesAndSleep(user, startDate, endDate);

        return ResponseEntity.status(HttpStatus.CREATED).body(new ResponseMessage(201, "Google data have been synchronized", ResponseMessageStatus.DATA_SAVED));
    }
}