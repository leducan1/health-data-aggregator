package springboot.application.healthdataapp.service.sync;

import com.fasterxml.jackson.databind.JsonNode;
import net.minidev.json.JSONObject;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import springboot.application.healthdataapp.exception.AccessTokenNotFoundException;
import springboot.application.healthdataapp.exception.AccessTokenNotGrantedException;
import springboot.application.healthdataapp.exception.InvalidRefreshTokenException;
import springboot.application.healthdataapp.helpers.DateConverter;
import springboot.application.healthdataapp.model.activity.ActivityLog;
import springboot.application.healthdataapp.model.activity.ActivityMinutes;
import springboot.application.healthdataapp.model.activity.HRZone;
import springboot.application.healthdataapp.model.dailysummary.DailySummary;
import springboot.application.healthdataapp.model.sleep.SleepLevel;
import springboot.application.healthdataapp.model.sleep.SleepLog;
import springboot.application.healthdataapp.model.type.HRZoneType;
import springboot.application.healthdataapp.model.type.ResponseMessageStatus;
import springboot.application.healthdataapp.model.type.SleepLevelType;
import springboot.application.healthdataapp.model.type.TokenType;
import springboot.application.healthdataapp.model.user.AuthCredentials;
import springboot.application.healthdataapp.model.user.User;
import springboot.application.healthdataapp.model.water.WaterLog;
import springboot.application.healthdataapp.model.weight.WeightLog;
import springboot.application.healthdataapp.request.AuthCodeRequest;
import springboot.application.healthdataapp.response.ResponseMessage;

import java.net.http.HttpResponse;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

@Service
public class FitbitSyncService extends BaseSyncService {
    @Override
    public JSONObject getConsentScreenURL() {
        return new JSONObject() {{
            put("url", "https://www.fitbit.com/oauth2/authorize?response_type=code&client_id=22BT66&redirect_uri=http%3A%2F%2Flocalhost%3A3000%2Ffitbit%2Fauth%2Fresponse&scope=activity%20heartrate%20location%20nutrition%20profile%20settings%20sleep%20social%20weight&expires_in=604800");
        }};
    }

    /**
     * Creates a sleep log created from fitbit data
     *
     * @param json json representation of data
     * @param user to which data belong to
     * @return created fitbit sleep log
     */
    public SleepLog createSleepLog(JsonNode json, User user) {
        SleepLog sleepLog = new SleepLog();
        sleepLog.setUser(user);
        sleepLog.setOrigin(TokenType.FITBIT);

        sleepLog.setDate(DateConverter.stringToLocalDate(json.get("dateOfSleep").textValue()));
        sleepLog.setStartDateTime(DateConverter.stringToLocalDateTime(json.get("startTime").textValue(), "yyyy-MM-dd HH:mm:ss"));
        sleepLog.setEndDateTime(DateConverter.stringToLocalDateTime(json.get("endTime").textValue(), "yyyy-MM-dd HH:mm:ss"));
        sleepLog.setEfficiency(json.get("efficiency").asInt());
        sleepLog.setDurationInSeconds(json.get("duration").asInt() / 1000);
        sleepLog.setSecondsAsleep(json.get("minutesAsleep").asInt() * 60);
        sleepLog.setSecondsAwake(json.get("minutesAwake").asInt() * 60);

        for (JsonNode level : json.get("levels").get("data")) {
            SleepLevel sleepLevel = createFitbitSleepLevel(level, sleepLog);
            sleepLevel.setSleepLog(sleepLog);
            sleepLog.addSleepLogLevel(sleepLevel);
        }

        return sleepLog;
    }

    /**
     * Creates a sleep level
     * This object has no meaning on its own, it is an object in a list of sleep levels in a fitbit sleep log
     *
     * @param level json representation of a sleep level
     * @return created fitbit sleep level
     */
    public SleepLevel createFitbitSleepLevel(JsonNode level, SleepLog sleepLog) {
        SleepLevel sleepLevel = new SleepLevel();
        sleepLevel.setSleepLog(sleepLog);
        sleepLevel.setStarDateTime(DateConverter.stringToLocalDateTime(level.get("dateTime").textValue(), "yyyy-MM-dd HH:mm:ss"));
        sleepLevel.setDurationInSeconds(Integer.parseInt(level.get("seconds").toString()));
        switch (level.get("level").textValue()) {
            case "restless":
                sleepLevel.setLevelType(SleepLevelType.RESTLESS);
                break;
            case "awake":
                sleepLevel.setLevelType(SleepLevelType.AWAKE);
                break;
            case "asleep":
                sleepLevel.setLevelType(SleepLevelType.ASLEEP);
                break;
            case "deep":
                sleepLevel.setLevelType(SleepLevelType.DEEP);
                break;
            case "light":
                sleepLevel.setLevelType(SleepLevelType.LIGHT);
                break;
            case "rem":
                sleepLevel.setLevelType(SleepLevelType.REM);
                break;
            case "wake":
                sleepLevel.setLevelType(SleepLevelType.WAKE);
                break;
        }
        return sleepLevel;
    }

    /**
     * fetch data from fitbit servers
     *
     * @param uri  uri
     * @param user user
     * @return data from fitbit server
     */
    private JsonNode fetchData(String uri, User user) {
        AuthCredentials authCredentials = authCredentialsDao.findByUserAndTokenType(user, TokenType.FITBIT);

        if (authCredentials == null)
            throw new AccessTokenNotFoundException("Fitbit access token not found in database", ResponseMessageStatus.TOKEN_NOT_FOUND);

        HttpResponse<String> response = requestSender.sendFitbitDataRequest(uri, authCredentials.getAccessToken());

        if (response.statusCode() != 200) {
            authCredentials = refreshAccessToken(user);
            response = requestSender.sendFitbitDataRequest(uri, authCredentials.getAccessToken());
        }

        return convertToJsonNode(response.body());
    }

    @Transactional
    @Override
    public ResponseEntity<?> getAccessToken(User user, AuthCodeRequest authCodeRequest) {
        authCredentialsDao.removeAllByUserAndTokenType(user, TokenType.FITBIT);
        HttpResponse<String> response = requestSender.sendFitbitAccessTokenRequest(authCodeRequest.getAuthCode());

        if (response.statusCode() == 200) {
            saveToken(user, convertToJsonNode(response.body()), TokenType.FITBIT, null);
            return ResponseEntity.status(HttpStatus.OK).body("Fitbit authorization succesful");
        }

        throw new AccessTokenNotGrantedException("Fitbit authorization failed");
    }

    @Transactional
    @Override
    public AuthCredentials refreshAccessToken(User user) {
        AuthCredentials authCredentials = authCredentialsDao.findByUserAndTokenType(user, TokenType.FITBIT);
        HttpResponse<String> response = requestSender.sendFitbitRefreshTokenRequest(authCredentials.getRefreshToken());

        if (response.statusCode() == 200) {
            JsonNode json = convertToJsonNode(response.body());
            updateToken(authCredentials, json.get("access_token").textValue(), json.get("refresh_token").textValue(), null);
            return authCredentials;
        }

        throw new InvalidRefreshTokenException("Fitbit refresh token is invalid");
    }

    /**
     * create and persist sleep logs from data from fitbit servers
     *
     * @param json      JsonNode object containing data
     * @param startDate start date
     * @param endDate   end date
     * @param user      user
     */
    @Transactional
    public void createAndPersistSleepLogs(JsonNode json, LocalDate startDate, LocalDate endDate, User user) {
        List<SleepLog> sleepLogList = new ArrayList<>();

        for (JsonNode node : json.fields().next().getValue()) {

            if (node != null) {
                SleepLog sleepLog = createSleepLog(node, user);
                sleepLogList.add(sleepLog);
            }
        }

        sleepLogRepository.removeAllByDateBetweenAndOriginAndUserOrderByDateAsc(startDate, endDate, TokenType.FITBIT, user);
        sleepLogRepository.saveAll(sleepLogList);
    }

    /**
     * create and persist water logs from data from fitbit servers
     *
     * @param json      JsonNode object containing data
     * @param startDate start date
     * @param endDate   end date
     * @param user      user
     */
    @Transactional
    public void createAndPersistWaterLogs(JsonNode json, LocalDate startDate, LocalDate endDate, User user) {
        List<WaterLog> waterLogList = new ArrayList<>();
        for (JsonNode record : json.get("foods-log-water")) {
            double waterValue = record.get("value").asDouble();

            if (waterValue != 0) {
                WaterLog waterLog = new WaterLog();
                waterLog.setOrigin(TokenType.FITBIT);
                waterLog.setAmount(waterValue);
                waterLog.setDate(DateConverter.stringToLocalDate(record.get("dateTime").asText()));
                waterLog.setUser(user);

                waterLogList.add(waterLog);
            }
        }

        waterLogDao.removeAllByDateBetweenAndUserAndOrigin(startDate, endDate, user, TokenType.FITBIT);
        waterLogDao.saveAll(waterLogList);
    }

    /**
     * create and persist weight logs from fitbit servers
     *
     * @param json      JsonNode object containing data
     * @param startDate start date
     * @param endDate   end date
     * @param user      user
     */
    @Transactional
    public void createAndPersistWeightLogs(JsonNode json, LocalDate startDate, LocalDate endDate, User user) {
        List<WeightLog> weightLogList = new ArrayList<>();

        for (JsonNode record : json.get("weight")) {
            WeightLog weightLog = new WeightLog();
            weightLog.setBmi(record.get("bmi").asDouble());
            weightLog.setBodyFatPercentage(0.0);
            weightLog.setWeightInKg(record.get("weight").asDouble());
            weightLog.setDate(DateConverter.stringToLocalDate(record.get("date").asText()));
            weightLog.setTime(DateConverter.stringToLocalTime(record.get("time").asText()));

            weightLog.setUser(user);
            weightLog.setOrigin(TokenType.FITBIT);
            weightLogList.add(weightLog);
        }

        weightLogDao.removeAllByDateBetweenAndUserAndOrigin(startDate, endDate, user, TokenType.FITBIT);
        weightLogDao.saveAll(weightLogList);
    }

    /**
     * create and persist activity logs
     *
     * @param json      JsonNode object containing data
     * @param startDate start date
     * @param endDate   end date
     * @param user      user
     */
    @Transactional
    public void createAndPersistActivityLogs(JsonNode json, LocalDate startDate, LocalDate endDate, User user) {
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy-MM-dd'T'HH:mm:ss[.SSS][XXXXX]");
        List<ActivityLog> activityLogList = new ArrayList<>();

        for (JsonNode record : json.get("activities")) {
            LocalDate date = LocalDate.parse(record.get("startTime").asText(), dtf);

            if (date.isAfter(endDate))
                break;

            String activityName = record.get("activityName").asText();
            ActivityLog activityLog = new ActivityLog();
            activityLog.setOrigin(TokenType.FITBIT);
            activityLog.setDurationInSeconds(record.get("duration").asInt() / 1000);
            activityLog.setName(activityName);
            activityLog.setDescription("");
            activityLog.setStartDateTime(LocalDateTime.parse(record.get("startTime").asText(), dtf));
            activityLog.setDate(LocalDate.parse(record.get("startTime").asText(), dtf));
            activityLog.setUser(user);

            JsonNode calories = record.get("calories");
            JsonNode distance = record.get("distance");
            JsonNode steps = record.get("steps");
            JsonNode speed = record.get("speed");
            JsonNode hrZones = record.get("heartRateZones");
            JsonNode activityLevel = record.get("activityLevel");

            if (calories != null) activityLog.setCaloriesOut(calories.asInt());
            if (distance != null) activityLog.setDistanceInMeters(distance.asDouble() * 1000);
            if (steps != null) activityLog.setSteps(steps.asInt());
            if (speed != null) activityLog.setSpeed(speed.asDouble());

            if (activityLevel != null && activityLevel.size() != 0) {
                ActivityMinutes activityMinutes = new ActivityMinutes();
                activityLevel.forEach((minute -> {
                    String levelName = minute.get("name").asText();
                    if (levelName.equalsIgnoreCase("fairly"))
                        activityMinutes.setFairly(minute.get("minutes").asInt());
                    else if (levelName.equalsIgnoreCase("lightly"))
                        activityMinutes.setLightly(minute.get("minutes").asInt());
                    else if (levelName.equalsIgnoreCase("sedentary"))
                        activityMinutes.setSedentary(minute.get("minutes").asInt());
                    else if (levelName.equalsIgnoreCase("very"))
                        activityMinutes.setVery(minute.get("minutes").asInt());
                }));
                activityLog.setActivityMinutes(activityMinutes);
            }

            List<HRZone> hrZoneList = new ArrayList<>();
            if (hrZones != null) {
                hrZones.forEach((zone) -> {
                    HRZone hrZone = new HRZone();
                    hrZone.setCaloriesOut(zone.get("caloriesOut").asInt());
                    hrZone.setMin(zone.get("min").asInt());
                    hrZone.setMax(zone.get("max").asInt());
                    hrZone.setMinutes(zone.get("minutes").asInt());

                    String zoneName = zone.get("name").asText();
                    if (zoneName.equalsIgnoreCase("Out of Range"))
                        hrZone.setHRZoneType(HRZoneType.OUT_OF_RANGE);
                    else if (zoneName.equalsIgnoreCase("Fat Burn"))
                        hrZone.setHRZoneType(HRZoneType.FAT_BURN);
                    else if (zoneName.equalsIgnoreCase("Cardio"))
                        hrZone.setHRZoneType(HRZoneType.CARDIO);
                    else if (zoneName.equalsIgnoreCase("Peak"))
                        hrZone.setHRZoneType(HRZoneType.PEAK);

                    hrZone.setActivityLog(activityLog);
                    hrZoneList.add(hrZone);
                });
            }

            if (!hrZoneList.isEmpty()) {
                activityLog.setHrZones(hrZoneList);
            }

            activityLogList.add(activityLog);
        }

        activityDao.removeAllByStartDateTimeBetweenAndOriginAndUser(DateConverter.stringToLocalDateTime(startDate + " 00:00:00", "yyyy-MM-dd HH:mm:ss"), DateConverter.stringToLocalDateTime(endDate + " 00:00:00", "yyyy-MM-dd HH:mm:ss"), TokenType.FITBIT, user);
        activityDao.saveAll(activityLogList);
    }

    /**
     * sync daily summary data from fitbit servers
     *
     * @param startDate start date
     * @param endDate   end date
     * @param user      user
     */
    @Transactional
    public void syncDailySummary(LocalDate startDate, LocalDate endDate, User user) {
        JsonNode caloriesJson = fetchData("https://api.fitbit.com/1/user/-/activities/calories/date/" + startDate.toString() + "/" + endDate.toString() + ".json", user).fields().next().getValue();
        JsonNode stepsJson = fetchData("https://api.fitbit.com/1/user/-/activities/steps/date/" + startDate.toString() + "/" + endDate.toString() + ".json", user).fields().next().getValue();
        JsonNode distanceJson = fetchData("https://api.fitbit.com/1/user/-/activities/distance/date/" + startDate.toString() + "/" + endDate.toString() + ".json", user).fields().next().getValue();
        JsonNode floorsJson = fetchData("https://api.fitbit.com/1/user/-/activities/floors/date/" + startDate.toString() + "/" + endDate.toString() + ".json", user).fields().next().getValue();
        JsonNode elevationJson = fetchData("https://api.fitbit.com/1/user/-/activities/elevation/date/" + startDate.toString() + "/" + endDate.toString() + ".json", user).fields().next().getValue();
        JsonNode activitiesCaloriesJson = fetchData("https://api.fitbit.com/1/user/-/activities/activityCalories/date/" + startDate.toString() + "/" + endDate.toString() + ".json", user).fields().next().getValue();

        List<DailySummary> dailySummaryList = new ArrayList<>();

        int i = 0;
        for (LocalDate date = startDate; date.isBefore(endDate); date = date.plusDays(1)) {
            int calories = caloriesJson.get(i).get("value").asInt();
            int caloriesActive = activitiesCaloriesJson.get(i).get("value").asInt();
            double distance = distanceJson.get(i).get("value").asDouble();
            double elevation = elevationJson.get(i).get("value").asDouble();
            double floors = floorsJson.get(i).get("value").asDouble();
            int steps = stepsJson.get(i).get("value").asInt();

            if (caloriesActive != 0 && distance != 0 && elevation != 0 && floors != 0 && steps != 0) {
                DailySummary dailySummary = new DailySummary();
                dailySummary.setDate(date);
                dailySummary.setCalories(calories);
                dailySummary.setCaloriesActivities(caloriesActive);
                dailySummary.setDistance(distance);
                dailySummary.setElevation(elevation);
                dailySummary.setFloors(floors);
                dailySummary.setSteps(steps);
                dailySummary.setUser(user);

                dailySummary.setOrigin(TokenType.FITBIT);
                dailySummaryList.add(dailySummary);
            }
            i++;
        }

        dailySummaryDao.removeAllByDateBetweenAndUserAndOrigin(startDate, endDate, user, TokenType.FITBIT);
        dailySummaryDao.saveAll(dailySummaryList);
    }

    /**
     * sync sleep data from fitbit servers
     *
     * @param startDate start date
     * @param endDate   end date
     * @param user      user
     */
    @Transactional
    public void syncSleepLogs(LocalDate startDate, LocalDate endDate, User user) {
        JsonNode json = fetchData("https://api.fitbit.com/1.2/user/-/sleep/date/" + startDate.toString() + "/" + endDate.toString() + ".json", user);


        if (json == null || json.fields().next() == null || json.fields().next().getValue() == null) return;

        createAndPersistSleepLogs(json, startDate, endDate, user);
    }

    /**
     * sync water data from fitbit servers
     *
     * @param startDate start date
     * @param endDate   end date
     * @param user      user
     */
    @Transactional
    public void syncWaterLogs(LocalDate startDate, LocalDate endDate, User user) {
        JsonNode json = fetchData("https://api.fitbit.com/1/user/-/foods/log/water/date/" + startDate + "/" + endDate + ".json", user);

        if (json == null || json.get("foods-log-water") == null) return;

        createAndPersistWaterLogs(json, startDate, endDate, user);
    }

    /**
     * sync weight data from fitbit servers
     *
     * @param startDate start date
     * @param endDate   end date
     * @param user      user
     */
    @Transactional
    public void syncWeightLogs(LocalDate startDate, LocalDate endDate, User user) {
        long daysBetween = ChronoUnit.DAYS.between(startDate, endDate);
        if (daysBetween > 31) endDate = startDate.plusDays(31);

        JsonNode json = fetchData("https://api.fitbit.com/1/user/-/body/log/weight/date/" + startDate.toString() + "/" + endDate.toString() + ".json", user);

        if (json == null || json.get("weight") == null) return;

        createAndPersistWeightLogs(json, startDate, endDate, user);
    }

    /**
     * sync activity logs from fitbit servers
     *
     * @param startDate start date
     * @param endDate   end date
     * @param user      user
     */
    @Transactional
    public void syncActivityLogs(LocalDate startDate, LocalDate endDate, User user) {
        String params = "?afterDate=" + startDate + "&limit=100&offset=0&sort=asc";
        JsonNode json = fetchData("https://api.fitbit.com/1/user/-/activities/list.json" + params, user);

        if (json == null || json.get("activities") == null) return;

        createAndPersistActivityLogs(json, startDate, endDate, user);
    }

    /**
     * sync data from fitbit servers
     *
     * @param startDate start date
     * @param endDate   end date
     * @param user      user
     */
    @Transactional
    public ResponseEntity<?> syncFromDateRange(User user, String startDate, String endDate) {
        AuthCredentials authCredentials = authTokenRepository.findByUserAndTokenType(user, TokenType.FITBIT);

        if (authCredentials == null)
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(new ResponseMessage(400, "Access to Fitbit data has not been granted yet!", ResponseMessageStatus.TOKEN_NOT_FOUND));

        LocalDate start = LocalDate.parse(startDate.trim(), DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        LocalDate end = LocalDate.parse(endDate.trim(), DateTimeFormatter.ofPattern("yyyy-MM-dd"));
        syncDailySummary(start, end, user);
        syncSleepLogs(start, end, user);
        syncWaterLogs(start, end, user);
        syncWeightLogs(start, end, user);
        syncActivityLogs(start, end, user);

        return ResponseEntity.status(HttpStatus.CREATED).body(new ResponseMessage(201, "Fitbit data have been synchronized", ResponseMessageStatus.DATA_SAVED));
    }
}