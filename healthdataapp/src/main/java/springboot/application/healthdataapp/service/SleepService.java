package springboot.application.healthdataapp.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import springboot.application.healthdataapp.dao.SleepLogDao;
import springboot.application.healthdataapp.helpers.ObjValidator;
import springboot.application.healthdataapp.model.type.ResponseMessageStatus;
import springboot.application.healthdataapp.model.user.User;
import springboot.application.healthdataapp.model.sleep.SleepLog;
import springboot.application.healthdataapp.model.type.TokenType;
import springboot.application.healthdataapp.response.ResponseMessage;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.List;

@Service
public class SleepService {
    @Autowired SleepLogDao sleepLogDao;

    /**
     * find sleep log from a date
     * @param user user
     * @param from data origin
     * @param date yyyy-MM-dd format
     * @return list of sleep logs from date
     */
    @Transactional
    public List<SleepLog> getSleepLog(User user, TokenType from, String date) {
        if (from == TokenType.ALL)
            return sleepLogDao.findAllByDateAndUserOrderByDateAsc(LocalDate.parse(date), user);
        return sleepLogDao.findAllByDateAndOriginAndUserOrderByDateAsc(LocalDate.parse(date), from, user);
    }

    /**
     * find all sleep logs by date range
     * @param user user
     * @param from data origin
     * @param startDate yyyy-MM-dd format
     * @param endDate yyyy-MM-dd format
     * @return list of sleep logs
     */
    @Transactional
    public List<SleepLog> getSleepLogByDateRange(User user, TokenType from, String startDate, String endDate) {
        if (from == TokenType.ALL)
            return sleepLogDao.findAllByDateBetweenAndUserOrderByDateAsc(LocalDate.parse(startDate), LocalDate.parse(endDate), user);
        return sleepLogDao.findAllByDateBetweenAndOriginAndUserOrderByDateAsc(LocalDate.parse(startDate), LocalDate.parse(endDate), from, user);
    }

    /**
     * @param sleepLog sleep log to add
     * @param user     user
     * @param date     date in format yyyy-MM-dd
     * @return response with status code and message
     */
    @Transactional
    public ResponseEntity<?> addSleepLog(SleepLog sleepLog, User user, String date) {
        sleepLog.setOrigin(TokenType.THIS);
        if (sleepLog.getDurationInSeconds() == null) {
            sleepLog.setEndDateTime(sleepLog.getStartDateTime());
        }
        else {
            sleepLog.setEndDateTime(sleepLog.getStartDateTime().plusSeconds(sleepLog.getDurationInSeconds()));
        }
        sleepLog.setUser(user);

        List<String> violations = ObjValidator.getConstraintViolations(sleepLog);
        if (violations.size() != 0)
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(violations);

        sleepLogDao.removeAllByDateAndOriginAndUser(sleepLog.getDate(), TokenType.THIS, user);
        sleepLogDao.save(sleepLog);
        return ResponseEntity.status(HttpStatus.CREATED).body(new ResponseMessage(201, "Sleep log from " + date + " has been created", ResponseMessageStatus.DATA_CREATED));
    }
}
