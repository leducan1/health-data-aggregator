package springboot.application.healthdataapp.service;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import springboot.application.healthdataapp.dao.BloodPressureDao;
import springboot.application.healthdataapp.helpers.DateConverter;
import springboot.application.healthdataapp.helpers.ObjValidator;
import springboot.application.healthdataapp.model.blood.BloodPressure;
import springboot.application.healthdataapp.model.type.ResponseMessageStatus;
import springboot.application.healthdataapp.model.type.TokenType;
import springboot.application.healthdataapp.model.user.User;
import springboot.application.healthdataapp.response.ResponseMessage;

import java.util.List;

@Service
public class BloodPressureService {
    private static final Logger LOG = LoggerFactory.getLogger(BloodPressure.class);
    @Autowired BloodPressureDao bloodPressureDao;

    /**
     * find all blood pressure logs ordered by date asc
     *
     * @param user user
     * @param from data origin
     * @param date yyyy-MM-dd format
     * @return list of blood pressure logs
     */
    public List<BloodPressure> getBloodPressureLogs(User user, TokenType from, String date) {
        if (from == TokenType.ALL)
            return bloodPressureDao.findAllByDateAndUserOrderByDateAsc(DateConverter.stringToLocalDate(date), user);
        return bloodPressureDao.findAllByDateAndOriginAndUserOrderByDateAsc(DateConverter.stringToLocalDate(date), from, user);
    }

    public List<BloodPressure> getBloodPressureLogsByDateRange(User user, TokenType from, String startDate, String endDate) {
        if (from == TokenType.ALL)
            return bloodPressureDao.findAllByDateBetweenAndUserOrderByDateAsc(DateConverter.stringToLocalDate(startDate), DateConverter.stringToLocalDate(endDate), user);
        return bloodPressureDao.findAllByDateBetweenAndOriginAndUserOrderByDateAsc(DateConverter.stringToLocalDate(startDate), DateConverter.stringToLocalDate(endDate), from, user);
    }

    /**
     * add blood pressure log
     *
     * @param log  blood pressure log
     * @param user user
     * @return response with status code and message
     */
    @Transactional
    public ResponseEntity<?> addBloodPressureLog(BloodPressure log, User user) {
        log.setOrigin(TokenType.THIS);
        log.setUser(user);

        List<String> violations = ObjValidator.getConstraintViolations(log);
        if (violations.size() != 0) {
            LOG.info("Blood pressure log has not been created due to constraint violation");
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(violations);
        }

        bloodPressureDao.removeAllByDateAndTimeAndOriginAndUser(log.getDate(), log.getTime(), TokenType.THIS, user);
        bloodPressureDao.save(log);
        LOG.info("Blood pressure from " + log.getDate().toString() + " has been created");
        return ResponseEntity.status(HttpStatus.CREATED).body(new ResponseMessage(201, "Blood pressure log from " + log.getDate() + " has been created", ResponseMessageStatus.DATA_CREATED));
    }
}
