package springboot.application.healthdataapp.service.sync;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import net.minidev.json.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import springboot.application.healthdataapp.service.authentication.JwtUtil;
import springboot.application.healthdataapp.dao.*;
import springboot.application.healthdataapp.model.type.TokenType;
import springboot.application.healthdataapp.model.user.AuthCredentials;
import springboot.application.healthdataapp.model.user.User;
import springboot.application.healthdataapp.dao.repository.SleepLogRepository;
import springboot.application.healthdataapp.request.AuthCodeRequest;
import springboot.application.healthdataapp.service.RequestSender;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

@Service
public abstract class BaseSyncService {
    @Autowired protected AuthCredentialsDao authTokenRepository;
    @Autowired protected JwtUtil jwtTokenUtil;
    @Autowired protected UserDao userDao;
    @Autowired protected ActivityDao activityDao;
    @Autowired protected SleepLogRepository sleepLogRepository;
    @Autowired protected AuthCredentialsDao authCredentialsDao;
    @Autowired protected WaterLogDao waterLogDao;
    @Autowired protected WeightLogDao weightLogDao;
    @Autowired protected DailySummaryDao dailySummaryDao;
    @Autowired protected RequestSender requestSender;
    @Autowired protected BloodPressureDao bloodPressureDao;

    /**
     * @return object with a URL to a consent screen
     */
    public abstract JSONObject getConsentScreenURL();

    /**
     * get access token
     *
     * @param user            user
     * @param authCodeRequest request with auth code
     * @return response with access token
     */
    public abstract ResponseEntity<?> getAccessToken(User user, AuthCodeRequest authCodeRequest);

    /**
     * refreshes access token
     *
     * @param user user
     * @return auth credentials object with a new access token and refresh token
     */
    public abstract AuthCredentials refreshAccessToken(User user);

    /**
     * convers a JSON string to JSONNode object
     *
     * @param json string
     * @return JSONNode object
     */
    protected JsonNode convertToJsonNode(String json) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.readTree(json);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * save token to the database
     *
     * @param user            user
     * @param authCredentials authorization credentials
     * @param tokenType       token type - data origin
     * @param scopes          scopes
     */
    protected void saveToken(User user, JsonNode authCredentials, TokenType tokenType, String scopes) {
        AuthCredentials authToken = new AuthCredentials();
        authToken.setAccessToken(authCredentials.get("access_token").textValue());
        authToken.setRefreshToken(authCredentials.get("refresh_token").textValue());
        authToken.setTokenType(tokenType);
        authToken.setUser(user);
        if (scopes != null) authToken.setScope(scopes);
        authTokenRepository.save(authToken);
    }

    /**
     * update token in the database
     *
     * @param token           token
     * @param newAccessToken  new access token
     * @param newRefreshToken new refresh token
     * @param scope           scope
     */
    protected void updateToken(AuthCredentials token, String newAccessToken, String newRefreshToken, String scope) {
        token.setAccessToken(newAccessToken);
        token.setRefreshToken(newRefreshToken);
        if (scope != null) token.setScope(scope);
        authTokenRepository.save(token);
    }
}