package springboot.application.healthdataapp.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import springboot.application.healthdataapp.dao.ActivityDao;
import springboot.application.healthdataapp.helpers.DateConverter;
import springboot.application.healthdataapp.helpers.ObjValidator;
import springboot.application.healthdataapp.model.activity.ActivityLog;
import springboot.application.healthdataapp.model.type.ResponseMessageStatus;
import springboot.application.healthdataapp.model.type.TokenType;
import springboot.application.healthdataapp.model.user.User;
import springboot.application.healthdataapp.response.ResponseMessage;

import java.util.List;

@Service
public class ActivityService {
    @Autowired
    private ActivityDao activityDao;

    /**
     * add activity log
     * @param activityLog activity log
     * @param user user
     * @param date yyyy-MM-dd format
     * @return response with status code and message
     */
    @Transactional
    public ResponseEntity<?> addActivityLog(ActivityLog activityLog, User user, String date) {
        activityLog.setUser(user);
        activityLog.setOrigin(TokenType.THIS);

        List<String> violations = ObjValidator.getConstraintViolations(activityLog);
        if (violations.size() != 0)
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).body(violations);

        activityDao.removeAllByStartDateTimeBetweenAndOriginAndUser(activityLog.getStartDateTime(), activityLog.getStartDateTime().plusSeconds(activityLog.getDurationInSeconds()), TokenType.THIS, user);
        activityDao.save(activityLog);
        return ResponseEntity.status(HttpStatus.CREATED).body(new ResponseMessage(201, "Activity log from " + date + " has been created", ResponseMessageStatus.DATA_CREATED));
    }

    /**
     * find all activity logs ordered by start date time asc
     * @param user user
     * @param origin data origin
     * @param date yyyy-MM-dd format
     * @return list of activity logs
     */
    @Transactional
    public List<ActivityLog> getActivityLogs(User user, TokenType origin, String date) {
        if (origin == TokenType.ALL)
            return activityDao.findAllByDateAndUserOrderByStartDateTimeAsc(DateConverter.stringToLocalDate(date, "yyyy-MM-dd"), user);
        return activityDao.findAllByDateAndOriginAndUserOrderByDateAsc(DateConverter.stringToLocalDate(date, "yyyy-MM-dd"), origin, user);
    }
}
