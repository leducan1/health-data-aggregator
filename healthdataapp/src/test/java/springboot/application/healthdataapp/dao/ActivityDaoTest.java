package springboot.application.healthdataapp.dao;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import springboot.application.healthdataapp.Generator;
import springboot.application.healthdataapp.helpers.DateConverter;
import springboot.application.healthdataapp.model.activity.ActivityLog;
import springboot.application.healthdataapp.model.type.Role;
import springboot.application.healthdataapp.model.type.TokenType;
import springboot.application.healthdataapp.model.user.User;
import springboot.application.healthdataapp.dao.repository.ActivityLogRepository;
import springboot.application.healthdataapp.dao.repository.UserRepository;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


@SpringBootTest
@ActiveProfiles("test")
@RunWith(SpringRunner.class)
public class ActivityDaoTest {
    @Autowired
    private ActivityLogRepository repo;

    @Autowired
    private UserRepository userRepo;

    private User user;

    @Before
    public void init() {
        User user = new User();
        user.setEmail(Generator.generateEmail());
        user.setPassword("123456789");
        user.setRole(Role.ROLE_USER);
        userRepo.save(user);
        this.user = user;
    }

    @Test
    public void whenFindingAllFromEmptyDatabase_thenReturnEmptyList() {
        List<ActivityLog> all = repo.findAll();
        assertTrue(all.isEmpty());
    }

    @Test
    public void whenAddingToDatabase_thenSizeOfListOfAllIsGreaterByOneThanBefore() {
        List<ActivityLog> initialAll = repo.findAll();
        ActivityLog log = new ActivityLog();
        log.setOrigin(TokenType.THIS);
        log.setDurationInSeconds(5000);
        log.setDate(Generator.generateLocalDate());
        log.setStartDateTime(Generator.generateLocalDateTime());
        log.setUser(user);
        repo.save(log);
        List<ActivityLog> allAfterSave = repo.findAll();
        assertEquals(initialAll.size() + 1, allAfterSave.size());
    }

    @Test
    public void whenDeletingAll_thenNoRecords() {
        ActivityLog log = new ActivityLog();
        log.setOrigin(TokenType.THIS);
        log.setDurationInSeconds(180);
        log.setDate(Generator.generateLocalDate());
        log.setStartDateTime(Generator.generateLocalDateTime());
        log.setUser(user);
        repo.save(log);
        repo.deleteAll();

        List<ActivityLog> all = repo.findAll();
        assertEquals(0, all.size());
    }

    @Test
    public void whenAdding1000Records_thenReturn1000Records() {
        List<ActivityLog> bpToPersist = new ArrayList<>();
        for (int i = 0; i < 1000; i++) {
            ActivityLog log = new ActivityLog();
            LocalDate date = Generator.generateLocalDate();
            log.setOrigin(TokenType.THIS);
            log.setDurationInSeconds(Generator.getRandomInt(0,50000));
            log.setDate(Generator.generateLocalDate());
            log.setSteps(Generator.getRandomInt(1,20000));
            log.setName(Generator.generateString());
            log.setStartDateTime(DateConverter.stringToLocalDateTime(date.toString() + " 00:00:00", "yyyy-MM-dd HH:mm:ss"));
            log.setUser(user);
            bpToPersist.add(log);
            date = date.plusDays(1);
        }
        repo.saveAll(bpToPersist);
        List<ActivityLog> bpFromDb = repo.findAll();

        assertEquals(1000, bpFromDb.size());
        assertEquals(bpFromDb.size(), bpToPersist.size());
    }

    @After
    public void teardown() {
        repo.deleteAll();
    }
}
