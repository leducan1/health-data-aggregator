package springboot.application.healthdataapp.dao;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import springboot.application.healthdataapp.Generator;
import springboot.application.healthdataapp.model.blood.BloodPressure;
import springboot.application.healthdataapp.model.type.Role;
import springboot.application.healthdataapp.model.type.TokenType;
import springboot.application.healthdataapp.model.user.User;
import springboot.application.healthdataapp.dao.repository.BloodPressureRepository;
import springboot.application.healthdataapp.dao.repository.UserRepository;

import java.time.LocalTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.*;


@SpringBootTest
@ActiveProfiles("test")
@RunWith(SpringRunner.class)
public class BloodPressureDaoTest {
    @Autowired
    private BloodPressureRepository repo;

    @Autowired
    private UserRepository userRepo;

    private User user;

    @Before
    public void init() {
        User user = new User();
        user.setEmail(Generator.generateEmail());
        user.setPassword("123456789");
        user.setRole(Role.ROLE_USER);
        userRepo.save(user);
        this.user = user;
    }

    @Test
    public void whenFindingAllFromEmptyDatabase_thenReturnEmptyList() {
        List<BloodPressure> all = repo.findAll();
        assertTrue(all.isEmpty());
    }

    @Test
    public void whenAddingBloodPressureToDatabase_thenSizeOfListOfAllBloodPressureIsGreaterByOneThanBefore() {
        List<BloodPressure> initialAll = repo.findAll();
        BloodPressure bp = new BloodPressure();
        bp.setOrigin(TokenType.THIS);
        bp.setSystolic(180);
        bp.setDiastolic(50);
        bp.setDate(Generator.generateLocalDate());
        bp.setTime(Generator.getRandomLocaTime());
        bp.setUser(user);
        repo.save(bp);
        List<BloodPressure> allAfterSave = repo.findAll();
        assertEquals(initialAll.size() + 1, allAfterSave.size());
    }

    @Test
    public void whenDeletingAll_thenNoBloodPressureRecords() {
        BloodPressure bp = new BloodPressure();
        bp.setOrigin(TokenType.THIS);
        bp.setSystolic(180);
        bp.setDiastolic(50);
        bp.setDate(Generator.generateLocalDate());
        bp.setTime(Generator.getRandomLocaTime());
        bp.setUser(user);
        repo.save(bp);
        repo.deleteAll();

        List<BloodPressure> all = repo.findAll();
        assertEquals(0, all.size());
    }

    @Test
    public void whenAdding1000Records_thenReturn1000Records() {
        List<BloodPressure> bpToPersist = new ArrayList<>();
        for (int i = 0; i < 1000; i++) {
            BloodPressure bp = new BloodPressure();
            LocalTime time = LocalTime.parse("00:00:00");
            bp.setOrigin(TokenType.THIS);
            bp.setSystolic(180);
            bp.setDiastolic(50);
            bp.setDate(Generator.generateLocalDate());
            bp.setTime(time);
            bp.setUser(user);
            bpToPersist.add(bp);
            time = time.plusSeconds(1);
        }
        repo.saveAll(bpToPersist);
        List<BloodPressure> bpFromDb = repo.findAll();

        assertEquals(1000, bpFromDb.size());
        assertEquals(bpFromDb.size(), bpToPersist.size());
    }

    @Test
//    public void whenAddingBloodPressureWithExistingDateAndTime_thenReplaceTheOldRecordWithNewOne() {
//    public void whenAddingBloodPressureWithExistingDateAndTime_thenReplaceTheOldRecordWithNewOne() {
//        BloodPressure bp = new BloodPressure();
//        LocalDate date = Generator.generateLocalDate();
//        LocalTime time = Generator.getRandomLocaTime();
//        bp.setOrigin(TokenType.THIS);
//        bp.setSystolic(180);
//        bp.setDiastolic(50);
//        bp.setDate(date);
//        bp.setTime(time);
//        bp.setUser(user);
//        bloodPressureRepository.save(bp);
//        List<BloodPressure> all = bloodPressureRepository.findAllByDateAndOriginAndUserOrderByDateAsc(date, TokenType.THIS, user);
//
//        BloodPressure bp1 = new BloodPressure();
//        LocalDate date1 = Generator.generateLocalDate();
//        LocalTime time1 = Generator.getRandomLocaTime();
//        bp.setOrigin(TokenType.THIS);
//        bp.setSystolic(120);
//        bp.setDiastolic(80);
//        bp.setDate(date);
//        bp.setTime(time);
//        bp.setUser(user);
//        bloodPressureRepository.save(bp);
//        List<BloodPressure> all1 = bloodPressureRepository.findAllByDateAndOriginAndUserOrderByDateAsc(date, TokenType.THIS, user);
//
//        assertNotEquals(all1, all);
//    }



    @After
    public void teardown() {
        repo.deleteAll();
    }
}
