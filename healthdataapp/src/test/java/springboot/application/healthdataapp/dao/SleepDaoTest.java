package springboot.application.healthdataapp.dao;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import springboot.application.healthdataapp.Generator;
import springboot.application.healthdataapp.model.sleep.SleepLog;
import springboot.application.healthdataapp.model.type.Role;
import springboot.application.healthdataapp.model.type.TokenType;
import springboot.application.healthdataapp.model.user.User;
import springboot.application.healthdataapp.dao.repository.SleepLogRepository;
import springboot.application.healthdataapp.dao.repository.UserRepository;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


@SpringBootTest
@ActiveProfiles("test")
@RunWith(SpringRunner.class)
public class SleepDaoTest {
    @Autowired
    private SleepLogRepository repo;

    @Autowired
    private UserRepository userRepo;

    private User user;

    @Before
    public void init() {
        User user = new User();
        user.setEmail(Generator.generateEmail());
        user.setPassword("123456789");
        user.setRole(Role.ROLE_USER);
        userRepo.save(user);
        this.user = user;
    }

    @Test
    public void whenFindingAllFromEmptyDatabase_thenReturnEmptyList() {
        List<SleepLog> all = repo.findAll();
        assertTrue(all.isEmpty());
    }

    @Test
    public void whenAddingToDatabase_thenSizeOfListOfAllIsGreaterByOneThanBefore() {
        List<SleepLog> initialAll = repo.findAll();
        SleepLog log = new SleepLog();
        LocalDateTime ldt = Generator.generateLocalDateTime();
        log.setOrigin(TokenType.THIS);
        log.setDate(ldt.toLocalDate());
        log.setStartDateTime(ldt);
        log.setEndDateTime(ldt.plusDays(1));
        log.setUser(user);
        repo.save(log);
        List<SleepLog> allAfterSave = repo.findAll();
        assertEquals(initialAll.size() + 1, allAfterSave.size());
    }

    @Test
    public void whenDeletingAll_thenNoRecords() {
        SleepLog log = new SleepLog();
        LocalDateTime ldt = Generator.generateLocalDateTime();
        log.setOrigin(TokenType.THIS);
        log.setDate(ldt.toLocalDate());
        log.setStartDateTime(ldt);
        log.setEndDateTime(ldt.plusMinutes(60));
        log.setUser(user);
        repo.save(log);
        repo.deleteAll();

        List<SleepLog> all = repo.findAll();
        assertEquals(0, all.size());
    }

    @Test
    public void whenAdding1000Records_thenReturn1000Records() {
        List<SleepLog> toPersist = new ArrayList<>();
        for (int i = 0; i < 1000; i++) {
            SleepLog log = new SleepLog();
            LocalDate date = Generator.generateLocalDate();
            LocalDateTime ldt = Generator.generateLocalDateTime();
            log.setOrigin(TokenType.THIS);
            log.setDate(ldt.toLocalDate());
            log.setStartDateTime(ldt);
            log.setEndDateTime(ldt.plusMinutes(Generator.getRandomInt(60, 420)));
            log.setUser(user);
            toPersist.add(log);
            date = date.plusDays(1);
        }
        repo.saveAll(toPersist);
        List<SleepLog> fromDb = repo.findAll();

        assertEquals(1000, fromDb.size());
        assertEquals(fromDb.size(), toPersist.size());
    }
    
    @After
    public void teardown() {
        repo.deleteAll();
    }
}
