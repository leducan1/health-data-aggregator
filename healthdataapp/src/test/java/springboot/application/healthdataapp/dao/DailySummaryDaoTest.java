package springboot.application.healthdataapp.dao;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import springboot.application.healthdataapp.Generator;
import springboot.application.healthdataapp.model.dailysummary.DailySummary;
import springboot.application.healthdataapp.model.type.Role;
import springboot.application.healthdataapp.model.type.TokenType;
import springboot.application.healthdataapp.model.user.User;
import springboot.application.healthdataapp.dao.repository.DailySummaryRepository;
import springboot.application.healthdataapp.dao.repository.UserRepository;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


@SpringBootTest
@ActiveProfiles("test")
@RunWith(SpringRunner.class)
public class DailySummaryDaoTest {
    @Autowired
    private DailySummaryRepository repo;

    @Autowired
    private UserRepository userRepo;

    private User user;

    @Before
    public void init() {
        User user = new User();
        user.setEmail(Generator.generateEmail());
        user.setPassword("123456789");
        user.setRole(Role.ROLE_USER);
        userRepo.save(user);
        this.user = user;
    }

    @Test
    public void whenFindingAllFromEmptyDatabase_thenReturnEmptyList() {
        List<DailySummary> all = repo.findAll();
        assertTrue(all.isEmpty());
    }

    @Test
    public void whenAddingToDatabase_thenSizeOfListOfAllIsGreaterByOneThanBefore() {
        List<DailySummary> initialAll = repo.findAll();
        DailySummary log = new DailySummary();
        log.setOrigin(TokenType.THIS);
        log.setSteps(5000);
        log.setDate(Generator.generateLocalDate());
        log.setDistance((double) 50000);
        log.setCalories(500);
        log.setUser(user);
        repo.save(log);
        List<DailySummary> allAfterSave = repo.findAll();
        assertEquals(initialAll.size() + 1, allAfterSave.size());
    }

    @Test
    public void whenDeletingAll_thenNoRecords() {
        DailySummary log = new DailySummary();
        log.setOrigin(TokenType.THIS);
        log.setSteps(5000);
        log.setDate(Generator.generateLocalDate());
        log.setDistance((double) 50000);
        log.setCalories(500);
        log.setUser(user);
        repo.save(log);
        repo.deleteAll();

        List<DailySummary> all = repo.findAll();
        assertEquals(0, all.size());
    }

    @Test
    public void whenAdding1000Records_thenReturn1000Records() {
        List<DailySummary> bpToPersist = new ArrayList<>();
        for (int i = 0; i < 1000; i++) {
            DailySummary log = new DailySummary();
            LocalDate date = Generator.generateLocalDate();
            log.setOrigin(TokenType.THIS);
            log.setDate(Generator.generateLocalDate());
            log.setUser(user);
            log.setSteps(Generator.getRandomInt(0,90000));
            log.setCalories(Generator.getRandomInt(0,1000));
            log.setCaloriesActivities(Generator.getRandomInt(0,500));
            log.setDistance((double) Generator.getRandomInt(0,5000));
            bpToPersist.add(log);
            date = date.plusDays(1);
        }
        repo.saveAll(bpToPersist);
        List<DailySummary> bpFromDb = repo.findAll();

        assertEquals(1000, bpFromDb.size());
        assertEquals(bpFromDb.size(), bpToPersist.size());
    }
    
    @After
    public void teardown() {
        repo.deleteAll();
    }
}
