package springboot.application.healthdataapp.service;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import springboot.application.healthdataapp.Generator;
import springboot.application.healthdataapp.model.blood.BloodPressure;
import springboot.application.healthdataapp.model.type.Role;
import springboot.application.healthdataapp.model.type.TokenType;
import springboot.application.healthdataapp.model.user.User;
import springboot.application.healthdataapp.dao.repository.BloodPressureRepository;
import springboot.application.healthdataapp.dao.repository.UserRepository;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;


@SpringBootTest
@ActiveProfiles("test")
@RunWith(SpringRunner.class)
public class BloodPressureServiceTest {
    @Autowired BloodPressureService service;

    @Autowired BloodPressureRepository repo;

    @Autowired
    private UserRepository userRepo;

    private User user;

    @Before
    public void init() {
        User user = new User();
        user.setEmail(Generator.generateEmail());
        user.setPassword("123456789");
        user.setRole(Role.ROLE_USER);
        userRepo.save(user);
        this.user = user;
    }

    @Test
    public void whenAddLogWithNegativeValue_ThenNotSaveIntoDatabase() {
        List<BloodPressure> all = repo.findAll();
        BloodPressure log = new BloodPressure();
        log.setOrigin(TokenType.THIS);
        log.setSystolic(-180);
        log.setDiastolic(-50);
        log.setDate(Generator.generateLocalDate());
        log.setTime(Generator.getRandomLocaTime());
        log.setUser(user);
        service.addBloodPressureLog(log, user);
        List<BloodPressure> allAfter = repo.findAll();

        assertEquals(all.size(), allAfter.size());
    }

    @Test
    public void whenAddLogWithSameDateAndTimeTwice_ThenDatabaseWillOnlyHaveOne() {
        BloodPressure log = new BloodPressure();
        LocalDateTime date = Generator.generateLocalDateTime();
        log.setOrigin(TokenType.THIS);
        log.setSystolic(180);
        log.setDiastolic(50);
        log.setDate(date.toLocalDate());
        log.setTime(date.toLocalTime());
        log.setUser(user);
        service.addBloodPressureLog(log, user);
        List<BloodPressure> all = service.getBloodPressureLogsByDateRange(user, TokenType.THIS, date.toLocalDate().toString(), date.toLocalDate().toString());

        BloodPressure log1 = new BloodPressure();
        log1.setOrigin(TokenType.THIS);
        log1.setSystolic(180);
        log1.setDiastolic(50);
        log1.setDate(date.toLocalDate());
        log1.setTime(date.toLocalTime());
        log1.setUser(user);
        service.addBloodPressureLog(log, user);
        List<BloodPressure> allAfter = service.getBloodPressureLogs(user, TokenType.THIS, date.toLocalDate().toString());

        assertEquals(1, all.size());
        assertEquals(1, allAfter.size());
    }

    @After
    public void teardown() {
        repo.deleteAll();
    }
}
