package springboot.application.healthdataapp.service;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import springboot.application.healthdataapp.Generator;
import springboot.application.healthdataapp.model.type.Role;
import springboot.application.healthdataapp.model.type.TokenType;
import springboot.application.healthdataapp.model.user.User;
import springboot.application.healthdataapp.model.water.WaterLog;
import springboot.application.healthdataapp.dao.repository.WaterLogRepository;
import springboot.application.healthdataapp.dao.repository.UserRepository;

import java.time.LocalDateTime;
import java.util.List;

import static org.junit.Assert.assertEquals;


@SpringBootTest
@ActiveProfiles("test")
@RunWith(SpringRunner.class)
public class WaterServiceTest {
    @Autowired WaterService service;

    @Autowired WaterLogRepository repo;

    @Autowired
    private UserRepository userRepo;

    private User user;

    @Before
    public void init() {
        User user = new User();
        user.setEmail(Generator.generateEmail());
        user.setPassword("123456789");
        user.setRole(Role.ROLE_USER);
        userRepo.save(user);
        this.user = user;
    }

    @Test
    public void whenAddLogWithNegativeAmount_ThenNotSaveIntoDatabase() {
        WaterLog log = new WaterLog();
        LocalDateTime ldt = Generator.generateLocalDateTime();
        List<WaterLog> all = service.getWaterLogsByDateRange(user, TokenType.THIS, ldt.toLocalDate().toString(), ldt.toLocalDate().toString());
        log.setOrigin(TokenType.THIS);
        log.setDate(ldt.toLocalDate());
        log.setAmount(-500.5);
        log.setUser(user);
        service.addWaterLog(log, user, ldt.toLocalDate().toString());
        List<WaterLog> allAfter = service.getDailyWaterLog(user, TokenType.THIS, ldt.toLocalDate().toString());

        assertEquals(all.size(), allAfter.size());
    }

    @Test
    public void whenAddMultipleLogsFromSameDate_ThenDatabaseSizeIsSame() {
        WaterLog log = new WaterLog();
        LocalDateTime ldt = Generator.generateLocalDateTime();
        log.setOrigin(TokenType.THIS);
        log.setDate(ldt.toLocalDate());
        log.setAmount(1200.0);
        log.setUser(user);
        service.addWaterLog(log, user, ldt.toLocalDate().toString());
        List<WaterLog> all = repo.findAll();

        WaterLog log1 = new WaterLog();
        log1.setOrigin(TokenType.THIS);
        log1.setDate(ldt.toLocalDate());
        log1.setAmount(1200.0);
        log1.setUser(user);
        service.addWaterLog(log1, user, ldt.toLocalDate().toString());
        List<WaterLog> allAfter = repo.findAll();

        assertEquals(all.size(), allAfter.size());
    }

    @After
    public void teardown() {
        repo.deleteAll();
    }
}
