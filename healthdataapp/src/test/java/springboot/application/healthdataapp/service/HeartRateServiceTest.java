package springboot.application.healthdataapp.service;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import springboot.application.healthdataapp.Generator;
import springboot.application.healthdataapp.model.heart.HeartRate;
import springboot.application.healthdataapp.model.type.Role;
import springboot.application.healthdataapp.model.type.TokenType;
import springboot.application.healthdataapp.model.user.User;
import springboot.application.healthdataapp.dao.repository.HeartRateRepository;
import springboot.application.healthdataapp.dao.repository.UserRepository;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import static org.junit.Assert.assertEquals;


@SpringBootTest
@ActiveProfiles("test")
@RunWith(SpringRunner.class)
public class HeartRateServiceTest {
    @Autowired HeartRateService service;

    @Autowired HeartRateRepository repo;

    @Autowired
    private UserRepository userRepo;

    private User user;

    @Before
    public void init() {
        User user = new User();
        user.setEmail(Generator.generateEmail());
        user.setPassword("123456789");
        user.setRole(Role.ROLE_USER);
        userRepo.save(user);
        this.user = user;
    }

    @Test
    public void whenAddLogWithNoStartAndEndtime_ThenNotSaveIntoDatabase() {
        HeartRate log = new HeartRate();
        LocalDateTime ldt = Generator.generateLocalDateTime();
        List<HeartRate> all = service.getHeartRateLogsByDateRange(user, TokenType.THIS, ldt.toLocalDate().toString(), ldt.toLocalDate().toString());
        log.setOrigin(TokenType.THIS);
        log.setDate(ldt.toLocalDate());
        log.setValue(Generator.getRandomInt(1,130));
        log.setUser(user);
        service.addHeartRateLog(log, user);
        List<HeartRate> allAfter = service.getHeartRateLogs(user, TokenType.THIS, ldt.toLocalDate().toString());

        assertEquals(all.size(), allAfter.size());
    }

    @Test
    public void whenAddLogsWithDifferentData_ThenSaveEveryLog() {
        HeartRate log = new HeartRate();
        LocalDateTime ldt = Generator.generateLocalDateTime();
        log.setOrigin(TokenType.THIS);
        log.setDate(ldt.toLocalDate());
        log.setStartDateTime(ldt);
        log.setEndDateTime(ldt);
        log.setValue(Generator.getRandomInt(1,130));
        log.setUser(user);
        service.addHeartRateLog(log, user);
        List<HeartRate> all = repo.findAll();

        HeartRate log1 = new HeartRate();
        LocalDate date1 = Generator.generateLocalDate();
        LocalDateTime ldt1 = Generator.generateLocalDateTime();
        log1.setOrigin(TokenType.THIS);
        log1.setDate(ldt1.toLocalDate());
        log1.setStartDateTime(ldt1);
        log1.setEndDateTime(ldt1);
        log1.setValue(Generator.getRandomInt(1,130));
        log1.setUser(user);
        service.addHeartRateLog(log, user);
        List<HeartRate> allAfter = repo.findAll();

        assertEquals(1, all.size());
        assertEquals(1, allAfter.size());
    }

    @After
    public void teardown() {
        repo.deleteAll();
    }
}
