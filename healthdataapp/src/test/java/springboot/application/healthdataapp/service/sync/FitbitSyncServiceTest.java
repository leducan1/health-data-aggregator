package springboot.application.healthdataapp.service.sync;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import springboot.application.healthdataapp.Generator;
import springboot.application.healthdataapp.dao.repository.*;
import springboot.application.healthdataapp.model.activity.ActivityLog;
import springboot.application.healthdataapp.model.blood.BloodPressure;
import springboot.application.healthdataapp.model.sleep.SleepLog;
import springboot.application.healthdataapp.model.type.Role;
import springboot.application.healthdataapp.model.user.User;
import springboot.application.healthdataapp.model.water.WaterLog;
import springboot.application.healthdataapp.model.weight.WeightLog;

import java.time.LocalDate;
import java.util.List;

@SpringBootTest
@ActiveProfiles("test")
@RunWith(SpringRunner.class)
public class FitbitSyncServiceTest {
    @Autowired
    private UserRepository userRepo;

    @Autowired
    private FitbitSyncService fitbitSyncService;

    @Autowired
    private ActivityLogRepository activityLogRepository;

    @Autowired
    private DailySummaryRepository dailySummaryRepository;

    @Autowired
    private WeightLogRepository weightLogRepository;

    @Autowired
    private SleepLogRepository sleepLogRepository;

    @Autowired
    private HeartRateRepository heartRateRepository;

    @Autowired
    private WaterLogRepository waterLogRepository;

    @Autowired
    private BloodPressureRepository bloodPressureRepository;

    private User user;

    @Before
    public void init() {
        User user = new User();
        user.setEmail(Generator.generateEmail());
        user.setPassword("123456789");
        user.setRole(Role.ROLE_USER);
        userRepo.save(user);
        this.user = user;
    }

    protected JsonNode convertToJsonNode(String json) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            return mapper.readTree(json);
        } catch (JsonProcessingException e) {
            e.printStackTrace();
            return null;
        }
    }

    @Test
    public void whenCreateAndPersistSleepLogsWithValidData_ThenSaveInDatabase() {
        List<SleepLog> sleepListBefore = sleepLogRepository.findAll();

        JsonNode json = convertToJsonNode("{\"sleep\":[{\"dateOfSleep\":\"2021-02-25\",\"duration\":32700000,\"efficiency\":91,\"endTime\":\"2021-02-25T07:33:17.000\",\"infoCode\":0,\"isMainSleep\":true,\"levels\":{\"data\":[{\"dateTime\":\"2021-02-24T22:28:00.000\",\"level\":\"awake\",\"seconds\":420},{\"dateTime\":\"2021-02-24T22:35:00.000\",\"level\":\"restless\",\"seconds\":60},{\"dateTime\":\"2021-02-24T22:36:00.000\",\"level\":\"awake\",\"seconds\":840},{\"dateTime\":\"2021-02-24T22:50:00.000\",\"level\":\"restless\",\"seconds\":60},{\"dateTime\":\"2021-02-24T22:51:00.000\",\"level\":\"awake\",\"seconds\":60},{\"dateTime\":\"2021-02-24T22:52:00.000\",\"level\":\"restless\",\"seconds\":120},{\"dateTime\":\"2021-02-24T22:54:00.000\",\"level\":\"asleep\",\"seconds\":3960},{\"dateTime\":\"2021-02-25T00:00:00.000\",\"level\":\"restless\",\"seconds\":120},{\"dateTime\":\"2021-02-25T00:02:00.000\",\"level\":\"asleep\",\"seconds\":3480},{\"dateTime\":\"2021-02-25T01:00:00.000\",\"level\":\"restless\",\"seconds\":180},{\"dateTime\":\"2021-02-25T01:03:00.000\",\"level\":\"asleep\",\"seconds\":60},{\"dateTime\":\"2021-02-25T01:04:00.000\",\"level\":\"restless\",\"seconds\":60},{\"dateTime\":\"2021-02-25T01:05:00.000\",\"level\":\"asleep\",\"seconds\":60},{\"dateTime\":\"2021-02-25T01:06:00.000\",\"level\":\"restless\",\"seconds\":60},{\"dateTime\":\"2021-02-25T01:07:00.000\",\"level\":\"asleep\",\"seconds\":960},{\"dateTime\":\"2021-02-25T01:23:00.000\",\"level\":\"restless\",\"seconds\":60},{\"dateTime\":\"2021-02-25T01:24:00.000\",\"level\":\"asleep\",\"seconds\":1860},{\"dateTime\":\"2021-02-25T01:55:00.000\",\"level\":\"restless\",\"seconds\":180},{\"dateTime\":\"2021-02-25T01:58:00.000\",\"level\":\"asleep\",\"seconds\":360},{\"dateTime\":\"2021-02-25T02:04:00.000\",\"level\":\"restless\",\"seconds\":60},{\"dateTime\":\"2021-02-25T02:05:00.000\",\"level\":\"asleep\",\"seconds\":540},{\"dateTime\":\"2021-02-25T02:14:00.000\",\"level\":\"restless\",\"seconds\":120},{\"dateTime\":\"2021-02-25T02:16:00.000\",\"level\":\"asleep\",\"seconds\":960},{\"dateTime\":\"2021-02-25T02:32:00.000\",\"level\":\"restless\",\"seconds\":60},{\"dateTime\":\"2021-02-25T02:33:00.000\",\"level\":\"asleep\",\"seconds\":60},{\"dateTime\":\"2021-02-25T02:34:00.000\",\"level\":\"restless\",\"seconds\":60},{\"dateTime\":\"2021-02-25T02:35:00.000\",\"level\":\"asleep\",\"seconds\":60},{\"dateTime\":\"2021-02-25T02:36:00.000\",\"level\":\"restless\",\"seconds\":120},{\"dateTime\":\"2021-02-25T02:38:00.000\",\"level\":\"asleep\",\"seconds\":1440},{\"dateTime\":\"2021-02-25T03:02:00.000\",\"level\":\"restless\",\"seconds\":240},{\"dateTime\":\"2021-02-25T03:06:00.000\",\"level\":\"asleep\",\"seconds\":5280},{\"dateTime\":\"2021-02-25T04:34:00.000\",\"level\":\"restless\",\"seconds\":60},{\"dateTime\":\"2021-02-25T04:35:00.000\",\"level\":\"asleep\",\"seconds\":180},{\"dateTime\":\"2021-02-25T04:38:00.000\",\"level\":\"restless\",\"seconds\":60},{\"dateTime\":\"2021-02-25T04:39:00.000\",\"level\":\"asleep\",\"seconds\":2280},{\"dateTime\":\"2021-02-25T05:17:00.000\",\"level\":\"restless\",\"seconds\":60},{\"dateTime\":\"2021-02-25T05:18:00.000\",\"level\":\"asleep\",\"seconds\":2040},{\"dateTime\":\"2021-02-25T05:52:00.000\",\"level\":\"restless\",\"seconds\":120},{\"dateTime\":\"2021-02-25T05:54:00.000\",\"level\":\"asleep\",\"seconds\":180},{\"dateTime\":\"2021-02-25T05:57:00.000\",\"level\":\"restless\",\"seconds\":60},{\"dateTime\":\"2021-02-25T05:58:00.000\",\"level\":\"asleep\",\"seconds\":120},{\"dateTime\":\"2021-02-25T06:00:00.000\",\"level\":\"restless\",\"seconds\":240},{\"dateTime\":\"2021-02-25T06:04:00.000\",\"level\":\"awake\",\"seconds\":60},{\"dateTime\":\"2021-02-25T06:05:00.000\",\"level\":\"restless\",\"seconds\":60},{\"dateTime\":\"2021-02-25T06:06:00.000\",\"level\":\"asleep\",\"seconds\":540},{\"dateTime\":\"2021-02-25T06:15:00.000\",\"level\":\"restless\",\"seconds\":180},{\"dateTime\":\"2021-02-25T06:18:00.000\",\"level\":\"asleep\",\"seconds\":180},{\"dateTime\":\"2021-02-25T06:21:00.000\",\"level\":\"restless\",\"seconds\":360},{\"dateTime\":\"2021-02-25T06:27:00.000\",\"level\":\"awake\",\"seconds\":60},{\"dateTime\":\"2021-02-25T06:28:00.000\",\"level\":\"restless\",\"seconds\":120},{\"dateTime\":\"2021-02-25T06:30:00.000\",\"level\":\"asleep\",\"seconds\":3780}],\"summary\":{\"asleep\":{\"count\":0,\"minutes\":473},\"awake\":{\"count\":5,\"minutes\":24},\"restless\":{\"count\":24,\"minutes\":46}}},\"logId\":31072016331,\"minutesAfterWakeup\":0,\"minutesAsleep\":473,\"minutesAwake\":46,\"minutesToFallAsleep\":26,\"startTime\":\"2021-02-24T22:28:00.000\",\"timeInBed\":545,\"type\":\"classic\"},{\"dateOfSleep\":\"2021-02-20\",\"duration\":33600000,\"efficiency\":97,\"endTime\":\"2021-02-20T09:09:22.000\",\"infoCode\":0,\"isMainSleep\":true,\"levels\":{\"data\":[{\"dateTime\":\"2021-02-19T23:49:00.000\",\"level\":\"restless\",\"seconds\":60},{\"dateTime\":\"2021-02-19T23:50:00.000\",\"level\":\"awake\",\"seconds\":420},{\"dateTime\":\"2021-02-19T23:57:00.000\",\"level\":\"restless\",\"seconds\":120},{\"dateTime\":\"2021-02-19T23:59:00.000\",\"level\":\"asleep\",\"seconds\":4440},{\"dateTime\":\"2021-02-20T01:13:00.000\",\"level\":\"restless\",\"seconds\":60},{\"dateTime\":\"2021-02-20T01:14:00.000\",\"level\":\"asleep\",\"seconds\":540},{\"dateTime\":\"2021-02-20T01:23:00.000\",\"level\":\"restless\",\"seconds\":180},{\"dateTime\":\"2021-02-20T01:26:00.000\",\"level\":\"asleep\",\"seconds\":18600},{\"dateTime\":\"2021-02-20T06:36:00.000\",\"level\":\"restless\",\"seconds\":60},{\"dateTime\":\"2021-02-20T06:37:00.000\",\"level\":\"asleep\",\"seconds\":960},{\"dateTime\":\"2021-02-20T06:53:00.000\",\"level\":\"restless\",\"seconds\":60},{\"dateTime\":\"2021-02-20T06:54:00.000\",\"level\":\"asleep\",\"seconds\":240},{\"dateTime\":\"2021-02-20T06:58:00.000\",\"level\":\"restless\",\"seconds\":240},{\"dateTime\":\"2021-02-20T07:02:00.000\",\"level\":\"asleep\",\"seconds\":60},{\"dateTime\":\"2021-02-20T07:03:00.000\",\"level\":\"restless\",\"seconds\":120},{\"dateTime\":\"2021-02-20T07:05:00.000\",\"level\":\"asleep\",\"seconds\":1980},{\"dateTime\":\"2021-02-20T07:38:00.000\",\"level\":\"restless\",\"seconds\":180},{\"dateTime\":\"2021-02-20T07:41:00.000\",\"level\":\"asleep\",\"seconds\":60},{\"dateTime\":\"2021-02-20T07:42:00.000\",\"level\":\"restless\",\"seconds\":180},{\"dateTime\":\"2021-02-20T07:45:00.000\",\"level\":\"asleep\",\"seconds\":3960},{\"dateTime\":\"2021-02-20T08:51:00.000\",\"level\":\"restless\",\"seconds\":60},{\"dateTime\":\"2021-02-20T08:52:00.000\",\"level\":\"asleep\",\"seconds\":1020}],\"summary\":{\"asleep\":{\"count\":0,\"minutes\":531},\"awake\":{\"count\":1,\"minutes\":7},\"restless\":{\"count\":11,\"minutes\":22}}},\"logId\":30984557106,\"minutesAfterWakeup\":0,\"minutesAsleep\":531,\"minutesAwake\":19,\"minutesToFallAsleep\":10,\"startTime\":\"2021-02-19T23:49:00.000\",\"timeInBed\":560,\"type\":\"classic\"}]}\n");

        fitbitSyncService.createAndPersistSleepLogs(json, LocalDate.parse("2021-02-18"), LocalDate.parse("2021-04-18"), user);

        List<SleepLog> sleepListAfter = sleepLogRepository.findAll();

        Assert.assertTrue(sleepListAfter.size() != sleepListBefore.size());
        sleepLogRepository.deleteAll();
    }

    @Test
    public void whenCreateAndPersistWeightWithValidData_ThenSaveInDatabase() {
        List<WeightLog> listBefore = weightLogRepository.findAll();

        JsonNode json = convertToJsonNode("{\"weight\":[{\"bmi\":19.61,\"date\":\"2021-02-21\",\"fat\":20,\"logId\":1613951999000,\"source\":\"API\",\"time\":\"23:59:59\",\"weight\":56},{\"bmi\":19.82,\"date\":\"2021-02-22\",\"fat\":20,\"logId\":1614038399000,\"source\":\"API\",\"time\":\"23:59:59\",\"weight\":56.6},{\"bmi\":19.82,\"date\":\"2021-02-26\",\"fat\":23,\"logId\":1614383999000,\"source\":\"API\",\"time\":\"23:59:59\",\"weight\":56.6},{\"bmi\":19.12,\"date\":\"2021-03-02\",\"fat\":23,\"logId\":1614729599000,\"source\":\"API\",\"time\":\"23:59:59\",\"weight\":54.6},{\"bmi\":19.92,\"date\":\"2021-03-03\",\"fat\":23,\"logId\":1614815999000,\"source\":\"API\",\"time\":\"23:59:59\",\"weight\":56.9},{\"bmi\":20.34,\"date\":\"2021-03-04\",\"fat\":21,\"logId\":1614902399000,\"source\":\"API\",\"time\":\"23:59:59\",\"weight\":58.1},{\"bmi\":18.73,\"date\":\"2021-03-05\",\"fat\":20,\"logId\":1614988799000,\"source\":\"API\",\"time\":\"23:59:59\",\"weight\":53.5}]}\n");
        fitbitSyncService.createAndPersistWeightLogs(json, LocalDate.parse("2021-02-18"), LocalDate.parse("2021-04-18"), user);

        List<WeightLog> listAfter = weightLogRepository.findAll();

        Assert.assertTrue(listAfter.size() != listBefore.size());
        weightLogRepository.deleteAll();
    }

    @Test
    public void whenCreateAndPersistWaterLogsWithValidData_ThenSaveInDatabase() {
        List<WaterLog> listBefore = waterLogRepository.findAll();

        JsonNode json = convertToJsonNode("{\"foods-log-water\":[{\"dateTime\":\"2021-02-18\",\"value\":\"0.0\"},{\"dateTime\":\"2021-02-19\",\"value\":\"0.0\"},{\"dateTime\":\"2021-02-20\",\"value\":\"0.0\"},{\"dateTime\":\"2021-02-21\",\"value\":\"709.7650146484375\"},{\"dateTime\":\"2021-02-22\",\"value\":\"0.0\"},{\"dateTime\":\"2021-02-23\",\"value\":\"0.0\"},{\"dateTime\":\"2021-02-24\",\"value\":\"0.0\"},{\"dateTime\":\"2021-02-25\",\"value\":\"0.0\"},{\"dateTime\":\"2021-02-26\",\"value\":\"0.0\"},{\"dateTime\":\"2021-02-27\",\"value\":\"473.1759948730469\"},{\"dateTime\":\"2021-02-28\",\"value\":\"0.0\"},{\"dateTime\":\"2021-03-01\",\"value\":\"0.0\"},{\"dateTime\":\"2021-03-02\",\"value\":\"3548.8251953125\"},{\"dateTime\":\"2021-03-03\",\"value\":\"2129.2900390625\"},{\"dateTime\":\"2021-03-04\",\"value\":\"7097.64990234375\"},{\"dateTime\":\"2021-03-05\",\"value\":\"3548.820068359375\"},{\"dateTime\":\"2021-03-06\",\"value\":\"0.0\"},{\"dateTime\":\"2021-03-07\",\"value\":\"0.0\"},{\"dateTime\":\"2021-03-08\",\"value\":\"0.0\"},{\"dateTime\":\"2021-03-09\",\"value\":\"0.0\"},{\"dateTime\":\"2021-03-10\",\"value\":\"0.0\"},{\"dateTime\":\"2021-03-11\",\"value\":\"0.0\"},{\"dateTime\":\"2021-03-12\",\"value\":\"0.0\"},{\"dateTime\":\"2021-03-13\",\"value\":\"0.0\"},{\"dateTime\":\"2021-03-14\",\"value\":\"0.0\"},{\"dateTime\":\"2021-03-15\",\"value\":\"0.0\"},{\"dateTime\":\"2021-03-16\",\"value\":\"0.0\"},{\"dateTime\":\"2021-03-17\",\"value\":\"0.0\"},{\"dateTime\":\"2021-03-18\",\"value\":\"0.0\"},{\"dateTime\":\"2021-03-19\",\"value\":\"0.0\"},{\"dateTime\":\"2021-03-20\",\"value\":\"0.0\"},{\"dateTime\":\"2021-03-21\",\"value\":\"0.0\"},{\"dateTime\":\"2021-03-22\",\"value\":\"0.0\"},{\"dateTime\":\"2021-03-23\",\"value\":\"0.0\"},{\"dateTime\":\"2021-03-24\",\"value\":\"0.0\"},{\"dateTime\":\"2021-03-25\",\"value\":\"0.0\"},{\"dateTime\":\"2021-03-26\",\"value\":\"0.0\"},{\"dateTime\":\"2021-03-27\",\"value\":\"0.0\"},{\"dateTime\":\"2021-03-28\",\"value\":\"0.0\"},{\"dateTime\":\"2021-03-29\",\"value\":\"592121.0\"},{\"dateTime\":\"2021-03-30\",\"value\":\"0.0\"},{\"dateTime\":\"2021-03-31\",\"value\":\"0.0\"},{\"dateTime\":\"2021-04-01\",\"value\":\"0.0\"},{\"dateTime\":\"2021-04-02\",\"value\":\"0.0\"},{\"dateTime\":\"2021-04-03\",\"value\":\"0.0\"},{\"dateTime\":\"2021-04-04\",\"value\":\"0.0\"},{\"dateTime\":\"2021-04-05\",\"value\":\"0.0\"},{\"dateTime\":\"2021-04-06\",\"value\":\"0.0\"},{\"dateTime\":\"2021-04-07\",\"value\":\"0.0\"},{\"dateTime\":\"2021-04-08\",\"value\":\"0.0\"},{\"dateTime\":\"2021-04-09\",\"value\":\"0.0\"},{\"dateTime\":\"2021-04-10\",\"value\":\"0.0\"},{\"dateTime\":\"2021-04-11\",\"value\":\"0.0\"},{\"dateTime\":\"2021-04-12\",\"value\":\"0.0\"},{\"dateTime\":\"2021-04-13\",\"value\":\"0.0\"},{\"dateTime\":\"2021-04-14\",\"value\":\"0.0\"},{\"dateTime\":\"2021-04-15\",\"value\":\"0.0\"},{\"dateTime\":\"2021-04-16\",\"value\":\"0.0\"},{\"dateTime\":\"2021-04-17\",\"value\":\"0.0\"},{\"dateTime\":\"2021-04-18\",\"value\":\"0.0\"}]}\n");
        fitbitSyncService.createAndPersistWaterLogs(json, LocalDate.parse("2021-02-18"), LocalDate.parse("2021-04-18"), user);

        List<WaterLog> listAfter = waterLogRepository.findAll();

        Assert.assertTrue(listAfter.size() != listBefore.size());
        waterLogRepository.deleteAll();
    }

    @Test
    public void whenCreateAndPersistActivitiesWithValidData_ThenSaveInDatabase() {
        List<ActivityLog> listBefore = activityLogRepository.findAll();

        JsonNode json = convertToJsonNode("{\"activities\":[{\"activeDuration\":4080000,\"activeZoneMinutes\":{\"minutesInHeartRateZones\":[{\"minuteMultiplier\":1,\"minutes\":0,\"order\":1,\"type\":\"FAT_BURN\",\"zoneName\":\"Fat Burn\"},{\"minuteMultiplier\":0,\"minutes\":0,\"order\":0,\"type\":\"OUT_OF_ZONE\",\"zoneName\":\"Out of Range\"},{\"minuteMultiplier\":2,\"minutes\":0,\"order\":2,\"type\":\"CARDIO\",\"zoneName\":\"Cardio\"},{\"minuteMultiplier\":2,\"minutes\":0,\"order\":3,\"type\":\"PEAK\",\"zoneName\":\"Peak\"}],\"totalMinutes\":0},\"activityLevel\":[{\"minutes\":0,\"name\":\"sedentary\"},{\"minutes\":0,\"name\":\"lightly\"},{\"minutes\":0,\"name\":\"fairly\"},{\"minutes\":68,\"name\":\"very\"}],\"activityName\":\"Soccer\",\"activityTypeId\":15605,\"calories\":721,\"duration\":4080000,\"hasActiveZoneMinutes\":true,\"lastModified\":\"2021-03-05T20:11:59.000Z\",\"logId\":38046444209,\"logType\":\"manual\",\"manualValuesSpecified\":{\"calories\":true,\"distance\":true,\"steps\":false},\"originalDuration\":4080000,\"originalStartTime\":\"2021-03-05T17:41:00.000+01:00\",\"source\":{\"id\":\"228VSR\",\"name\":\"Fitbit for Android\",\"trackerFeatures\":[],\"type\":\"app\",\"url\":\"https://www.fitbit.com/android\"},\"startTime\":\"2021-03-05T17:41:00.000+01:00\",\"steps\":0},{\"activeDuration\":922000,\"activeZoneMinutes\":{\"minutesInHeartRateZones\":[{\"minuteMultiplier\":1,\"minutes\":9,\"order\":1,\"type\":\"FAT_BURN\",\"zoneName\":\"Fat Burn\"},{\"minuteMultiplier\":0,\"minutes\":0,\"order\":0,\"type\":\"OUT_OF_ZONE\",\"zoneName\":\"Out of Range\"},{\"minuteMultiplier\":2,\"minutes\":0,\"order\":2,\"type\":\"CARDIO\",\"zoneName\":\"Cardio\"},{\"minuteMultiplier\":2,\"minutes\":0,\"order\":3,\"type\":\"PEAK\",\"zoneName\":\"Peak\"}],\"totalMinutes\":9},\"activityLevel\":[{\"minutes\":0,\"name\":\"sedentary\"},{\"minutes\":0,\"name\":\"lightly\"},{\"minutes\":7,\"name\":\"fairly\"},{\"minutes\":8,\"name\":\"very\"}],\"activityName\":\"Walk\",\"activityTypeId\":90013,\"averageHeartRate\":119,\"calories\":94,\"duration\":922000,\"elevationGain\":6.096,\"hasActiveZoneMinutes\":true,\"heartRateLink\":\"https://api.fitbit.com/1/user/-/activities/heart/date/2021-03-15/2021-03-15/1sec/time/14:04:08/14:19:30.json\",\"heartRateZones\":[{\"caloriesOut\":38.554080000000006,\"max\":118,\"min\":30,\"minutes\":6,\"name\":\"Out of Range\"},{\"caloriesOut\":55.13648,\"max\":145,\"min\":118,\"minutes\":9,\"name\":\"Fat Burn\"},{\"caloriesOut\":0,\"max\":178,\"min\":145,\"minutes\":0,\"name\":\"Cardio\"},{\"caloriesOut\":0,\"max\":220,\"min\":178,\"minutes\":0,\"name\":\"Peak\"}],\"lastModified\":\"2021-03-15T14:06:16.000Z\",\"logId\":38268792525,\"logType\":\"auto_detected\",\"manualValuesSpecified\":{\"calories\":false,\"distance\":false,\"steps\":false},\"originalDuration\":922000,\"originalStartTime\":\"2021-03-15T14:04:08.000+01:00\",\"startTime\":\"2021-03-15T14:04:08.000+01:00\",\"steps\":1372,\"tcxLink\":\"https://api.fitbit.com/1/user/-/activities/38268792525.tcx\"},{\"activeDuration\":922000,\"activeZoneMinutes\":{\"minutesInHeartRateZones\":[{\"minuteMultiplier\":1,\"minutes\":13,\"order\":1,\"type\":\"FAT_BURN\",\"zoneName\":\"Fat Burn\"},{\"minuteMultiplier\":0,\"minutes\":0,\"order\":0,\"type\":\"OUT_OF_ZONE\",\"zoneName\":\"Out of Range\"},{\"minuteMultiplier\":2,\"minutes\":0,\"order\":2,\"type\":\"CARDIO\",\"zoneName\":\"Cardio\"},{\"minuteMultiplier\":2,\"minutes\":0,\"order\":3,\"type\":\"PEAK\",\"zoneName\":\"Peak\"}],\"totalMinutes\":13},\"activityLevel\":[{\"minutes\":0,\"name\":\"sedentary\"},{\"minutes\":0,\"name\":\"lightly\"},{\"minutes\":3,\"name\":\"fairly\"},{\"minutes\":12,\"name\":\"very\"}],\"activityName\":\"Walk\",\"activityTypeId\":90013,\"averageHeartRate\":126,\"calories\":100,\"duration\":922000,\"elevationGain\":18.288,\"hasActiveZoneMinutes\":true,\"heartRateLink\":\"https://api.fitbit.com/1/user/-/activities/heart/date/2021-03-15/2021-03-15/1sec/time/14:51:04/15:06:26.json\",\"heartRateZones\":[{\"caloriesOut\":13.26591999999998,\"max\":118,\"min\":30,\"minutes\":2,\"name\":\"Out of Range\"},{\"caloriesOut\":86.64304000000001,\"max\":145,\"min\":118,\"minutes\":13,\"name\":\"Fat Burn\"},{\"caloriesOut\":0,\"max\":178,\"min\":145,\"minutes\":0,\"name\":\"Cardio\"},{\"caloriesOut\":0,\"max\":220,\"min\":178,\"minutes\":0,\"name\":\"Peak\"}],\"lastModified\":\"2021-03-15T14:21:22.000Z\",\"logId\":38269222880,\"logType\":\"auto_detected\",\"manualValuesSpecified\":{\"calories\":false,\"distance\":false,\"steps\":false},\"originalDuration\":922000,\"originalStartTime\":\"2021-03-15T14:51:04.000+01:00\",\"startTime\":\"2021-03-15T14:51:04.000+01:00\",\"steps\":1236,\"tcxLink\":\"https://api.fitbit.com/1/user/-/activities/38269222880.tcx\"}],\"pagination\":{\"afterDate\":\"2021-02-18\",\"limit\":100,\"next\":\"\",\"offset\":0,\"previous\":\"\",\"sort\":\"asc\"}}\n");
        fitbitSyncService.createAndPersistActivityLogs(json, LocalDate.parse("2021-02-18"), LocalDate.parse("2021-04-18"), user);

        List<ActivityLog> listAfter = activityLogRepository.findAll();

        Assert.assertTrue(listAfter.size() != listBefore.size());
        activityLogRepository.deleteAll();
    }

    @After
    public void tearDown() throws Exception {
        userRepo.deleteAll();
    }
}
