package springboot.application.healthdataapp.service;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import springboot.application.healthdataapp.Generator;
import springboot.application.healthdataapp.service.authentication.AuthenticationRequest;
import springboot.application.healthdataapp.dao.UserDao;
import springboot.application.healthdataapp.exception.BadCredentialsException;
import springboot.application.healthdataapp.model.type.Role;
import springboot.application.healthdataapp.model.user.User;
import springboot.application.healthdataapp.dao.repository.UserRepository;
import springboot.application.healthdataapp.dao.repository.WeightLogRepository;

import static org.junit.Assert.*;


@SpringBootTest
@ActiveProfiles("test")
@RunWith(SpringRunner.class)
public class UserServiceTest {
    @Autowired UserService service;

    @Autowired WeightLogRepository repo;

    @Autowired
    private UserRepository userRepo;

    @Autowired
    private UserDao dao;

    private User user;

    @Before
    public void init() {
        User user = new User();
        user.setEmail(Generator.generateEmail());
        user.setPassword("123456789");
        user.setRole(Role.ROLE_USER);
        userRepo.save(user);
        this.user = userRepo.findByEmail(user.getEmail());;
    }

    @Test
    public void whenAddUserWithInvalidEmailFormat_ThenNotCreate() {
        User user = new User();
        user.setEmail("non-email");
        user.setPassword("123456789");
        service.createUser(user, Role.ROLE_USER);
        assertNull(userRepo.findByEmail("non-email"));
    }

    @Test
    public void whenAttemptAuthenticationWithValidCredentials_ThenSuccess() {
        User user = new User();
        user.setEmail("iducanhle@gmail.com");
        user.setPassword("123456789");
        user.setRole(Role.ROLE_USER);

        service.createUser(user, Role.ROLE_USER);

        AuthenticationRequest req = new AuthenticationRequest(user.getEmail(), "123456789");
        ResponseEntity<?> responseEntity = service.authenticate(req);

        assertEquals(responseEntity.getStatusCode(), HttpStatus.OK);
    }

    @Test
    public void whenAttemptAuthenticationWithInvalidCredentials_ThenExceptionThrown() {
        User user = new User();
        user.setEmail("iducanhle@gmail.com");
        user.setPassword("123456789");
        user.setRole(Role.ROLE_USER);

        service.createUser(user, Role.ROLE_USER);

        AuthenticationRequest req = new AuthenticationRequest(user.getEmail(), "12345678498499");


        assertThrows(BadCredentialsException.class, () -> {
            ResponseEntity<?> responseEntity = service.authenticate(req);
        });
    }

    @After
    public void teardown() {
        repo.deleteAll();
    }
}
