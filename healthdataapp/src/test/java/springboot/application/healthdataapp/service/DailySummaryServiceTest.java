package springboot.application.healthdataapp.service;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import springboot.application.healthdataapp.Generator;
import springboot.application.healthdataapp.model.dailysummary.DailySummary;
import springboot.application.healthdataapp.model.type.Role;
import springboot.application.healthdataapp.model.type.TokenType;
import springboot.application.healthdataapp.model.user.User;
import springboot.application.healthdataapp.dao.repository.DailySummaryRepository;
import springboot.application.healthdataapp.dao.repository.UserRepository;

import java.util.List;

import static org.junit.Assert.assertEquals;


@SpringBootTest
@ActiveProfiles("test")
@RunWith(SpringRunner.class)
public class DailySummaryServiceTest {
    @Autowired DailySummaryService service;

    @Autowired DailySummaryRepository repo;

    @Autowired
    private UserRepository userRepo;

    private User user;

    @Before
    public void init() {
        User user = new User();
        user.setEmail(Generator.generateEmail());
        user.setPassword("123456789");
        user.setRole(Role.ROLE_USER);
        userRepo.save(user);
        this.user = user;
    }

    @Test
    public void whenAddLogWithNegativeValue_ThenNotSaveIntoDatabase() {
        List<DailySummary> all = repo.findAll();
        DailySummary log = new DailySummary();
        log.setOrigin(TokenType.THIS);
        log.setSteps(-6000);
        log.setDate(Generator.generateLocalDate());
        log.setDistance((double) 50000);
        log.setCalories(500);
        log.setUser(user);
        service.addDailySummary(log, user);
        List<DailySummary> allAfter = repo.findAll();

        assertEquals(all.size(), allAfter.size());
    }

    @Test
    public void whenAddLogWithSameDateTwice_ThenDatabaseWillOnlyHaveOne() {
        DailySummary log = new DailySummary();
        log.setOrigin(TokenType.THIS);
        log.setSteps(300);
        log.setDate(Generator.generateLocalDate());
        log.setDistance((double) 50000);
        log.setCalories(500);
        log.setUser(user);
        service.addDailySummary(log, user);
        List<DailySummary> all = repo.findAll();

        DailySummary log1 = new DailySummary();
        log1.setOrigin(TokenType.THIS);
        log1.setSteps(-6000);
        log1.setDate(Generator.generateLocalDate());
        log1.setDistance((double) 50000);
        log1.setCalories(500);
        log1.setUser(user);
        service.addDailySummary(log, user);
        List<DailySummary> allAfter = repo.findAll();

        assertEquals(1, all.size());
        assertEquals(1, allAfter.size());
    }

    @After
    public void teardown() {
        repo.deleteAll();
    }
}
