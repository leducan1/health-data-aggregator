package springboot.application.healthdataapp.service;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import springboot.application.healthdataapp.Generator;
import springboot.application.healthdataapp.model.sleep.SleepLog;
import springboot.application.healthdataapp.model.type.Role;
import springboot.application.healthdataapp.model.type.TokenType;
import springboot.application.healthdataapp.model.user.User;
import springboot.application.healthdataapp.dao.repository.SleepLogRepository;
import springboot.application.healthdataapp.dao.repository.UserRepository;

import java.time.LocalDateTime;
import java.util.List;

import static org.junit.Assert.assertEquals;


@SpringBootTest
@ActiveProfiles("test")
@RunWith(SpringRunner.class)
public class SleepServiceTest {
    @Autowired SleepService service;

    @Autowired SleepLogRepository repo;

    @Autowired
    private UserRepository userRepo;

    private User user;

    @Before
    public void init() {
        User user = new User();
        user.setEmail(Generator.generateEmail());
        user.setPassword("123456789");
        user.setRole(Role.ROLE_USER);
        userRepo.save(user);
        this.user = user;
    }

    @Test
    public void whenAddLogWithNoStartAndEndtime_ThenNotSaveIntoDatabase() {
        List<SleepLog> all = repo.findAll();
        SleepLog log = new SleepLog();
        LocalDateTime ldt = Generator.generateLocalDateTime();
        log.setOrigin(TokenType.THIS);
        log.setDate(ldt.toLocalDate());
        log.setUser(user);
        service.addSleepLog(log, user, ldt.toLocalDate().toString());
        List<SleepLog> allAfter = repo.findAll();

        assertEquals(all.size(), allAfter.size());
    }

    @Test
    public void whenAddLogWithNegativeEfficiency_ThenNotSaveIntoDatabase() {
        List<SleepLog> all = repo.findAll();
        SleepLog log = new SleepLog();
        LocalDateTime ldt = Generator.generateLocalDateTime();
        log.setOrigin(TokenType.THIS);
        log.setStartDateTime(ldt);
        log.setEndDateTime(ldt.plusMinutes(800));
        log.setDate(ldt.toLocalDate());
        log.setUser(user);
        log.setEfficiency(-50);
        service.addSleepLog(log, user, ldt.toLocalDate().toString());
        List<SleepLog> allAfter = repo.findAll();

        assertEquals(all.size(), allAfter.size());
    }

    @Test
    public void whenAddLogWithNegativeSecondsAsleep_ThenNotSaveIntoDatabase() {
        List<SleepLog> all = repo.findAll();
        SleepLog log = new SleepLog();
        LocalDateTime ldt = Generator.generateLocalDateTime();
        log.setOrigin(TokenType.THIS);
        log.setStartDateTime(ldt);
        log.setEndDateTime(ldt.plusMinutes(800));
        log.setDate(ldt.toLocalDate());
        log.setUser(user);
        log.setEfficiency(60);
        log.setSecondsAsleep(-500);
        service.addSleepLog(log, user, ldt.toLocalDate().toString());
        List<SleepLog> allAfter = repo.findAll();

        assertEquals(all.size(), allAfter.size());
    }

    @Test
    public void whenAddValidLog_ThenSaveIntoDatabase() {
        SleepLog log = new SleepLog();
        LocalDateTime ldt = Generator.generateLocalDateTime();
        List<SleepLog> all = service.getSleepLogByDateRange(user, TokenType.THIS, ldt.toLocalDate().toString(), ldt.toLocalDate().toString());
        log.setOrigin(TokenType.THIS);
        log.setStartDateTime(ldt);
        log.setEndDateTime(ldt.plusMinutes(800));
        log.setDate(ldt.toLocalDate());
        log.setUser(user);
        log.setEfficiency(60);
        log.setSecondsAsleep(500);
        service.addSleepLog(log, user, ldt.toLocalDate().toString());
        List<SleepLog> allAfter = service.getSleepLog(user, TokenType.THIS, ldt.toLocalDate().toString());

        assertEquals(all.size() + 1, allAfter.size());
    }

    @After
    public void teardown() {
        repo.deleteAll();
    }
}
