package springboot.application.healthdataapp.service;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import springboot.application.healthdataapp.Generator;
import springboot.application.healthdataapp.model.activity.ActivityLog;
import springboot.application.healthdataapp.model.type.Role;
import springboot.application.healthdataapp.model.type.TokenType;
import springboot.application.healthdataapp.model.user.User;
import springboot.application.healthdataapp.dao.repository.ActivityLogRepository;
import springboot.application.healthdataapp.dao.repository.UserRepository;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.List;

import static org.junit.Assert.assertEquals;


@SpringBootTest
@ActiveProfiles("test")
@RunWith(SpringRunner.class)
public class ActivityServiceTest {
    @Autowired ActivityService service;

    @Autowired ActivityLogRepository repo;

    @Autowired
    private UserRepository userRepo;

    private User user;

    @Before
    public void init() {
        User user = new User();
        user.setEmail(Generator.generateEmail());
        user.setPassword("123456789");
        user.setRole(Role.ROLE_USER);
        userRepo.save(user);
        this.user = user;
    }

    @Test
    public void whenAddLogWithNegativeCalories_ThenNotSaveIntoDatabase() {
        List<ActivityLog> all = repo.findAll();
        ActivityLog log = new ActivityLog();
        log.setOrigin(TokenType.THIS);
        log.setDurationInSeconds(520);
        log.setCaloriesOut(-500);
        log.setDate(Generator.generateLocalDate());
        log.setStartDateTime(Generator.generateLocalDateTime());
        log.setUser(user);
        service.addActivityLog(log, user, log.getDate().toString());
        List<ActivityLog> allAfter = repo.findAll();

        assertEquals(all.size(), allAfter.size());
    }

    @Test
    public void whenAddLogWithSameDateTwice_ThenDatabaseWillOnlyHaveOne() {
        ActivityLog log = new ActivityLog();
        LocalDateTime date = Generator.generateLocalDateTime();
        log.setOrigin(TokenType.THIS);
        log.setDurationInSeconds(180);
        log.setDate(date.toLocalDate());
        log.setStartDateTime(Generator.generateLocalDateTime());
        log.setUser(user);
        service.addActivityLog(log, user,log.getDate().toString());
        List<ActivityLog> all = repo.findAll();

        ActivityLog log1 = new ActivityLog();
        log1.setOrigin(TokenType.THIS);
        log1.setDurationInSeconds(180);
        log1.setDate(date.toLocalDate());
        log1.setStartDateTime(Generator.generateLocalDateTime());
        log1.setUser(user);
        service.addActivityLog(log, user,log.getDate().toString());
        List<ActivityLog> allAfter = service.getActivityLogs(user, TokenType.THIS, log1.getDate().toString());

        assertEquals(1, all.size());
        assertEquals(1, allAfter.size());
    }

    @After
    public void teardown() {
        repo.deleteAll();
    }
}
