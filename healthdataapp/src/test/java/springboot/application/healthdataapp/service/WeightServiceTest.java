package springboot.application.healthdataapp.service;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import springboot.application.healthdataapp.Generator;
import springboot.application.healthdataapp.model.type.Role;
import springboot.application.healthdataapp.model.type.TokenType;
import springboot.application.healthdataapp.model.user.User;
import springboot.application.healthdataapp.model.weight.WeightLog;
import springboot.application.healthdataapp.dao.repository.UserRepository;
import springboot.application.healthdataapp.dao.repository.WeightLogRepository;

import java.awt.font.TextHitInfo;
import java.time.LocalDateTime;
import java.util.List;

import static org.junit.Assert.assertEquals;


@SpringBootTest
@ActiveProfiles("test")
@RunWith(SpringRunner.class)
public class WeightServiceTest {
    @Autowired WeightService service;

    @Autowired WeightLogRepository repo;

    @Autowired
    private UserRepository userRepo;

    private User user;

    @Before
    public void init() {
        User user = new User();
        user.setEmail(Generator.generateEmail());
        user.setPassword("123456789");
        user.setRole(Role.ROLE_USER);
        userRepo.save(user);
        this.user = user;
    }

    @Test
    public void whenAddLogWithNegativeAmount_ThenNotSaveIntoDatabase() {
        WeightLog log = new WeightLog();
        LocalDateTime ldt = Generator.generateLocalDateTime();
        List<WeightLog> all = service.getWeightLogsByDateRange(ldt.toLocalDate().toString(), ldt.toLocalDate().toString(), TokenType.THIS, user);
        log.setOrigin(TokenType.THIS);
        log.setDate(ldt.toLocalDate());
        log.setUser(user);
        log.setWeightInKg(59.9);
        log.setTime(ldt.toLocalTime());
        log.setBmi(-12.9);
        log.setBodyFatPercentage(15.0);
        service.addWeightLog(log, user, ldt.toLocalDate().toString());
        List<WeightLog> allAfter = service.getWeightLogsByDate(ldt.toLocalDate().toString(), TokenType.THIS, user);

        assertEquals(all.size(), allAfter.size());
    }

    @Test
    public void whenAddMultipleLogsWithSameDateAndtime_ThenDatabaseSizeIsSame() {
        WeightLog log = new WeightLog();
        LocalDateTime ldt = Generator.generateLocalDateTime();
        log.setOrigin(TokenType.THIS);
        log.setDate(ldt.toLocalDate());
        log.setUser(user);
        log.setWeightInKg(59.9);
        log.setTime(ldt.toLocalTime());
        log.setBmi(12.9);
        log.setBodyFatPercentage(15.0);
        service.addWeightLog(log, user, ldt.toLocalDate().toString());
        List<WeightLog> all = repo.findAll();

        WeightLog log1 = new WeightLog();
        log1.setOrigin(TokenType.THIS);
        log1.setDate(ldt.toLocalDate());
        log1.setUser(user);
        log1.setWeightInKg(59.9);
        log1.setTime(ldt.toLocalTime());
        log1.setBmi(12.9);
        log1.setBodyFatPercentage(15.0);
        service.addWeightLog(log1, user, ldt.toLocalDate().toString());
        List<WeightLog> allAfter = repo.findAll();

        assertEquals(all.size(), allAfter.size());
    }

    @After
    public void teardown() {
        repo.deleteAll();
    }
}
