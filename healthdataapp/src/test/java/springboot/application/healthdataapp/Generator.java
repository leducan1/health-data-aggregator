package springboot.application.healthdataapp;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.Random;
import java.util.concurrent.ThreadLocalRandom;

/* asi to nebude moc useful, ale kdo vi :D */
public abstract class Generator {

    public static String generateString() {
        int leftLimit = 97; // letter 'a'
        int rightLimit = 122; // letter 'z'
        int targetStringLength = new Random().nextInt((8 - 3) + 1) + 3;
        Random random = new Random();

        return random.ints(leftLimit, rightLimit + 1)
                .limit(targetStringLength)
                .collect(StringBuilder::new, StringBuilder::appendCodePoint, StringBuilder::append)
                .toString();
    }

    public static int getRandomInt(int min, int max) {
        return ThreadLocalRandom.current().nextInt(min, max + 1);
    }

    public static LocalDate generateLocalDate() {
        long minDay = LocalDate.of(1970, 1, 1).toEpochDay();
        long maxDay = LocalDate.of(2015, 12, 31).toEpochDay();
        long randomDay = ThreadLocalRandom.current().nextLong(minDay, maxDay);
        return LocalDate.ofEpochDay(randomDay);
    }

    public static LocalDateTime generateLocalDateTime() {
        return  LocalDateTime.of(getRandomInt(1970,2020),getRandomInt(1,12),getRandomInt(1,28),getRandomInt(0,23),getRandomInt(0,59),getRandomInt(0,59));
    }

    public static LocalTime getRandomLocaTime() {
        return LocalTime.of(getRandomInt(0,23), getRandomInt(0,59), getRandomInt(0,59));
    }

    public static String generateEmail() {
        return generateString() + "@" + generateString() + ".com";
    }

    public static String generatePassword() {
        return generateString();
    }
}
