create table activity_log
(
    id                  integer      not null,
    calories_out        integer,
    date                date         not null,
    description         varchar(160),
    distance_in_meters  double precision,
    duration_in_seconds integer      not null,
    name                varchar(120),
    origin              varchar(255) not null,
    speed               double precision,
    start_date_time     datetime     not null,
    steps               integer,
    activity_minutes_id integer,
    user_id             integer      not null,
    primary key (id)
) engine=MyISAM
create table activity_minutes
(
    id        integer not null,
    fairly    integer,
    lightly   integer,
    sedentary integer,
    very      integer,
    primary key (id)
) engine=MyISAM
create table auth_credentials
(
    id            integer       not null,
    access_token  varchar(2048) not null,
    refresh_token varchar(2048) not null,
    scope         varchar(2048),
    token_type    varchar(255),
    user_id       integer,
    primary key (id)
) engine=MyISAM
create table blood_pressure
(
    id        integer      not null,
    date      date         not null,
    diastolic integer      not null,
    origin    varchar(255) not null,
    systolic  integer      not null,
    time      time         not null,
    user_id   integer,
    primary key (id)
) engine=MyISAM
create table daily_summary
(
    id                  integer not null,
    calories            integer,
    calories_activities integer,
    date                date    not null,
    distance            double precision,
    elevation           double precision,
    floors              double precision,
    origin              varchar(255),
    steps               integer,
    user_id             integer not null,
    primary key (id)
) engine=MyISAM
create table heart_rate
(
    id              integer      not null,
    date            date         not null,
    end_date_time   datetime     not null,
    origin          varchar(255) not null,
    start_date_time datetime     not null,
    value           integer      not null,
    user_id         integer,
    primary key (id)
) engine=MyISAM
create table hibernate_sequence
(
    next_val bigint
) engine=MyISAM
insert into hibernate_sequence
values (1)
create table hr_zone
(
    id              integer      not null,
    hrzone_type     varchar(255) not null,
    calories_out    integer,
    max             integer      not null,
    min             integer      not null,
    minutes         integer      not null,
    activity_log_id integer      not null,
    primary key (id)
) engine=MyISAM
create table sleep_level
(
    id                  integer not null,
    duration_in_seconds integer,
    end_date_time       datetime,
    level_type          integer,
    star_date_time      datetime,
    sleep_log_id        integer,
    primary key (id)
) engine=MyISAM
create table sleep_log
(
    id                  integer      not null,
    date                date         not null,
    duration_in_seconds integer,
    efficiency          integer,
    end_date_time       datetime     not null,
    origin              varchar(255) not null,
    seconds_asleep      integer,
    seconds_awake       integer,
    start_date_time     datetime     not null,
    user_id             integer      not null,
    primary key (id)
) engine=MyISAM
create table users
(
    id          integer      not null,
    create_date datetime,
    email       varchar(320) not null,
    password    varchar(128) not null,
    role        integer      not null,
    primary key (id)
) engine=MyISAM
create table water_log
(
    id      integer          not null,
    amount  double precision not null,
    date    date             not null,
    origin  varchar(255)     not null,
    unit    varchar(255)     not null,
    user_id integer          not null,
    primary key (id)
) engine=MyISAM
create table weight_log
(
    id                  integer          not null,
    bmi                 double precision,
    body_fat_percentage double precision,
    date                date             not null,
    origin              varchar(255)     not null,
    time                time,
    weight_in_kg        double precision not null,
    user_id             integer          not null,
    primary key (id)
) engine=MyISAM
alter table activity_log
    add constraint FKijl57p63fsfc41fgljjb45rxs foreign key (activity_minutes_id) references activity_minutes (id)
alter table activity_log
    add constraint FK634sb3dvk7nyxjcjcem7m282a foreign key (user_id) references users (id)
alter table auth_credentials
    add constraint FK46e9qm3gfbpgb3h4ckyqwmdam foreign key (user_id) references users (id)
alter table blood_pressure
    add constraint FK2ssh6gqo3srn5va4wwb2cdy85 foreign key (user_id) references users (id)
alter table daily_summary
    add constraint FKn2qnftlt6wqgo5c124p92obmt foreign key (user_id) references users (id)
alter table heart_rate
    add constraint FKlpybyyqajwacgfs111vhc5ejj foreign key (user_id) references users (id)
alter table hr_zone
    add constraint FK52rfhg5p94ogyqwkl7jq46rpa foreign key (activity_log_id) references activity_log (id)
alter table sleep_level
    add constraint FK7v1fhywiv6mp7t82r5dlthm7r foreign key (sleep_log_id) references sleep_log (id)
alter table sleep_log
    add constraint FK83bjnwujdn85k2y55u3sv506d foreign key (user_id) references users (id)
alter table water_log
    add constraint FKq5b8p9e4v59ecaxoptgnnmoaj foreign key (user_id) references users (id)
alter table weight_log
    add constraint FK14pit7tbt960llufekx95kbx6 foreign key (user_id) references users (id)
create table activity_log (id integer not null, calories_out integer, date date not null, description varchar(160), distance_in_meters double precision, duration_in_seconds integer not null, name varchar(120), origin varchar(255) not null, speed double precision, start_date_time datetime not null, steps integer, activity_minutes_id integer, user_id integer not null, primary key (id)) engine=MyISAM
create table activity_minutes (id integer not null, fairly integer, lightly integer, sedentary integer, very integer, primary key (id)) engine=MyISAM
create table auth_credentials (id integer not null, access_token varchar(2048) not null, refresh_token varchar(2048) not null, scope varchar(2048), token_type varchar(255), user_id integer, primary key (id)) engine=MyISAM
create table blood_pressure (id integer not null, date date not null, diastolic integer not null, origin varchar(255) not null, systolic integer not null, time time not null, user_id integer, primary key (id)) engine=MyISAM
create table daily_summary (id integer not null, calories integer, calories_activities integer, date date not null, distance double precision, elevation double precision, floors double precision, origin varchar(255), steps integer, user_id integer not null, primary key (id)) engine=MyISAM
create table heart_rate (id integer not null, date date not null, end_date_time datetime not null, origin varchar(255) not null, start_date_time datetime not null, value integer not null, user_id integer, primary key (id)) engine=MyISAM
create table hibernate_sequence (next_val bigint) engine=MyISAM
insert into hibernate_sequence values ( 1 )
create table hr_zone (id integer not null, hrzone_type varchar(255) not null, calories_out integer, max integer not null, min integer not null, minutes integer not null, activity_log_id integer not null, primary key (id)) engine=MyISAM
create table sleep_level (id integer not null, duration_in_seconds integer, end_date_time datetime, level_type integer, star_date_time datetime, sleep_log_id integer, primary key (id)) engine=MyISAM
create table sleep_log (id integer not null, date date not null, duration_in_seconds integer, efficiency integer, end_date_time datetime not null, origin varchar(255) not null, seconds_asleep integer, seconds_awake integer, start_date_time datetime not null, user_id integer not null, primary key (id)) engine=MyISAM
create table users (id integer not null, create_date datetime, email varchar(320) not null, password varchar(128) not null, role integer not null, primary key (id)) engine=MyISAM
create table water_log (id integer not null, amount double precision not null, date date not null, origin varchar(255) not null, unit varchar(255) not null, user_id integer not null, primary key (id)) engine=MyISAM
create table weight_log (id integer not null, bmi double precision, body_fat_percentage double precision, date date not null, origin varchar(255) not null, time time, weight_in_kg double precision not null, user_id integer not null, primary key (id)) engine=MyISAM
alter table activity_log add constraint FKijl57p63fsfc41fgljjb45rxs foreign key (activity_minutes_id) references activity_minutes (id)
alter table activity_log add constraint FK634sb3dvk7nyxjcjcem7m282a foreign key (user_id) references users (id)
alter table auth_credentials add constraint FK46e9qm3gfbpgb3h4ckyqwmdam foreign key (user_id) references users (id)
alter table blood_pressure add constraint FK2ssh6gqo3srn5va4wwb2cdy85 foreign key (user_id) references users (id)
alter table daily_summary add constraint FKn2qnftlt6wqgo5c124p92obmt foreign key (user_id) references users (id)
alter table heart_rate add constraint FKlpybyyqajwacgfs111vhc5ejj foreign key (user_id) references users (id)
alter table hr_zone add constraint FK52rfhg5p94ogyqwkl7jq46rpa foreign key (activity_log_id) references activity_log (id)
alter table sleep_level add constraint FK7v1fhywiv6mp7t82r5dlthm7r foreign key (sleep_log_id) references sleep_log (id)
alter table sleep_log add constraint FK83bjnwujdn85k2y55u3sv506d foreign key (user_id) references users (id)
alter table water_log add constraint FKq5b8p9e4v59ecaxoptgnnmoaj foreign key (user_id) references users (id)
alter table weight_log add constraint FK14pit7tbt960llufekx95kbx6 foreign key (user_id) references users (id)
create table activity_log (id integer not null, calories_out integer, date date not null, description varchar(160), distance_in_meters double precision, duration_in_seconds integer not null, name varchar(120), origin varchar(255) not null, speed double precision, start_date_time datetime not null, steps integer, activity_minutes_id integer, user_id integer not null, primary key (id)) engine=MyISAM
create table activity_minutes (id integer not null, fairly integer, lightly integer, sedentary integer, very integer, primary key (id)) engine=MyISAM
create table auth_credentials (id integer not null, access_token varchar(2048) not null, refresh_token varchar(2048) not null, scope varchar(2048), token_type varchar(255), user_id integer, primary key (id)) engine=MyISAM
create table blood_pressure (id integer not null, date date not null, diastolic integer not null, origin varchar(255) not null, systolic integer not null, time time not null, user_id integer, primary key (id)) engine=MyISAM
create table daily_summary (id integer not null, calories integer, calories_activities integer, date date not null, distance double precision, elevation double precision, floors double precision, origin varchar(255), steps integer, user_id integer not null, primary key (id)) engine=MyISAM
create table heart_rate (id integer not null, date date not null, end_date_time datetime not null, origin varchar(255) not null, start_date_time datetime not null, value integer not null, user_id integer, primary key (id)) engine=MyISAM
create table hibernate_sequence (next_val bigint) engine=MyISAM
insert into hibernate_sequence values ( 1 )
create table hr_zone (id integer not null, hrzone_type varchar(255) not null, calories_out integer, max integer not null, min integer not null, minutes integer not null, activity_log_id integer not null, primary key (id)) engine=MyISAM
create table sleep_level (id integer not null, duration_in_seconds integer, end_date_time datetime, level_type integer, star_date_time datetime, sleep_log_id integer, primary key (id)) engine=MyISAM
create table sleep_log (id integer not null, date date not null, duration_in_seconds integer, efficiency integer, end_date_time datetime not null, origin varchar(255) not null, seconds_asleep integer, seconds_awake integer, start_date_time datetime not null, user_id integer not null, primary key (id)) engine=MyISAM
create table users (id integer not null, create_date datetime, email varchar(320) not null, password varchar(128) not null, role integer not null, primary key (id)) engine=MyISAM
create table water_log (id integer not null, amount double precision not null, date date not null, origin varchar(255) not null, unit varchar(255) not null, user_id integer not null, primary key (id)) engine=MyISAM
create table weight_log (id integer not null, bmi double precision, body_fat_percentage double precision, date date not null, origin varchar(255) not null, time time, weight_in_kg double precision not null, user_id integer not null, primary key (id)) engine=MyISAM
alter table activity_log add constraint FKijl57p63fsfc41fgljjb45rxs foreign key (activity_minutes_id) references activity_minutes (id)
alter table activity_log add constraint FK634sb3dvk7nyxjcjcem7m282a foreign key (user_id) references users (id)
alter table auth_credentials add constraint FK46e9qm3gfbpgb3h4ckyqwmdam foreign key (user_id) references users (id)
alter table blood_pressure add constraint FK2ssh6gqo3srn5va4wwb2cdy85 foreign key (user_id) references users (id)
alter table daily_summary add constraint FKn2qnftlt6wqgo5c124p92obmt foreign key (user_id) references users (id)
alter table heart_rate add constraint FKlpybyyqajwacgfs111vhc5ejj foreign key (user_id) references users (id)
alter table hr_zone add constraint FK52rfhg5p94ogyqwkl7jq46rpa foreign key (activity_log_id) references activity_log (id)
alter table sleep_level add constraint FK7v1fhywiv6mp7t82r5dlthm7r foreign key (sleep_log_id) references sleep_log (id)
alter table sleep_log add constraint FK83bjnwujdn85k2y55u3sv506d foreign key (user_id) references users (id)
alter table water_log add constraint FKq5b8p9e4v59ecaxoptgnnmoaj foreign key (user_id) references users (id)
alter table weight_log add constraint FK14pit7tbt960llufekx95kbx6 foreign key (user_id) references users (id)
