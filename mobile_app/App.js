import React, {useEffect, useMemo, useState} from 'react';
import {NavigationContainer} from '@react-navigation/native';
import BottomNavigation from './components/BottomNavigation';
import {AppContext} from './components/context';
import SignIn from './screens/SignIn';
import axios from 'axios';
import {BACKEND} from './constants/Constants';
import {getKey, setKey, removeKey} from './helpers/StorageHelper';
import RNSamsungHealth from 'rn-samsung-health';
import BackgroundService from 'react-native-background-actions';

import {LogBox} from 'react-native';
import RNSecureStorage, {ACCESSIBLE} from 'rn-secure-storage';
import DailyOverview from './screens/DailyOverview';
import RecordWaterScreen from './screens/RecordWaterScreen';
import RecordWeightScreen from './screens/RecordWeightScreen';
import RecordBloodPressureScreen from './screens/RecordBloodPressureScreen';
import {createStackNavigator} from '@react-navigation/stack';
import SignUp from './screens/SignUp';
import Simple from './screens/Simple';


LogBox.ignoreLogs(['Warning: ...']);
const Stack = createStackNavigator();
const App = () => {
    const [userToken, setUserToken] = useState(null);
    const [selectedDate, setSelectedDate] = useState(null);
    const [latestData, setLatestData] = useState(null);
    const [dataOrigin, setDataOrigin] = useState('samsung');

    useEffect(() => {

        let isMounted = true;
        RNSecureStorage.get('userToken').then((value) => {
            setUserToken(value);
        }).catch((err) => {
            signOut();
        });
        RNSecureStorage.get('origin').then((value) => {
            setDataOrigin(value);
        }).catch((err) => {

        });
        return () => { isMounted = false; };
    }, [latestData]);

    const isSignedIn = () => {
        const jwt = RNSecureStorage.get('userToken');
        return userToken != null && jwt != null;
    };

    const fetchData = async (endpoint) => {

        if (userToken != null) {
            try {
                const jwt = await RNSecureStorage.get('userToken');
                if (jwt != null) {
                    const response = await axios.get(BACKEND + `${endpoint}`, {
                        headers: {
                            Authorization: 'Bearer ' + jwt,
                        },
                    });

                    return response;
                }
            } catch (e) {
                if (e.response.status == 403) {
                    await signOut();
                }
                return {};
            }
        }
        return [];
    };

    const signIn = async ({username, password}) => {
        try {
            const response = await axios.post(
                'http://10.0.2.2:8080/login',
                {email: username, password: password},
            );
            if (response.status == 200) {
                setUserToken(response.data.message);
                await RNSecureStorage.set('userToken', response.data.message, {accessible: ACCESSIBLE.WHEN_UNLOCKED});
                await RNSamsungHealth.authorize();
            }
        } catch (e) {
            return 'Invalid user credentials';
        }
    };

    const signUp = async ({username, password}) => {
        try {
            const response = await axios.post('http://10.0.2.2:8080/register', {
                email: username,
                password: password
            })
            if (response.status == 201) {
                return "Registration successful";
            }
        } catch (e) {
            const status = e.response.status;
            if (status == 409)
                return "E-mail is already registered";
        }
    };

    const signOut = async () => {
        setUserToken(null);
        RNSecureStorage.remove('userToken').then((val) => {
            console.log(val);
        }).catch((err) => {
        });
        setDataOrigin('samsung')
        await RNSecureStorage.remove("origin").then((val) => {
            console.log(val);
        }).catch((err) => {
        });

        await BackgroundService.stop();
    };

    const syncHealthData = async () => {
        try {
            const jwt = await RNSecureStorage.get('userToken');
            let startDate = new Date().setDate(new Date().getDate() - 65); // x days back date
            let endDate = new Date().setDate(new Date().getDate() - 7); //today's date
            // let opt = {startDate, endDate};
            let opt = {startDate, endDate};
            const activities = await RNSamsungHealth.getExercise(opt);
            const weight = await RNSamsungHealth.getWeight(opt);
            const sleep = await RNSamsungHealth.getSleep(opt);
            const hr = await RNSamsungHealth.getHeartRate(opt);
            const water = await RNSamsungHealth.getWaterIntake(opt);
            const sleepStage = await RNSamsungHealth.getSleepStage(opt);
            const bloodPressure = await RNSamsungHealth.getBloodPressure(opt);
            const dailySteps = await RNSamsungHealth.getStepCountDailies(opt);
            const floors = await RNSamsungHealth.getFloorsClimbed(opt);
            const filteredDailySteps = dailySteps.filter(
                (record) => record.source.model == 'Combined',
            );


            let obj = {
                activities: activities,
                dailySteps: filteredDailySteps,
                weight: weight,
                sleep: sleep,
                heartRate: hr,
                water: water,
                sleepStage: sleepStage,
                bloodPressure: bloodPressure,
                floors: floors,
            };

            await axios.post(
                BACKEND + 'samsung/sync/' + startDate + '/' + endDate,
                obj
                , {
                    headers: {
                        Authorization: 'Bearer ' + jwt,
                    },
                });
        } catch (error) {
            console.log(error);
        }
    };

    return (
        <AppContext.Provider value={{latestData,setLatestData, dataOrigin, setDataOrigin, userToken, health: syncHealthData, signIn, signOut, signUp, fetchData, selectedDate, setSelectedDate}}>
            {
                userToken != null ?
                    <NavigationContainer>
                        <BottomNavigation/>
                    </NavigationContainer>
                    :
                    (
                        <NavigationContainer>
                            <Stack.Navigator initialRouteName={'Onboarding'}>
                                <Stack.Screen name={'Onboarding'}
                                              component={Simple}
                                              options={{headerShown: false}}
                                />
                                <Stack.Screen name={'Sign In'}
                                              component={SignIn}
                                              options={{headerShown: false}}
                                />
                                <Stack.Screen name={'Sign Up'}
                                              component={SignUp}
                                              options={{headerShown: false}}
                                />
                            </Stack.Navigator>
                        </NavigationContainer>
                    )
            }
        </AppContext.Provider>
    );
};

export default App;
