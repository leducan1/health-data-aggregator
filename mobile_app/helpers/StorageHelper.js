import RNSecureStorage, { ACCESSIBLE } from 'rn-secure-storage'

export function setKey(key, value) {
    RNSecureStorage.set(key, value, {accessible: ACCESSIBLE.WHEN_UNLOCKED})
        .then((res) => {
            console.log(res);
        }, (err) => {
            console.log(err);
        });
}

export function getKey(key) {
    RNSecureStorage.get(key).then((value) => {
        return value;
    }).catch((err) => {
        console.log(err)
    })
}

export function removeKey(key) {
    RNSecureStorage.remove(key).then((val) => {
        console.log(val)
    }).catch((err) => {
        console.log(err)
    });
}
