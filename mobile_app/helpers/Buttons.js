import React from 'react';
import { Text, View, StyleSheet, Pressable } from 'react-native';
import Icon from '../components/Icon';

export default function RecordButton({props}) {
    const { onPress, title = 'Record' } = props;
    return (
        <Pressable style={styles.recordButton} onPress={onPress}>
            <Text style={{color: "white"}}>{title}</Text>
        </Pressable>
    );
}

export function SyncButton(props) {
    const { title, onPress } = props;
    return (
        <Pressable onPress={onPress} style={styles.syncButton}>
            <Text style={{color: "white"}}>{title}</Text>
        </Pressable>
    );
}

export function DateButton(props) {
    const { title, onPress, label } = props;
    return (
        <Pressable style={{width: "50%"}} onPress={onPress} >
            {label != null && <Text style={{color: "#858585"}}>{label}</Text>}
            <View style={styles.dateButton}>
                <Icon hex={"000000"} type={"calendar"}/>
                <Text style={styles.text}>{title}</Text>
            </View>
        </Pressable>
    );
}

export function SubmitButton(props) {
    const { title, onPress, style } = props;
    return (
        <Pressable style={style} onPress={onPress} style={styles.submitButton}>
            <Text style={styles.submitButtonText}>{title}</Text>
        </Pressable>
    );
}


const styles = StyleSheet.create({
    recordButton: {
        borderWidth: 1,
        borderStyle: "solid",
        borderColor: "#5b5b5b",
        color: "#5b5b5b",
        borderRadius: 15,
        paddingTop: 5,
        paddingBottom: 5,
        paddingRight: 19,
        paddingLeft: 19,
        backgroundColor: "#262626"
    },
    syncButton: {
        borderWidth: 1,
        borderStyle: "solid",
        borderColor: "#5b5b5b",
        color: "#ffffff",
        borderRadius: 60,
        paddingTop: 14,
        paddingBottom: 14,
        paddingRight: 19,
        paddingLeft: 19,
        marginBottom: 20,
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: "#262626",
    },
    submitButton: {
        color: "#ffffff",
        borderRadius: 60,
        paddingTop: 14,
        paddingBottom: 14,
        paddingRight: 25,
        paddingLeft: 25,
        marginBottom: 20,
        display: "flex",
        justifyContent: "center",
        alignItems: "center",
        backgroundColor: 'tomato'
    },
    dateButton: {
        borderWidth: 1,
        borderStyle: "solid",
        borderColor: "#d4d4d4",
        color: "#5b5b5b",
        borderRadius: 6,
        paddingTop: 12,
        backgroundColor: "white",
        paddingBottom: 12,
        paddingRight: 19,
        paddingLeft: 19,
        marginBottom: 20,
        display: "flex",
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center",
    },
    text: {
        color: "#5b5b5b",
    },
    submitButtonText: {
        color: "#ffffff",
    }
});
