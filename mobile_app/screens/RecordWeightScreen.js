import 'react-native-gesture-handler';
import React, {useContext, useEffect, useState} from 'react';
import {Alert, Platform, ScrollView, StyleSheet, Text, TextInput, View} from 'react-native';
import RNSamsungHealth from 'rn-samsung-health';
import axios from 'axios';
import {AppContext} from '../components/context';
import RNSecureStorage from 'rn-secure-storage';
import {BACKEND} from '../constants/Constants';
import {AwesomeTextInput} from 'react-native-awesome-text-input';
import moment from 'moment';
import {SubmitButton} from '../helpers/Buttons';
import {err} from 'react-native-svg/lib/typescript/xml';

const RecordWeightScreen = () => {
    const {selectedDate} = useContext(AppContext);
    const [weight, setWeight] = useState('0');
    const [bodyFat, setBodyFat] = useState();
    const [errMsg, setErrMsg] = useState("");

    useEffect(() => {
    });

    const submitForm = async (event) => {
        const authToken = await RNSecureStorage.get('userToken');
        const date = selectedDate != null ? selectedDate : new Date();
        axios.post(BACKEND + "weight/this/" + moment(date).format("yyyy-MM-DD"), {
            date: moment(date).format("yyyy-MM-DD"),
            time: moment(date).format("HH:mm"),
            weightInKg: weight,
            bodyFatPercentage: bodyFat == "" ? null : bodyFat,
        }, {
            headers: {
                Authorization: "Bearer " + authToken
            }
        }).then(res => {
            Alert.alert("Success", res.data.message);
            setErrMsg("");
        }).catch(e => {
            setErrMsg(e.response.data.join("\n"));
        });
    };

    return (
        <ScrollView contentInsetAdjustmentBehavior="automatic" contentContainerStyle={{display: 'flex', flexDirection: 'column', alignItems: 'center', paddingTop: 50}}>
            <Text style={styles.heading2}>Add a weight log from {moment(selectedDate != null ? selectedDate : new Date()).format('DD.MM.yyyy')}</Text>
            {errMsg != "" && <Text style={styles.errMsg}>{errMsg}</Text>}
            <View>
                <Text style={styles.label}>Weight in kg*</Text>
                <TextInput
                    maxLength={4}
                    style={styles.input}
                    onChangeText={value => (!isNaN(value) && value >= 0) && setWeight(value)}
                    value={weight}
                    placeholder="kg"
                    keyboardType="numeric"
                />
            </View>
            <View>
                <Text style={styles.label}>Body fat percentage</Text>
                <TextInput
                    maxLength={2}
                    style={styles.input}
                    onChangeText={value => (!isNaN(value) && value >= 0) && setBodyFat(value)}
                    value={bodyFat}
                    placeholder="%"
                    keyboardType="numeric"
                />
            </View>
            <SubmitButton title={"Add Weight Log"} onPress={submitForm}/>
        </ScrollView>
    );
};

const styles = StyleSheet.create({
    main: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        width: '100%',
    },
    input: {
        borderWidth: 1,
        borderStyle: "solid",
        borderColor: "#d4d4d4",
        color: "#5b5b5b",
        borderRadius: 6,
        paddingTop: 12,
        backgroundColor: "white",
        paddingBottom: 12,
        paddingRight: 19,
        paddingLeft: 19,
        marginBottom: 20,
        display: "flex",
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center",
        width: 150,
        textAlign: "center"
    },
    label: {
        fontSize: 12,
        fontWeight: "bold",
        color: "#323232"
    },
    heading2: {
        fontSize: 18,
        fontWeight: "bold",
        marginBottom: 20
    },
    errMsg: {
        color: "red",
        marginBottom: 30,textAlign: "center"
    }
});

export default RecordWeightScreen;
