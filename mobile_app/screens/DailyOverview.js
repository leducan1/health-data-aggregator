import 'react-native-gesture-handler';
import React, {useContext, useEffect, useState} from 'react';
import DateTimePicker from '@react-native-community/datetimepicker';
import {Platform, Pressable, ScrollView, StyleSheet, Text, View} from 'react-native';
import style from '../styles/ScreenStyles';
import {AppContext} from '../components/context';
import moment from 'moment';
import RecordButton, {DateButton} from '../helpers/Buttons';
import Icon from '../components/Icon';
import {Path, Svg} from 'react-native-svg';
import ModalSelector from 'react-native-modal-selector';
import {getKey, setKey} from '../helpers/StorageHelper';
import RNSecureStorage, {ACCESSIBLE} from 'rn-secure-storage';


const DailyOverview = ({navigation}) => {
    const [date, setDate] = useState(new Date());
    const [summaryData, setSummaryData] = useState(null);
    const [sleepData, setSleepData] = useState(null);
    const [weightData, setWeightData] = useState(null);
    const [waterData, setWaterData] = useState(null);
    const [bpData, setBpData] = useState(null);
    const [mode, setMode] = useState('date');
    const [show, setShow] = useState(false);
    const {fetchData, selectedDate, latestData, setSelectedDate, dataOrigin, setDataOrigin} = useContext(AppContext);

    useEffect(() => {
        fetchDailySummary();
        fetchSleep();
        fetchWeight();
        fetchWater();
        fetchBP();
    }, [date, dataOrigin]);

    const fetchDailySummary = async () => {
        const res = await fetchData('daily-summary/' + dataOrigin.toLowerCase() + '/' + moment(date).format('yyyy-MM-DD'));

        const resdata = res.data;
        if (resdata != null && resdata.length > 0) {
            setSummaryData(res.data[0]);
        }
        else {
            setSummaryData(null);
        }
        return res;
    };

    const fetchSleep = async () => {
        const res = await fetchData('sleep/' + dataOrigin.toLowerCase() + '/' + moment(date).format('yyyy-MM-DD'));
        const resdata = res.data;
        if (resdata != null && resdata.length > 0) {
            setSleepData(res.data[0]);
        }
        else {
            setSleepData(null);
        }
        return res;
    };

    const fetchWeight = async () => {
        const res = await fetchData('weight/' + dataOrigin.toLowerCase() + '/' + moment(date).format('yyyy-MM-DD'));
        const resdata = res.data;
        if (resdata != null && resdata.length > 0) {
            setWeightData(res.data[0]);
        }
        else {
            setWeightData(null);
        }
        return res;
    };

    const fetchWater = async () => {
        const res = await fetchData('water/' + dataOrigin.toLowerCase() + '/' + moment(date).format('yyyy-MM-DD'));
        const resdata = res.data;
        if (resdata != null && resdata.length > 0) {
            setWaterData(res.data[0]);
        }
        else {
            setWaterData(null);
        }
        return res;
    };

    const fetchBP = async () => {
        const res = await fetchData('blood-pressure/' + dataOrigin.toLowerCase() + '/' + moment(date).format('yyyy-MM-DD'));
        const resdata = res.data;
        if (resdata != null && resdata.length > 0) {
            setBpData(res.data[0]);
        }
        else {
            setBpData(null);
        }
        return res;
    };

    const onChange = (event, selectedDate) => {
        const currentDate = selectedDate || date;
        setShow(Platform.OS === 'ios');

        setDate(currentDate);
        setSelectedDate(currentDate);
    };

    const showMode = (currentMode) => {
        setShow(true);
        setMode(currentMode);
    };

    const showDatepicker = () => {
        showMode('date');
    };

    const handlePressRecordWater = () => {
        navigation.navigate('Record Water Intake');
    };

    const handlePressRecordWeight = () => {
        navigation.navigate('Record Weight');
    };

    const handlePressSleep = () => {
        navigation.navigate('Sleep');
    };

    const handlePressRecordBloodPressure = () => {
        navigation.navigate('Record Blood Pressure');
    };

    const handlePressActivities = () => {
        navigation.navigate('Activities');
    };

    const handlePressBloodPressure = () => {
        navigation.navigate('Blood Pressure');
    };

    let index = 0;
    const data = [
        {key: index++, section: true, label: 'Select a platform'},
        {key: 'samsung', label: 'Samsung'},
        {key: 'fitbit', label: 'Fitbit'},
        {key: 'google', label: 'Google'},
        {key: 'this', label: 'This App'},
    ];

    const handleSetDataOrigin = async (value) => {
        setDataOrigin(value);

        try {
            await RNSecureStorage.set("origin", value, {accessible: ACCESSIBLE.WHEN_UNLOCKED});
        } catch (e) {
            alert(e)
        }
    }


    return (
        <ScrollView contentContainerStyle={{display: 'flex', flexDirection: 'column', width: '100%'}} contentInsetAdjustmentBehavior="automatic">
            <Text style={style.currentScreenHeading}>Daily Overview</Text>
            <View style={{flex: 1, justifyContent: 'center', alignItems: "center", padding: 50}}>
                <ModalSelector
                    data={data}
                    initValue={dataOrigin}
                    onChange={(option) => {handleSetDataOrigin(option.key)}}>
                    <Pressable style={{backgroundColor: '#262626', padding: 10, textAlign: "center", width: 150, borderRadius: 75}}>
                        <Text style={{color: "#ffffff", textAlign:"center"}}>{dataOrigin.toUpperCase()}</Text>
                    </Pressable>
                </ModalSelector>
            </View>
            <View style={{display: 'flex', flexDirection: 'row', alignItems: 'center', justifyContent: 'center', marginTop: 0}}>
                <Svg style={{marginTop: -17}} onPress={() => {
                    const currentDate = date;
                    const yesterdayDate = currentDate.setDate(currentDate.getDate() - 1);
                    setDate(new Date(yesterdayDate));
                    setSelectedDate(new Date(yesterdayDate));
                }} xmlns="http://www.w3.org/2000/Svg" width="32" height="32" viewBox="0 0 24 24" strokeWidth="1.5"
                     stroke={'#000000'}
                     fill="none" strokeLinecap="round" strokeLinejoin="round">
                    <Path stroke="none" d="M0 0h24v24H0z" fill="none"/>
                    <Path d="M18 15l-6 -6l-6 6h12" transform="rotate(270 12 12)"/>
                </Svg>
                <DateButton onPress={showDatepicker} title={date != null ? date.toDateString() : ''}/>
                <Svg style={{marginTop: -17}} onPress={() => {
                    const currentDate = date;
                    const yesterdayDate = currentDate.setDate(currentDate.getDate() + 1);
                    setDate(new Date(yesterdayDate));
                    setSelectedDate(new Date(yesterdayDate));
                }} xmlns="http://www.w3.org/2000/Svg" width="32" height="32" viewBox="0 0 24 24" strokeWidth="1.5"
                     stroke={'#000000'}
                     fill="none" strokeLinecap="round" strokeLinejoin="round">
                    <Path stroke="none" d="M0 0h24v24H0z" fill="none"/>
                    <Path d="M18 15l-6 -6l-6 6h12" transform="rotate(90 12 12)"/>
                </Svg>
            </View>
            {show && (
                <DateTimePicker
                    testID="dateTimePicker"
                    value={date}
                    mode={mode}
                    is24Hour={true}
                    display="default"
                    onChange={onChange}
                />
            )}
            <View>
                <View>
                    <View style={{display: 'flex', flexDirection: 'column', justifyContent: 'center', alignItems: 'center', marginTop: 25}}>
                        <Icon hex={'ff6347'} type={'flame'}/>
                        <Text style={{color: '#000000', marginTop: 4, fontSize: 25, fontWeight: 'bold'}}>{summaryData != null ? summaryData.calories + ' kcal' : ' - '}</Text>
                    </View>
                </View>
                <View style={{display: 'flex', flexDirection: 'row', justifyContent: 'center', alignItems: 'center'}}>
                    <View style={{margin: 40}}>
                        <View style={{display: 'flex', flexDirection: 'column', justifyContent: 'center', alignItems: 'center'}}>
                            <Icon hex={'ff6347'} type={'footprint'}/>
                            <Text style={{color: '#000000', marginTop: 4}}>{summaryData != null ? summaryData.steps + ' steps' : ' - '}</Text>
                        </View>
                    </View>
                    <View style={{margin: 40}}>
                        <View style={{display: 'flex', flexDirection: 'column', justifyContent: 'center', alignItems: 'center'}}>
                            <Icon hex={'ff6347'} type={'distance'}/>
                            <Text style={{color: '#000000', marginTop: 4}}>{summaryData != null ? parseFloat(summaryData.distance / 1000).toFixed(1) + ' km' : ' - '}</Text>
                        </View>
                    </View>
                </View>
            </View>
            <View style={styles.sections}>
                <Pressable style={styles.sectionDiv}>
                    <View style={{display: 'flex', flexDirection: 'row', alignItems: 'center', justifyContent: 'center'}}>
                        <Icon hex={'ff6347'} type={'water_drop'}/>
                        <Text style={{marginLeft: 10}}>{waterData != null ? parseFloat(waterData.amount).toFixed(0) + ' ' + waterData.unit.toLowerCase() : ' - '}</Text>
                    </View>
                    {dataOrigin == "this" && <RecordButton props={{styles, onPress: handlePressRecordWater}}/>}
                </Pressable>
                <Pressable onPress={handlePressSleep} style={styles.sectionDiv}>
                    <View style={{display: 'flex', flexDirection: 'row', alignItems: 'center', justifyContent: 'center'}}>
                        <Icon hex={'ff6347'} type={'sleep'}/>
                        <Text style={{marginLeft: 10}}>{(sleepData != null) ? parseFloat(sleepData.durationInSeconds / 60 / 60).toFixed(1) + ' h' : ' - '}</Text>
                    </View>
                </Pressable>
                <Pressable onPress={null} style={styles.sectionDiv}>
                    <View style={{display: 'flex', flexDirection: 'row', alignItems: 'center', justifyContent: 'center'}}>
                        <Icon hex={'ff6347'} type={'scale'}/>
                        <Text style={{marginLeft: 10}}>{(weightData != null) ? parseFloat(weightData.weightInKg).toFixed(1) + ' kg' : ' - '}</Text>
                    </View>
                    {dataOrigin == "this" && <RecordButton props={{styles, onPress: handlePressRecordWeight}}/>}
                </Pressable>
                <Pressable onPress={null} style={styles.sectionDiv}>
                    <View style={{display: 'flex', flexDirection: 'row', alignItems: 'center', justifyContent: 'center'}}>
                        <Icon hex={'ff6347'} type={'blood_pressure'}/>
                        <Text style={{marginLeft: 10}}>{(bpData != null) ? bpData.systolic + ' / ' + bpData.diastolic : ' - '}</Text>
                    </View>
                    {dataOrigin == "this" && <RecordButton props={{styles, onPress: handlePressRecordBloodPressure}}/>}
                </Pressable>
                <Pressable onPress={handlePressActivities} style={styles.sectionDiv}>
                    <View style={{display: 'flex', flexDirection: 'row', alignItems: 'center', justifyContent: 'center'}}>
                        <Icon hex={'ff6347'} type={'dumbbell'}/>
                        <Text style={{marginLeft: 10}}>Activities</Text>
                    </View>
                </Pressable>

            </View>
        </ScrollView>
    );
};

const styles = StyleSheet.create({
    sections: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    sectionDiv: {
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'space-between',
        alignItems: 'center',
        padding: 16,
        backgroundColor: '#ffffff',
        width: '90%',
        shadowColor: '#000',
        shadowOffset: {
            width: 0,
            height: 1,
        },
        shadowOpacity: 0.12,
        shadowRadius: 2.22,
        elevation: 2,
        borderRadius: 10,
        minHeight: 95,
        maxHeight: 95,
        marginBottom: 15,
    },
    button: {},
    text: {},
});

export default DailyOverview;
