import 'react-native-gesture-handler';
import React, {useEffect, useState} from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar, TextInput, Button, Pressable,
} from 'react-native';
import {AppContext} from '../components/context';
import {SubmitButton} from '../helpers/Buttons';
import {log} from 'react-native-reanimated';
import {err} from 'react-native-svg/lib/typescript/xml';

const SignIn = ({navigation}) => {
    const [username, setUsername] = React.useState('');
    const [password, setPassword] = React.useState('');
    const [errMsg, setErrMsg] = React.useState('');

    const {signIn} = React.useContext(AppContext);

    const logIn = async () => {
        const res = await signIn({username,password});
        if (res == "Invalid user credentials") {
            setErrMsg(res);
        }
    }

    return (
        <SafeAreaView style={{display: "flex", alignItems: "center"}}>
            <View style={{width: "80%"}}>
                <Text style={styles.h1}>Sign In</Text>
                {errMsg != null && <Text style={styles.errMsg}>{errMsg}</Text>}
                <TextInput
                    style={styles.input}
                    placeholder="E-mail"
                    value={username}
                    onChangeText={(text) => {
                        setErrMsg("");
                        setUsername(text)
                    }}
                />
                <TextInput
                    style={styles.input}
                    placeholder="Password"
                    value={password}
                    onChangeText={(text) => {
                        setErrMsg("");
                        setPassword(text)
                    }}
                    secureTextEntry
                />
                <View style={{marginTop: 20}}>
                    <SubmitButton title={"Sign In"} onPress={logIn}/>
                </View>
            </View>
            <View>
                <Text style={{marginTop: 50, textAlign: "center", fontSize: 15, color: "#565656"}}>Don't have an account yet?</Text>
                <Pressable onPress={() => {navigation.navigate('Sign Up')}}>
                    <Text style={{textAlign: "center", fontSize: 15, fontWeight: "bold", color: "tomato"}}>Create one here.</Text>
                </Pressable>
            </View>
        </SafeAreaView>
    );
};

const styles = StyleSheet.create({
    h1: {
        fontSize: 26,
        fontWeight: "bold",
        textAlign: "center",
        marginTop: 60,
        marginBottom: 40
    },
    input: {
        borderWidth: 1,
        borderStyle: "solid",
        borderColor: "#d4d4d4",
        color: "#5b5b5b",
        borderRadius: 6,
        paddingTop: 12,
        backgroundColor: "white",
        paddingBottom: 12,
        paddingRight: 19,
        paddingLeft: 19,
        marginBottom: 20,
        display: "flex",
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center",
        width: "100%",
        fontSize: 13
    },
    errMsg: {
        color: "red",
        fontSize: 16,
        textAlign: "center",
        marginBottom: 15
    }
})

export default SignIn;
