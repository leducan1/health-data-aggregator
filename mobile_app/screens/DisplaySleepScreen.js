import 'react-native-gesture-handler';
import React, {useContext, useEffect, useState} from 'react';
import {Alert, Platform, ScrollView, StyleSheet, Text, TextInput, View} from 'react-native';
import RNSamsungHealth from 'rn-samsung-health';
import axios from 'axios';
import {AppContext} from '../components/context';
import RNSecureStorage, {ACCESSIBLE} from 'rn-secure-storage';
import {BACKEND} from '../constants/Constants';
import {AwesomeTextInput} from 'react-native-awesome-text-input';
import moment from 'moment';
import {SubmitButton} from '../helpers/Buttons';
import Icon from '../components/Icon';
import {G, Path, Svg} from 'react-native-svg';

const DisplaySleepScreen = () => {
    const {fetchData, selectedDate} = useContext(AppContext);
    const [sleepData, setSleepData] = useState(null);

    useEffect(() => {
        fetchSleepData();
    }, []);

    const fetchSleepData = async () => {
        let origin = 'samsung';
        try {
            origin = await RNSecureStorage.get('origin');
        } catch (e) {
            console.log(e);
        }

        const date = selectedDate != null ? selectedDate : new Date();
        const res = await fetchData('sleep/' + origin + '/' + moment(date).format('yyyy-MM-DD'));
        const resdata = res.data;

        if (resdata != null) {
            setSleepData(res.data);
        }
        else {
            setSleepData([]);
        }
    };

    const renderSleepData = () => {
        const logs = sleepData.map((log, index) => {
            return (
                <View key={index} style={{display: 'flex', alignItems: 'center'}}>
                    <View>
                        <View style={{width: 120, height: 120, borderRadius: 60, backgroundColor: '#dbd6d6', display: 'flex', alignItems: 'center', justifyContent: 'center'}}>
                            <Svg enableBackground="new 0 0 512 512" height="24" viewBox="0 0 512 512" width="24"
                                 xmlns="http://www.w3.org/2000/Svg">
                                <G>
                                    <Path fill={'#000000'}
                                          d="m462.23 346.905c-6.197-2.929-13.38-3.093-19.705-.451-.286.12-.566.247-.842.382-44.39 18.044-93.17 17.885-137.478-.474-44.635-18.496-79.389-53.251-97.859-97.861-19.317-46.656-18.151-99.663 3.163-145.351 2.767-5.844 3.09-12.414.909-18.5-2.18-6.086-6.602-10.959-12.445-13.72-6.197-2.93-13.381-3.097-19.622-.488-56.715 23.275-100.962 67.22-124.592 123.741s-23.813 118.851-.516 175.509 67.286 100.862 123.863 124.468c28.5 11.893 58.474 17.84 88.456 17.84 29.538 0 59.083-5.774 87.229-17.324 53.634-22.009 96.605-63.065 121.006-115.606 5.668-12.05.488-26.469-11.567-32.165zm-272.392-248.776c-.021.009-.043.018-.065.026.019-.007.047-.019.065-.026zm151.555 368.825c-101.779 41.768-218.592-6.974-260.4-108.648-39.942-97.138 2.812-207.992 95.535-254.144-19.123 50.034-18.515 106.016 2.096 155.795 21.533 52.008 62.05 92.525 114.087 114.089 47.365 19.626 99.116 21.411 147.284 5.354-21.922 39.468-56.479 70.268-98.602 87.554z"/>
                                    <Path fill={'#000000'}
                                          d="m461.098 64.273h-24.8l35.868-39.174c4.013-4.382 5.056-10.72 2.658-16.155-2.396-5.436-7.781-8.944-13.726-8.944h-58.85c-8.284 0-15 6.709-15 14.985s6.716 14.985 15 14.985h24.8l-35.868 39.174c-4.013 4.382-5.056 10.72-2.658 16.155 2.396 5.436 7.781 8.944 13.727 8.944h58.85c8.284 0 15-6.709 15-14.985s-6.717-14.985-15.001-14.985z"/>
                                    <Path fill={'#000000'}
                                          d="m277.685 111.546c2.396 5.436 7.781 8.944 13.727 8.944h47.036c8.284 0 15-6.709 15-14.985s-6.716-14.985-15-14.985h-12.986l24.055-26.272c4.013-4.382 5.056-10.72 2.658-16.155-2.396-5.436-7.781-8.944-13.727-8.944h-47.036c-8.284 0-15 6.709-15 14.985s6.716 14.985 15 14.985h12.986l-24.055 26.272c-4.013 4.382-5.056 10.72-2.658 16.155z"/>
                                    <Path fill={'#000000'}
                                          d="m374.685 199.713c2.396 5.436 7.781 8.944 13.727 8.944h47.036c8.284 0 15-6.709 15-14.985s-6.716-14.985-15-14.985h-12.986l24.055-26.272c4.013-4.382 5.056-10.72 2.658-16.155-2.396-5.436-7.781-8.944-13.727-8.944h-47.036c-8.284 0-15 6.709-15 14.985s6.716 14.985 15 14.985h12.986l-24.055 26.272c-4.013 4.382-5.056 10.72-2.658 16.155z"/>
                                </G>
                            </Svg>
                            <Text style={{fontSize: 16, marginTop: 5}}>{parseFloat(log.durationInSeconds / 60 / 60).toFixed(1) + ' h'}</Text>
                        </View>
                        {log.efficiency != null && <Text style={{textAlign: "center", marginTop: 10}}>{log.efficiency + ' % Efficiency'}</Text>}
                    </View>
                    <View style={{display: 'flex', flexDirection: 'row'}}>
                        <View style={{display: 'flex', flexDirection: 'column', alignItems: 'center', justifyContent: 'center', margin: 35}}>
                            <Text style={{color: 'tomato'}}>Start</Text>
                            <Text style={{fontSize: 20, fontWeight: 'bold'}}>{moment(log.startDateTime).format('HH:mm')}</Text>
                        </View>
                        <View style={{display: 'flex', flexDirection: 'column', alignItems: 'center', justifyContent: 'center', margin: 35}}>
                            <Text style={{color: 'tomato'}}>End</Text>
                            <Text style={{fontSize: 20, fontWeight: 'bold'}}>{moment(log.endDateTime).format('HH:mm')}</Text>
                        </View>
                    </View>
                </View>
            );
        });

        return logs;
    };

    return (
        <ScrollView contentInsetAdjustmentBehavior="automatic" contentContainerStyle={{paddingTop: 50}}>
            <Text style={styles.heading2}>Sleep data from {moment(selectedDate != null ? selectedDate : new Date()).format('DD.MM.yyyy')}</Text>
            {sleepData != null && renderSleepData()}
        </ScrollView>
    );
};

const styles = StyleSheet.create({
    main: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        width: '100%',
    },
    input: {
        borderWidth: 1,
        borderStyle: 'solid',
        borderColor: '#d4d4d4',
        color: '#5b5b5b',
        borderRadius: 6,
        paddingTop: 12,
        backgroundColor: 'white',
        paddingBottom: 12,
        paddingRight: 19,
        paddingLeft: 19,
        marginBottom: 20,
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        width: 150,
        textAlign: 'center',
    },
    label: {
        fontSize: 12,
        fontWeight: 'bold',
        color: '#323232',
    },
    heading2: {
        fontSize: 21,
        fontWeight: 'bold',
        marginBottom: 40,
        textAlign: 'center',
    },
});

export default DisplaySleepScreen;
