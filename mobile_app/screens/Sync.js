import 'react-native-gesture-handler';
import React, {useContext, useEffect, useState} from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar, Platform, Button, Alert,
} from 'react-native';
import style from '../styles/ScreenStyles';
import {DateButton, SyncButton} from '../helpers/Buttons';
import RNSamsungHealth from 'rn-samsung-health';
import axios from 'axios';
import AsyncStorage from '@react-native-async-storage/async-storage';
import DateTimePicker from '@react-native-community/datetimepicker';
import moment from 'moment';
import {AppContext} from '../components/context';
import RNSecureStorage from "rn-secure-storage";
import {BACKEND} from '../constants/Constants';
import {emitNotificationDecl} from 'react-native/ReactCommon/hermes/inspector/tools/msggen/src/HeaderWriter';

const Sync = () => {
    const {health} = useContext(AppContext);

    const [dateFrom, setDateFrom] = useState(new Date());

    const [dateTo, setDateTo] = useState(new Date());

    const [mode, setMode] = useState('date');
    const [show, setShow] = useState(false);

    const [modeFrom, setModeFrom] = useState('date');
    const [showFrom, setShowFrom] = useState(false);

    const [modeTo, setModeTo] = useState('date');
    const [showTo, setShowTo] = useState(false);

    const healthDateRange = async () => {
        try {
            const jwt = await RNSecureStorage.get('userToken');
            let startDate = dateFrom.getTime() // x days back date
            let endDate = dateTo.getTime() //today's date
            // let opt = {startDate, endDate};
            let opt = {startDate, endDate};
            const activities = await RNSamsungHealth.getExercise(opt);
            const weight = await RNSamsungHealth.getWeight(opt);
            const sleep = await RNSamsungHealth.getSleep(opt);
            const hr = await RNSamsungHealth.getHeartRate(opt);
            const water = await RNSamsungHealth.getWaterIntake(opt);
            const sleepStage = await RNSamsungHealth.getSleepStage(opt);
            const bloodPressure = await RNSamsungHealth.getBloodPressure(opt);
            const dailySteps = await RNSamsungHealth.getStepCountDailies(opt);
            const floors = await RNSamsungHealth.getFloorsClimbed(opt);
            const filteredDailySteps = dailySteps.filter(
                (record) => record.source.model == 'Combined',
            );


            let obj = {
                activities: activities,
                dailySteps: filteredDailySteps,
                weight: weight,
                sleep: sleep,
                heartRate: hr,
                water: water,
                sleepStage: sleepStage,
                bloodPressure: bloodPressure,
                floors: floors,
            };

            await axios.post(
                BACKEND + 'samsung/sync/' + startDate + '/' + endDate,
                obj
                , {
                    headers: {
                        Authorization: 'Bearer ' + jwt,
                    },
                });
            return true;
        } catch (error) {
            return false;
        }
    };

    const onChangeFrom = (event, selectedDate) => {
        const currentDate = selectedDate || dateFrom;
        setShowFrom(Platform.OS === 'ios');
        setDateFrom(currentDate);
    };

    const onChangeTo = (event, selectedDate) => {
        const currentDate = selectedDate || dateTo;
        setShowTo(Platform.OS === 'ios');
        setDateTo(currentDate);
    };

    const showModeFrom = (currentMode) => {
        setShowFrom(true);
        setModeFrom(currentMode);
    };

    const showModeTo = (currentMode) => {
        setShowTo(true);
        setModeTo(currentMode);
    };

    const showDatePickerFrom = () => {
        showModeFrom('date');
    };

    const showDatePickerTo = () => {
        showModeTo('date');
    };

    const onPressSyncSamsung = () => {
        const sync = healthDateRange();
        if (!sync) {
            Alert.alert("Error", "Synchronization did not proceed")
        }
        else {
            Alert.alert("Success", "Synchronization was successful")
        }
    };

    const onPressAuthAccess = async () => {
        await RNSamsungHealth.authorize();
    };

    return (
        <ScrollView contentInsetAdjustmentBehavior="automatic" contentContainerStyle={{display: "flex", flexDirection: "column", alignItems: "center"}}>
            <Text style={style.currentScreenHeading}>Synchronization</Text>
            <View style={{width: "80%"}}>
                <View style={{display: 'flex', flexDirection: 'row', marginTop: 30}}>
                    <DateButton label={"From"} onPress={showDatePickerFrom} title={dateFrom != null ? moment(dateFrom).format("DD MMM yyyy") : ''}/>
                    <DateButton label={"To"} onPress={showDatePickerTo} title={dateTo != null ? moment(dateTo).format("DD MMM yyyy") : ''} />
                </View>
                {showFrom && (
                    <DateTimePicker
                        testID="dateTimePicker"
                        value={dateFrom}
                        mode={modeFrom}
                        is24Hour={true}
                        display="default"
                        onChange={onChangeFrom}
                    />
                )}
                {showTo && (
                    <DateTimePicker
                        testID="dateTimePicker"
                        value={dateTo}
                        mode={modeTo}
                        is24Hour={true}
                        display="default"
                        onChange={onChangeTo}
                        minimumDate={dateFrom}
                    />
                )}
                <View>
                    <SyncButton onPress={onPressAuthAccess} title={'Authorize Samsung Access'}/>
                    <SyncButton onPress={onPressSyncSamsung} title={'Sync Samsung Data'}/>
                </View>
            </View>
        </ScrollView>
    );
};

const styles = StyleSheet.create({
    main: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        width: "100%"
    }
});

export default Sync;
