import 'react-native-gesture-handler';
import React, {useContext, useEffect, useState} from 'react';
import {Alert, Platform, ScrollView, StyleSheet, Text, TextInput, View} from 'react-native';
import RNSamsungHealth from 'rn-samsung-health';
import axios from 'axios';
import {AppContext} from '../components/context';
import RNSecureStorage from 'rn-secure-storage';
import {BACKEND} from '../constants/Constants';
import {AwesomeTextInput} from 'react-native-awesome-text-input';
import moment from 'moment';
import {SubmitButton} from '../helpers/Buttons';
import Icon from '../components/Icon';
import {Path, Svg} from 'react-native-svg';

const DisplayActivitiesScreen = () => {
    const {fetchData, selectedDate, dataOrigin} = useContext(AppContext);
    const [activityData, setActivityData] = useState(null);

    useEffect(() => {
        fetchActivities();
    }, []);

    const fetchActivities = async () => {
        const date = selectedDate != null ? selectedDate : new Date();
        const res = await fetchData('activity/' + dataOrigin.toLowerCase() + '/' + moment(date).format('yyyy-MM-DD'));
        const resdata = res.data;
        if (resdata != null) {
            setActivityData(res.data);
        }
        else {
            setActivityData([]);
        }
    };

    const renderActivities = () => {
        const logs = activityData.map((log, index) => {
            return (
                <View key={index} style={{borderTopWidth: 1, paddingTop: 13, paddingBottom: 13, paddingRight: 25, paddingLeft: 25, borderColor: '#b0b0b0'}}>
                    <View>
                        <Text style={{fontWeight: 'bold', fontSize: 14, marginBottom: 9}}>{log.name == 'Custom' ? 'Custom activity' : log.name}</Text>
                    </View>
                    <View style={{display: 'flex', flexDirection: 'row'}}>
                        <View style={{marginRight: 20, display: 'flex', flexDirection: 'row'}}>
                            <Icon hex={'ff6347'} type={'clock'}/>
                            <Text style={{marginLeft: 5}}>{moment(log.startDateTime).format('HH:mm:ss')}</Text>
                        </View>
                        <View style={{marginRight: 20, display: 'flex', flexDirection: 'row'}}>
                            <Icon hex={'ff6347'} type={'sandclock'}/>
                            <Text style={{marginLeft: 5}}>{parseFloat((log.durationInSeconds / 60)).toFixed(1) + ' min'}</Text>
                        </View>
                        <View style={{marginRight: 20, display: 'flex', flexDirection: 'row'}}>
                            <Svg height="24" viewBox="0 -71 512 512" width="24" xmlns="http://www.w3.org/2000/Svg">
                                <Path fill={'#ff6347'}
                                      d="m227.078125 113.53125c0-62.332031-50.476563-113.089844-112.707031-113.5195312l-.839844-.0117188c-62.601562 0-113.53125 50.929688-113.53125 113.53125 0 34.328125 17.128906 60.527344 36.21875 86.558594l52.351562 71.429687c-16.378906 8.878907-27.53125 26.25-27.53125 46.160157 0 28.949218 23.550782 52.5 52.5 52.5 23.742188 0 43.851563-15.828126 50.3125-37.488282h23.820313v-30h-23.820313s0 0 0-.011718c-4-13.398438-13.210937-24.558594-25.273437-31.128907.5-.710937 52.292969-71.460937 52.292969-71.460937 19.078125-26.03125 36.207031-52.230469 36.207031-86.558594zm-113.539063 226.808594c-12.507812 0-22.660156-10.140625-22.660156-22.660156 0-12.507813 10.152344-22.648438 22.660156-22.648438 12.511719 0 22.660157 10.140625 22.660157 22.648438 0 12.519531-10.148438 22.660156-22.660157 22.660156zm.050782-85.449219-53.179688-72.539063c-16.640625-22.691406-30.410156-43.53125-30.410156-68.820312 0-45.890625 37.199219-83.261719 83.019531-83.53125h.519531c46.0625 0 83.539063 37.480469 83.539063 83.53125 0 25.289062-13.769531 46.128906-30.40625 68.820312 0 0-52.351563 71.527344-53.082031 72.539063zm0 0"/>
                                <Path fill={'#ff6347'}
                                      d="m113.539062 58.421875c-30.390624 0-55.109374 24.71875-55.109374 55.109375s24.71875 55.109375 55.109374 55.109375c30.390626 0 55.109376-24.71875 55.109376-55.109375s-24.71875-55.109375-55.109376-55.109375zm0 80.21875c-13.847656 0-25.109374-11.261719-25.109374-25.109375 0-13.839844 11.261718-25.109375 25.109374-25.109375 13.851563 0 25.109376 11.269531 25.109376 25.109375 0 13.847656-11.257813 25.109375-25.109376 25.109375zm0 0"/>
                                <Path fill={'#ff6347'}
                                      d="m512 113.53125c0-62.332031-50.480469-113.089844-112.710938-113.5195312l-.839843-.0117188c-62.597657 0-113.527344 50.929688-113.527344 113.53125 0 34.328125 17.128906 60.527344 36.207031 86.558594l52.359375 71.429687c-12.089843 6.550781-21.339843 17.75-25.339843 31.160157v.011718h-23.109376v30h23.109376c6.460937 21.660156 26.570312 37.488282 50.3125 37.488282 28.949218 0 52.5-23.550782 52.5-52.5 0-19.890626-11.121094-37.238282-27.472657-46.140626.523438-.710937 52.300781-71.449218 52.300781-71.449218 19.082032-26.03125 36.210938-52.230469 36.210938-86.558594zm-113.539062 226.808594c-12.511719 0-22.660157-10.140625-22.660157-22.660156 0-12.507813 10.148438-22.648438 22.660157-22.648438 12.507812 0 22.660156 10.140625 22.660156 22.648438 0 12.519531-10.152344 22.660156-22.660156 22.660156zm.050781-85.449219-53.183594-72.539063c-16.636719-22.691406-30.40625-43.53125-30.40625-68.820312 0-45.890625 37.199219-83.261719 83.019531-83.53125h.519532c46.058593 0 83.539062 37.480469 83.539062 83.53125 0 25.289062-13.769531 46.128906-30.410156 68.820312 0 0-52.339844 71.527344-53.078125 72.539063zm0 0"/>
                                <Path fill={'#ff6347'}
                                      d="m398.460938 58.421875c-30.390626 0-55.109376 24.71875-55.109376 55.109375s24.71875 55.109375 55.109376 55.109375c30.390624 0 55.109374-24.71875 55.109374-55.109375s-24.71875-55.109375-55.109374-55.109375zm0 80.21875c-13.851563 0-25.109376-11.261719-25.109376-25.109375 0-13.839844 11.257813-25.109375 25.109376-25.109375 13.847656 0 25.109374 11.269531 25.109374 25.109375 0 13.847656-11.261718 25.109375-25.109374 25.109375zm0 0"/>
                                <Path fill={'#ff6347'} d="m209.039062 302.691406h36.628907v30h-36.628907zm0 0"/>
                                <Path fill={'#ff6347'} d="m267.039062 302.691406h36.628907v30h-36.628907zm0 0"/>
                            </Svg>
                            <Text style={{marginLeft: 5}}>{log.distanceInMeters != null ? parseFloat((log.distanceInMeters / 60)).toFixed(1) + ' m' : '-'}</Text>
                        </View>
                        <View style={{marginRight: 20, display: 'flex', flexDirection: 'row'}}>
                            <Svg height="23" viewBox="0 0 512 512" width="23" xmlns="http://www.w3.org/2000/Svg">
                                <Path fill={'#ff6347'} d="m332.118 489.943a34.6 34.6 0 0 0 36.864-32.183l1.519-22.411-67.324-30.1-3.242 47.835a34.642 34.642 0 0 0 32.183 36.859z"/>
                                <Path fill={'#ff6347'}
                                      d="m307.837 352.35 71.305 33.561 23.085-82.655c16.523-76.042 6.307-132.859-28.036-155.9-20.142-13.517-41.943-10.763-54.613-2.035-.143.1-.291.191-.442.277-.1.056-11.473 6.713-21.448 21.458-13.3 19.65-18.006 43.717-14 71.538z"/>
                                <Path fill={'#ff6347'} d="m208.509 289.305-67.382 29.979 1.473 22.416a34.6 34.6 0 1 0 69.057-4.55z"/>
                                <Path fill={'#ff6347'}
                                      d="m132.58 269.828 71.368-33.428 24.366-113.715c4.055-27.813-.61-51.888-13.868-71.563-9.948-14.764-21.312-21.443-21.425-21.508-.14-.081-.293-.178-.426-.269-12.652-8.75-34.447-11.546-54.616 1.933-34.387 22.979-44.708 79.778-28.328 155.851z"/>
                            </Svg>
                            <Text style={{marginLeft: 5}}>{log.steps != null ? log.steps : '-'}</Text>
                        </View>

                    </View>
                </View>
            );
        });

        return logs;
    };

    return (
        <ScrollView contentInsetAdjustmentBehavior="automatic" contentContainerStyle={{paddingTop: 50}}>
            <Text style={styles.heading2}>Activities from {moment(selectedDate != null ? selectedDate : new Date()).format('DD.MM.yyyy')}</Text>
            {activityData != null && renderActivities()}
        </ScrollView>
    );
};

const styles = StyleSheet.create({
    main: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        width: '100%',
    },
    input: {
        borderWidth: 1,
        borderStyle: 'solid',
        borderColor: '#d4d4d4',
        color: '#5b5b5b',
        borderRadius: 6,
        paddingTop: 12,
        backgroundColor: 'white',
        paddingBottom: 12,
        paddingRight: 19,
        paddingLeft: 19,
        marginBottom: 20,
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        width: 150,
        textAlign: 'center',
    },
    label: {
        fontSize: 12,
        fontWeight: 'bold',
        color: '#323232',
    },
    heading2: {
        fontSize: 18,
        fontWeight: 'bold',
        marginBottom: 40,
        textAlign: 'center',
    },
});

export default DisplayActivitiesScreen;
