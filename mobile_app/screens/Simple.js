import {Image} from 'react-native';
import React from 'react';

import Onboarding from 'react-native-onboarding-swiper';

const Simple = ({navigation}) => {
        const handleOnDone = () => {
            navigation.navigate('Sign In');
        };

        return (
            <Onboarding
                subTitleStyles={{fontSize: 18, paddingLeft: 15, paddingRight: 15}}
                onDone={handleOnDone}
                onSkip={handleOnDone}
                pages={[
                    {
                        backgroundColor: '#fff',
                        image: <Image source={require('./../images/healthcare.png')}/>,
                        title: '',
                        subtitle: 'Use our platform for tracking your health and fitness data',
                    },
                    {
                        backgroundColor: '#fe6e58',
                        image: <Image source={require('./../images/aggregate.png')}/>,
                        subtitle: 'Have all your fitness data in one place',
                        title: '',
                    },
                ]}
            />
        );
    }
;

export default Simple;
