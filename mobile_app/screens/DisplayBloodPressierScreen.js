import 'react-native-gesture-handler';
import React, {useContext, useEffect, useState} from 'react';
import {Alert, Platform, ScrollView, StyleSheet, Text, TextInput, View} from 'react-native';
import RNSamsungHealth from 'rn-samsung-health';
import axios from 'axios';
import {AppContext} from '../components/context';
import RNSecureStorage from 'rn-secure-storage';
import {BACKEND} from '../constants/Constants';
import {AwesomeTextInput} from 'react-native-awesome-text-input';
import moment from 'moment';
import {SubmitButton} from '../helpers/Buttons';
import Icon from '../components/Icon';
import {Path, Svg} from 'react-native-svg';

const DisplayBloodPressierScreen = () => {
    const {fetchData, selectedDate, dataOrigin} = useContext(AppContext);
    const [activityData, setActivityData] = useState(null);

    useEffect(() => {
        fetchActivities();
    }, []);

    const fetchActivities = async () => {
        const date = selectedDate != null ? selectedDate : new Date();
        const res = await fetchData('blood-pressure/' + dataOrigin.toLowerCase() + '/' + moment(date).format('yyyy-MM-DD'));
        const resdata = res.data;
        if (resdata != null) {
            setActivityData(res.data);
        }
        else {
            setActivityData([]);
        }
    };

    const renderActivities = () => {
        const logs = activityData.map((log, index) => {
            return (
                <View key={index} style={{borderTopWidth: 1, paddingTop: 13, paddingBottom: 13, paddingRight: 25, paddingLeft: 25, borderColor: '#b0b0b0'}}>
                    <View>
                        <Text style={{fontWeight: 'bold', fontSize: 14, marginBottom: 9}}>{log.name == 'Custom' ? 'Custom activity' : log.name}</Text>
                    </View>
                    <View style={{display: 'flex', flexDirection: 'row'}}>
                        <View style={{marginRight: 20, display: 'flex', flexDirection: 'row'}}>
                            <Icon hex={'ff6347'} type={'clock'}/>
                            <Text style={{marginLeft: 5}}>{(log.time).substring(0, 5)}</Text>
                        </View>
                        <View style={{marginRight: 20, display: 'flex', flexDirection: 'row'}}>
                            <Icon hex={'ff6347'} type={'blood_pressure'}/>
                            <Text style={{marginLeft: 5}}>{log.systolic + "/" + log.diastolic}</Text>
                        </View>
                    </View>
                </View>
            );
        });

        return logs;
    };

    return (
        <ScrollView contentInsetAdjustmentBehavior="automatic" contentContainerStyle={{paddingTop: 50}}>
            <Text style={styles.heading2}>Blood Pressure from {moment(selectedDate != null ? selectedDate : new Date()).format('DD.MM.yyyy')}</Text>
            {activityData != null && renderActivities()}
        </ScrollView>
    );
};

const styles = StyleSheet.create({
    main: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        width: '100%',
    },
    input: {
        borderWidth: 1,
        borderStyle: 'solid',
        borderColor: '#d4d4d4',
        color: '#5b5b5b',
        borderRadius: 6,
        paddingTop: 12,
        backgroundColor: 'white',
        paddingBottom: 12,
        paddingRight: 19,
        paddingLeft: 19,
        marginBottom: 20,
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        width: 150,
        textAlign: 'center',
    },
    label: {
        fontSize: 12,
        fontWeight: 'bold',
        color: '#323232',
    },
    heading2: {
        fontSize: 18,
        fontWeight: 'bold',
        marginBottom: 40,
        textAlign: 'center',
    },
});

export default DisplayBloodPressierScreen;
