import 'react-native-gesture-handler';
import React from 'react';
import {createStackNavigator} from '@react-navigation/stack';
import DailyOverview from './DailyOverview';
import RecordWaterScreen from './RecordWaterScreen';
import {StatusBar, View} from 'react-native';
import RecordWeightScreen from './RecordWeightScreen';
import RecordBloodPressureScreen from './RecordBloodPressureScreen';
import DisplayActivitiesScreen from './DisplayActivitiesScreen';
import DisplaySleepScreen from './DisplaySleepScreen';
import DisplayBloodPressierScreen from './DisplayBloodPressierScreen';


const Home = () => {
    const Stack = createStackNavigator();
    return (
        <View style={{flex: 1}}>
            <Stack.Navigator initialRouteName={'Daily Overview'}>
                <Stack.Screen name={'Daily Overview'}
                              component={DailyOverview}
                              options={{headerShown: false}}
                />
                <Stack.Screen name={'Record Water Intake'}
                              component={RecordWaterScreen}/>
                <Stack.Screen name={'Record Weight'}
                              component={RecordWeightScreen}/>
                <Stack.Screen name={'Record Blood Pressure'}
                              component={RecordBloodPressureScreen}/>
                <Stack.Screen name={'Activities'}
                              component={DisplayActivitiesScreen}/>
                <Stack.Screen name={'Sleep'}
                              component={DisplaySleepScreen}/>
            </Stack.Navigator>
        </View>
    );
};

export default Home;
