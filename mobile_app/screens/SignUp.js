import 'react-native-gesture-handler';
import React, {useEffect, useState} from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar, TextInput, Button, Pressable,
} from 'react-native';
import {AppContext} from '../components/context';
import {SubmitButton} from '../helpers/Buttons';
import {log} from 'react-native-reanimated';
import {err} from 'react-native-svg/lib/typescript/xml';

const SignUp = ({navigation}) => {
    const [username, setUsername] = React.useState('');
    const [password, setPassword] = React.useState('');
    const [errMsg, setErrMsg] = React.useState('');
    const [usernameRrrMsg, setUsernameRrrMsg] = React.useState('');
    const [pwdErrMsg, setPwdErrMsg] = React.useState('');

    const {signUp, signIn} = React.useContext(AppContext);

    function validateEmail(mail) {
        if (/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(mail)) {
            return (true);
        }
        return (false);
    }

    const register = async () => {
        if ((username.length < 5 || username.length > 320)) {
            setUsernameRrrMsg('E-mail must have between 5 and 320 characters');
        }
        if (!validateEmail(username)) {
            setUsernameRrrMsg('E-mail is in a wrong format');
        }
        else {
            setUsernameRrrMsg('');
        }
        if ((password.length < 5 || password.length > 120)) {
            setPwdErrMsg('Password must have between 5 and 120 characters');
        }
        else {
            setPwdErrMsg('');
        }
        if ((username.length >= 5 && username.length <= 320) && (password.length >= 5 && password.length <= 120) && validateEmail(username)) {
            setUsernameRrrMsg('');
            setPwdErrMsg('');
            const res = await signUp({username, password});
            if (res == 'E-mail is already registered') {
                setErrMsg(res);
            }
            else if (res == 'Registration successful') {
                signIn({username, password});
            }
        }
    };

    return (
        <SafeAreaView style={{display: 'flex', alignItems: 'center'}}>
            <View style={{width: '80%'}}>
                <Text style={styles.h1}>Sign Up</Text>
                {errMsg != null && <Text style={styles.errMsg}>{errMsg}</Text>}
                <TextInput
                    style={styles.input}
                    placeholder="E-mail"
                    value={username}
                    onChangeText={(text) => {
                        setErrMsg('');
                        setUsername(text);
                    }}
                />
                <Text style={styles.errMsgInput}>{usernameRrrMsg}</Text>
                <TextInput
                    style={styles.input}
                    placeholder="Password"
                    value={password}
                    onChangeText={(text) => {
                        setErrMsg('');
                        setPassword(text);
                    }}
                    secureTextEntry
                />
                <Text style={styles.errMsgInput}>{pwdErrMsg}</Text>
                <View style={{marginTop: 20}}>
                    <SubmitButton title={'Sign Up'} onPress={register}/>
                </View>
            </View>
            <View>
                <Text style={{marginTop: 50, textAlign: 'center', fontSize: 15, color: '#565656'}}>Already have an account?</Text>
                <Pressable onPress={() => {navigation.navigate('Sign In');}}>
                    <Text style={{textAlign: 'center', fontSize: 15, fontWeight: 'bold', color: 'tomato'}}>Sign in here.</Text>
                </Pressable>
            </View>
        </SafeAreaView>
    );
};

const styles = StyleSheet.create({
    h1: {
        fontSize: 26,
        fontWeight: 'bold',
        textAlign: 'center',
        marginTop: 60,
        marginBottom: 40,
    },
    input: {
        borderWidth: 1,
        borderStyle: 'solid',
        borderColor: '#d4d4d4',
        color: '#5b5b5b',
        borderRadius: 6,
        paddingTop: 12,
        backgroundColor: 'white',
        paddingBottom: 12,
        paddingRight: 19,
        paddingLeft: 19,
        marginTop: 20,
        display: 'flex',
        flexDirection: 'row',
        justifyContent: 'center',
        alignItems: 'center',
        width: '100%',
        fontSize: 13,
    },
    errMsg: {
        color: 'red',
        fontSize: 16,
        textAlign: 'center',
        marginBottom: 15,
    },
    errMsgInput: {
        color: 'red',
        fontSize: 11,
        textAlign: 'left',
        marginBottom: 0,
        marginLeft: 2,
        marginTop: 3,
    },
});

export default SignUp;
