import 'react-native-gesture-handler';
import React, {useContext, useEffect, useState} from 'react';
import {Alert, Platform, ScrollView, StyleSheet, Text, TextInput, View} from 'react-native';
import RNSamsungHealth from 'rn-samsung-health';
import axios from 'axios';
import {AppContext} from '../components/context';
import RNSecureStorage from 'rn-secure-storage';
import {BACKEND} from '../constants/Constants';
import {AwesomeTextInput} from 'react-native-awesome-text-input';
import moment from 'moment';
import {SubmitButton} from '../helpers/Buttons';
import {err} from 'react-native-svg/lib/typescript/xml';

const RecordBloodPressureScreen = ({navigation}) => {
    const {selectedDate, date, setDate} = useContext(AppContext);
    const [systolic, setSystolic] = useState('0');
    const [diastolic, setDiastolic] = useState('0');
    const [errMsg, setErrMsg] = useState("");

    useEffect(() => {

    });

    const submitForm = async (event) => {
        const authToken = await RNSecureStorage.get('userToken');
        const date = selectedDate != null ? selectedDate : new Date();
        axios.post(BACKEND + "blood-pressure/this/" + moment(date).format("yyyy-MM-DD"), {
            date: moment(date).format("yyyy-MM-DD"),
            time: "00:00:00",
            systolic: systolic,
            diastolic: diastolic
        }, {
            headers: {
                Authorization: "Bearer " + authToken
            }
        }).then(res => {
            Alert.alert("Success", res.data.message);
            setErrMsg("");
        }).catch(e => {
            setErrMsg(e.response.data.join("\n"));
        });
    };

    return (
        <ScrollView contentInsetAdjustmentBehavior="automatic" contentContainerStyle={{display: 'flex', flexDirection: 'column', alignItems: 'center', paddingTop: 50}}>
            <Text style={styles.heading2}>Add a blood pressure from {moment(selectedDate != null ? selectedDate : new Date()).format('DD.MM.yyyy')}</Text>
            {errMsg != "" && <Text style={styles.errMsg}>{errMsg}</Text>}
            <View>
                <Text style={styles.label}>Systolic*</Text>
                <TextInput
                    maxLength={3}
                    style={styles.input}
                    onChangeText={setSystolic}
                    value={systolic}
                    placeholder="120"
                    keyboardType="numeric"
                />
            </View>
            <View>
                <Text style={styles.label}>Diastolic*</Text>
                <TextInput
                    maxLength={3}
                    style={styles.input}
                    onChangeText={setDiastolic}
                    value={diastolic}
                    placeholder="85"
                    keyboardType="numeric"
                />
            </View>
            <SubmitButton title={"Add Blood Pressure"} onPress={submitForm}/>
        </ScrollView>
    );
};

const styles = StyleSheet.create({
    main: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        width: '100%',
    },
    input: {
        borderWidth: 1,
        borderStyle: "solid",
        borderColor: "#d4d4d4",
        color: "#5b5b5b",
        borderRadius: 6,
        paddingTop: 12,
        backgroundColor: "white",
        paddingBottom: 12,
        paddingRight: 19,
        paddingLeft: 19,
        marginBottom: 20,
        display: "flex",
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center",
        width: 150,
        textAlign: "center"
    },
    label: {
        fontSize: 12,
        fontWeight: "bold",
        color: "#323232"
    },
    heading2: {
        fontSize: 18,
        fontWeight: "bold",
        marginBottom: 20
    },
    errMsg: {
        color: "red",
        marginBottom: 30,textAlign: "center"
    }
});

export default RecordBloodPressureScreen;
