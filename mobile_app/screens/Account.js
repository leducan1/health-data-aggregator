import 'react-native-gesture-handler';
import React, {useEffect, useState} from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar, Pressable,
} from 'react-native';
import style from '../styles/ScreenStyles';
import {AppContext} from '../components/context';
import Svg, {Circle, Path} from 'react-native-svg';

const Account = () => {

    const {signOut} = React.useContext(AppContext);

    return (
        <SafeAreaView>
            <Text style={style.currentScreenHeading}>Account</Text>
            <Pressable style={styles.pressableItem} onPress={() => {}}>
                <Svg xmlns="http://www.w3.org/2000/svg" className="icon icon-tabler icon-tabler-user" width="24" height="24" viewBox="0 0 24 24" stroke-width="1.5" stroke="#000000"
                     fill="none" stroke-linecap="round" stroke-linejoin="round">
                    <Path stroke="none" d="M0 0h24v24H0z" fill="none"/>
                    <Circle cx="12" cy="7" r="4"/>
                    <Path d="M6 21v-2a4 4 0 0 1 4 -4h4a4 4 0 0 1 4 4v2"/>
                </Svg>
                <Text style={styles.pressableText}>Account settings</Text>
            </Pressable>
            <Pressable style={styles.pressableItem} onPress={signOut}>
                <Svg xmlns="http://www.w3.org/2000/svg" className="icon icon-tabler icon-tabler-logout" width="24" height="24" viewBox="0 0 24 24" stroke-width="2" stroke="#000000"
                     fill="none" stroke-linecap="round" stroke-linejoin="round">
                    <Path stroke="none" d="M0 0h24v24H0z" fill="none"/>
                    <Path d="M14 8v-2a2 2 0 0 0 -2 -2h-7a2 2 0 0 0 -2 2v12a2 2 0 0 0 2 2h7a2 2 0 0 0 2 -2v-2"/>
                    <Path d="M7 12h14l-3 -3m0 6l3 -3"/>
                </Svg>
                <Text style={styles.pressableText}>Sign out</Text>
            </Pressable>
        </SafeAreaView>
    );
};

export default Account;

const styles = StyleSheet.create({
    pressableItem: {
        display: "flex",
        flexDirection: "row",
        paddingTop: 20,
        paddingBottom: 20,
        paddingLeft: 30,
        borderBottomWidth: 1,
        borderBottomColor: "#E5E5E5"
    },
    pressableText: {
        fontSize: 16,
        marginLeft: 16
    }
})

