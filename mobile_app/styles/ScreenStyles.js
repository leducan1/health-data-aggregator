import { StyleSheet } from 'react-native';

export default StyleSheet.create({
    currentScreenHeading: {
        fontSize: 18,
        fontWeight: 'bold',
        textAlign: 'center',
        paddingTop: 14,
        paddingBottom: 14,
        // borderBottomWidth: 1,
        // borderBottomColor: "#C4C4C4",
        width: "100%",
        backgroundColor: "#ffffff"
    },
    caret: {
        marginTop: -10
    }
});
