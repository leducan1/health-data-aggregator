import 'react-native-gesture-handler';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import Home from '../screens/Home';
import Sync from '../screens/Sync';
import Account from '../screens/Account';
import React, {useContext, useEffect} from 'react';
import {
    SafeAreaView,
    StyleSheet,
    ScrollView,
    View,
    Text,
    StatusBar,
} from 'react-native';
import Svg, {
    Circle,
    Path,
    Polyline,
} from 'react-native-svg';
import BackgroundService from 'react-native-background-actions';
import moment from 'moment';
import {AppContext} from './context';


const Tab = createBottomTabNavigator();

const BottomNavigation = ({props}) => {
    const {health, userToken} = useContext(AppContext);

    useEffect(() => {
        if (userToken != null) {
            let isMounted = true;
            if (!BackgroundService.isRunning()) {
                runTask();
            }
            return () => { isMounted = false };
        }
    },[])
    const sleep = time => new Promise(resolve => setTimeout(() => resolve(), time));

    const options = {
        taskName: 'Samsung Sync',
        taskTitle: 'Samsung Sync',
        taskDesc: 'Samsung Sync',
        taskIcon: {
            name: 'ic_launcher',
            type: 'mipmap',
        },
        color: '#ff00ff',
        linkingURI: 'yourSchemeHere://chat/jane', // See Deep Linking for more info
        parameters: {
            delay: 1000 * 10 * 1,
        },
    };

    const infiniteTask = async (taskDataArguments) => {
        const { delay } = taskDataArguments;
        await new Promise( async (resolve) => {
            for (let i = 0; BackgroundService.isRunning(); i++) {
                try {
                    await health();
                    await BackgroundService.updateNotification({taskDesc: 'Last sync at ' + moment(new Date()).format("yyyy-MM-DD HH:mm:ss")});
                } catch (e) {
                    console.log(e)
                }
                await sleep(delay);
            }
        });
    };

    const runTask = async () => {
        await BackgroundService.start(infiniteTask, options);
    }

    return (
        <Tab.Navigator
            tabBarOptions={{
                activeTintColor: 'tomato',
                inactiveTintColor: 'gray',
            }}
            screenOptions={({route}) => ({
                tabBarIcon: ({focused}) => {
                    if (route.name == 'Home') {
                        return (
                            <Svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24"
                                 stroke-width="2" stroke={focused ? 'tomato' : '#2c3e50'}
                                 fill="none" stroke-linecap="round" stroke-linejoin="round">
                                <Path stroke="none" d="M0 0h24v24H0z" fill="none"/>
                                <Polyline points="5 12 3 12 12 3 21 12 19 12"/>
                                <Path d="M5 12v7a2 2 0 0 0 2 2h10a2 2 0 0 0 2 -2v-7"/>
                                <Path d="M9 21v-6a2 2 0 0 1 2 -2h2a2 2 0 0 1 2 2v6"/>
                            </Svg>
                        );
                    }
                    else if (route.name == 'Sync') {
                        return (
                            <Svg xmlns="http://www.w3.org/2000/svg" className="icon icon-tabler icon-tabler-refresh"
                                 width="24" height="24" viewBox="0 0 24 24" stroke-width="2"
                                 stroke={focused ? 'tomato' : '#2c3e50'}
                                 fill="none" stroke-linecap="round" stroke-linejoin="round">
                                <Path stroke="none" d="M0 0h24v24H0z" fill="none"/>
                                <Path d="M20 11a8.1 8.1 0 0 0 -15.5 -2m-.5 -4v4h4"/>
                                <Path d="M4 13a8.1 8.1 0 0 0 15.5 2m.5 4v-4h-4"/>
                            </Svg>
                        );
                    }
                    else if (route.name == 'Account') {
                        return (
                            <Svg xmlns="http://www.w3.org/2000/svg" className="icon icon-tabler icon-tabler-user"
                                 width="24" height="24" viewBox="0 0 24 24" stroke-width="2"
                                 stroke={focused ? 'tomato' : '#2c3e50'}
                                 fill="none" stroke-linecap="round" stroke-linejoin="round">
                                <Path stroke="none" d="M0 0h24v24H0z" fill="none"/>
                                <Circle cx="12" cy="7" r="4"/>
                                <Path d="M6 21v-2a4 4 0 0 1 4 -4h4a4 4 0 0 1 4 4v2"/>
                            </Svg>
                        );
                    }
                },
            })}
        >
            <Tab.Screen
                name={'Home'}
                component={Home}
            />
            <Tab.Screen
                name={'Sync'}
                component={Sync}
            />
            <Tab.Screen
                name={'Account'}
                component={Account}
            />
        </Tab.Navigator>
    );
};

export default BottomNavigation;
