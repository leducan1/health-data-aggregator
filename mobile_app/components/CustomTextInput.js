import 'react-native-gesture-handler';
import React, {useContext, useEffect, useState} from 'react';
import {Platform, ScrollView, StyleSheet, Text, TextInput} from 'react-native';
import RNSamsungHealth from 'rn-samsung-health';
import axios from 'axios';
import {AppContext} from '../components/context';
import RNSecureStorage from 'rn-secure-storage';
import {BACKEND} from '../constants/Constants';
import {AwesomeTextInput} from 'react-native-awesome-text-input';
import moment from 'moment';

const CustomTextInput = () => {
    const {selectedDate} = useContext(AppContext);
    const [amount, setAmount] = useState(0);

    useEffect(() => {
    });

    const submitForm = async (event) => {
        const authToken = await RNSecureStorage.get('userToken');
        const date = selectedDate != null ? selectedDate : new Date();
        await axios.post(BACKEND + 'water/this/' + moment(date).format('yyyy-MM-DD'), {
            date: moment(date).format('yyyy-MM-DD'),
            amount: amount,
        }, {
            headers: {
                Authorization: 'Bearer ' + authToken,
            },
        });
    };

    return (
        <ScrollView contentInsetAdjustmentBehavior="automatic" contentContainerStyle={{display: 'flex', flexDirection: 'column', alignItems: 'center', paddingTop: 50}}>
            <Text>Add a water log from {moment(selectedDate != null ? selectedDate : new Date()).format('DD.MM.yyyy')}</Text>
            <Text style={styles.label}>Amount of water in ml*</Text>
            <TextInput
                maxLength={7}
                style={styles.input}
                onChangeText={setAmount}
                value={amount}
                placeholder="ml"
                keyboardType="numeric"
            />
        </ScrollView>
    );
};

const styles = StyleSheet.create({
    main: {
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
        width: '100%',
    },
    input: {
        borderWidth: 1,
        borderStyle: "solid",
        borderColor: "#d4d4d4",
        color: "#5b5b5b",
        borderRadius: 6,
        paddingTop: 12,
        backgroundColor: "white",
        paddingBottom: 12,
        paddingRight: 19,
        paddingLeft: 19,
        marginBottom: 20,
        display: "flex",
        flexDirection: "row",
        justifyContent: "center",
        alignItems: "center",
        width: 150,
        textAlign: "center"
    },
    label: {
        fontSize: 12,
        fontWeight: "bold",
        color: "#323232"
    }
});

export default CustomTextInput;
